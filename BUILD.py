# FOR THE LOVE OF CHRIST DO NOT USE ISORT IN HERE.
import os
import sys
import yaml

PATHS = {}
if os.path.isfile(os.path.join('build','paths.yml')):
    with open(os.path.join('build','paths.yml'), 'r') as f:
        PATHS = yaml.load(f)

# This assumes a layout like described in the guide. If not, you need to change
# build/paths.yml.example and rename it to paths.yml.
UMBRELLA_DIR = os.path.abspath(PATHS.get('UMBRELLA_DIR', os.path.join('..','..')))
NYLON_BASE = os.path.abspath(PATHS.get('NYLON_BASE', os.path.join(UMBRELLA_DIR, 'nylon')))
PROJ_BASE = os.path.abspath(PATHS.get('PROJ_BASE', os.path.join(UMBRELLA_DIR, 'projects', 'Edenbound')))
TMP_DIR = os.path.abspath(os.path.join(PROJ_BASE, 'tmp'))
DIST_DIR = os.path.abspath(PATHS.get('DIST_DIR', os.path.join(PROJ_BASE, 'dist')))

print('UMBRELLA_DIR='+UMBRELLA_DIR)
print('NYLON_BASE='+NYLON_BASE)
print('PROJ_BASE='+PROJ_BASE)
print('TMP_DIR='+TMP_DIR)
print('DIST_DIR='+DIST_DIR)

if not os.path.isdir(NYLON_BASE):
    print(f'Cannot find Nylon in {NYLON_BASE}!  Please configure build/paths.yml properly.')
    sys.exit(1)
else:
    print("Nylon found!")
if not os.path.isdir(PROJ_BASE):
    print(f'Cannot find Edenbound in {PROJ_BASE}!  Please configure build/paths.yml properly.')
    sys.exit(1)
else:
    print("Edenbound found!")

# Nylon
sys.path.append(os.path.join(NYLON_BASE, 'src', 'python'))
sys.path.append(os.path.join(NYLON_BASE, 'lib', 'twine'))
sys.path.append(os.path.join(NYLON_BASE, 'lib', 'slimit-twine'))
# Edenbound
sys.path.append(os.path.join(PROJ_BASE, 'src', 'python'))

try:
    from buildtools import ENV, os_utils
    from buildtools.config import YAMLConfig
    from buildtools.maestro import BuildMaestro
    from buildtools.maestro.coffeescript import CoffeeBuildTarget
    from buildtools.maestro.fileio import (ConcatenateBuildTarget, CopyFilesTarget, ReplaceTextTarget, CopyFileTarget)
    # from buildtools.maestro.utils import SerializableFileLambda
    from buildtools.maestro.web import (DatafyImagesTarget, MinifySVGTarget, DartSCSSBuildTarget, UglifyJSTarget)
    from buildtools.maestro.package_managers import BrowserifyBuildTarget, YarnBuildTarget
    from buildtools.maestro.shell import CommandBuildTarget

    from edenbound.generators.JQueryUI import DownloadJQueryUIIconsTarget
    from edenbound.generators.Fonts import WebifyTarget
    from edenbound.generators.Serializer import SerializerGenerator
    from edenbound.generators.Enums import GenerateEnumTarget
    from edenbound.generators.MapData import MapDataTarget

    # Nylon
    from twinehack.buildtargets.nylon import (BuildNylonProjectTarget, GenNylonHeaderTarget)
    from twinehack.utils import CoffeeAcquireFilesLambda
except ImportError as ie:
    print(str(ie))
    print('ImportError: Did you follow the prep instructions for your OS?')


def CopyAdobeFontFromNPM(maestro, yarn, package, prefix, out_dirname, variants=['']):
    o = []
    format_major=[
        'EOT',
        #'OTF',
        #'TTF',
        'WOFF',
        'WOFF2'
    ]
    woff_format='TTF' #['OTF', 'TTF'] - EOT WOFF2 causes issues on Windows.
    for fmt in format_major:
        ext = '.'+fmt.lower()
        subext = ''
        subdir=''
        if fmt in ('WOFF','WOFF2'):
            subext = '.'+woff_format.lower()
            subdir = woff_format
        for variant in variants:
            var_suffix = ''
            if variant != '':
                var_suffix = '-'+variant
            source_path = os.path.join('node_modules', package, fmt, subdir, prefix+var_suffix+subext+ext)
            dest_path = os.path.join(DIST_DIR, 'fonts', out_dirname, out_dirname+var_suffix+ext)
            o += [maestro.add(CopyFileTarget(dest_path, source_path, verbose=True, dependencies=[yarn.target])).target]
    return o

def main():
    maestro = BuildMaestro()
    argp = maestro.build_argparser()
    argp.add_argument('--release', action='store_true', help='Builds Edenbound for release.  Minifies stuff, deploys to web/.')
    argp.add_argument('--submodules', action='store_true', help='Checks submodules.')
    args = maestro.parse_args(argp)

    cfg = YAMLConfig(os.path.join('build','config.yml'))

    EXE_SUFFIX = '.exe' if os_utils.is_windows() else ''
    CMD_SUFFIX = '.cmd' if os_utils.is_windows() else ''
    svgcli_executable = PATHS.get('SVGCLEANER-CLI',ENV.which('svgcleaner-cli') or os.path.join(NYLON_BASE,'bin','svgcleaner-cli'+EXE_SUFFIX))

    ENV.prependTo('PATH', os.path.abspath(os.path.join(PROJ_BASE,'node_modules','.bin')))
    ENV.prependTo('PATH', os.path.join(NYLON_BASE, 'bin'))


    os_utils.ensureDirExists(DIST_DIR, noisy=True)
    os_utils.ensureDirExists(TMP_DIR, noisy=True)

    if not os.path.isfile(os.path.join(NYLON_BASE, 'lib', 'twine', '.git')) or args.submodules:
        with os_utils.Chdir(NYLON_BASE):
            os_utils.cmd(['git','submodule','update','--init','--recursive'],echo=True)
    # We don't really need this, at the moment.
    #os_utils.cmd(['git','submodule','update','--init','--recursive'],echo=True)

    VERSION = ''

    yarn = YarnBuildTarget(working_dir=PROJ_BASE)
    maestro.add(yarn)

    NODE_MODULES_DIR = os.path.join(PROJ_BASE,'node_modules')
    requires = []
    for pkg in cfg.get('browserify.require',[]):
        requires+=['-r',pkg]
    browserify = BrowserifyBuildTarget(working_dir=PROJ_BASE,
                                       opts=requires+[
                                           '-o', './tmp/libs.node.js'
                                       ],
                                       target=os.path.join(PROJ_BASE,'tmp','libs.node.js'),
                                       files=[
                                         os.path.join(NODE_MODULES_DIR, 'zalgolize','zalgo.js')
                                       ],
                                       dependencies=[yarn.target],
                                       browserify_path=ENV.which('browserify') or os.path.join(PROJ_BASE, 'node_modules', '.bin', 'browserify'))

    maestro.add(browserify)

    story_cfg = YAMLConfig(os.path.join(PROJ_BASE, 'story.yml'), default=None)
    VERSION = story_cfg.get('variables.version')
    VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH = [int(x) for x in VERSION.split('.')]

    JQUERY_UI_THEME_DIR = os.path.join(PROJ_BASE, 'src', 'style', 'jquery-ui')
    MOCHA_THEME_DIR     = os.path.join(PROJ_BASE, 'src', 'style', 'mocha')
    MAIN_STYLE_DIR      = os.path.join(PROJ_BASE, 'src', 'style', 'main')

    svgTargets = []
    svgDirs = ['backgrounds', 'gender-buttons', 'symbol', 'avatar', 'weather', 'choice-badges']
    default_svg_tags=['svg']
    tag_overrides={
        'avatar-error': default_svg_tags+['glitch'],
    }

    svgs = [
        os.path.join(PROJ_BASE, 'svg', 'Logo.svg')
    ]

    OUT_SVG_BASE = os.path.join(PROJ_BASE, 'src', 'tiddler', 'svg')
    for svgdir in svgDirs:
        IN_SVG_BASE = os.path.join(PROJ_BASE, 'svg', svgdir)
        # os_utils.safe_rmtree(OUT_SVG_BASE)
        for filename in os_utils.get_file_list(IN_SVG_BASE):
            if filename.endswith('.svg'):
                svgs+=[os.path.join(IN_SVG_BASE, filename)]

    for filename in svgs:
        basefilename = os.path.basename(filename)
        svgtags = tag_overrides[basefilename[:-4]] if basefilename[:-4] in tag_overrides else default_svg_tags
        svg_hdr = GenNylonHeaderTarget(os.path.join(OUT_SVG_BASE, basefilename[:-4] + '.header.yml'), basefilename[:-4], tags=svgtags, ext='svg')
        maestro.add(svg_hdr)
        cmd = [svgcli_executable, filename, os.path.join(OUT_SVG_BASE, basefilename),'--copy-on-error', '--remove-unreferenced-ids','false','--trim-ids','false']
        svg_minify = CommandBuildTarget(
            targets=[os.path.join(OUT_SVG_BASE, basefilename)],
            files=[filename],
            dependencies=[svg_hdr.target],
            cmd=cmd,
            show_output=True,
            echo=False)
        maestro.add(svg_minify)
        svgTargets += svg_minify.provides()

    ENUM_DIR = os.path.join(PROJ_BASE,'src','data','enums')
    ENUM_DEST_DIR = os.path.join(PROJ_BASE,'src','coffee','enums')
    for enumfile in os_utils.get_file_list(ENUM_DIR):
        basename, ext = os.path.splitext(enumfile)
        if ext == '.yml':
            srcfilename = os.path.join(ENUM_DIR,enumfile)
            destfilename = os.path.join(ENUM_DEST_DIR,basename+'.coffee')
            svgTargets += [maestro.add(GenerateEnumTarget(destfilename, srcfilename)).target]

    acquired_coffee = CoffeeAcquireFilesLambda(
        os.path.join(PROJ_BASE, 'src', 'coffee'),
        manual_load_order=[],
        additional_files=[],
        additional_depscanners={
            #r'\b(post|pre)Display(\[|\b)': 'MutBar',
            #r'\bInventoryController\.':    'InventoryController',
        })
    serializers = maestro.add(SerializerGenerator('.build/serializers',acquired_coffee))

    scriptingCoffeeJS = CoffeeBuildTarget(
        target=os.path.join(PROJ_BASE, 'tmp', 'GameScript.cof.js'),
        files=acquired_coffee,
        dependencies=[serializers.target],
        make_map=True,
        coffee_opts=['-bc', '--no-header'],
        coffee_executable=os.path.join(NODE_MODULES_DIR,'.bin','coffee'+CMD_SUFFIX))
    maestro.add(scriptingCoffeeJS)

    scriptingJS = ConcatenateBuildTarget(
        target=os.path.join(PROJ_BASE, 'tmp', 'GameScript.js'),
        files=[
            scriptingCoffeeJS.target,
        ],
        dependencies=[scriptingCoffeeJS.target],
    )
    maestro.add(scriptingJS)

    FONTS_SRC=os.path.join(PROJ_BASE, 'src', 'fonts')
    FONTS_DEST=os.path.join(DIST_DIR, 'fonts')
    fonts = [
        maestro.add(WebifyTarget(os.path.join(FONTS_DEST,'gfs-didot', 'gfs-didot.woff'), os.path.join(FONTS_SRC, 'gfs-didot', 'GFSDidot-Regular.ttf'))).target,
        maestro.add(WebifyTarget(os.path.join(FONTS_DEST,'magnolia-light', 'magnolia-light.woff'), os.path.join(FONTS_SRC, 'magnolia', 'Magnolia Light.ttf'))).target,
        maestro.add(WebifyTarget(os.path.join(FONTS_DEST,'a-glitch-in-time', 'a-glitch-in-time.woff'), os.path.join(FONTS_SRC, 'a glitch in time', 'A Glitch In Time.ttf'))).target,
        maestro.add(WebifyTarget(os.path.join(FONTS_DEST,'marcellus', 'marcellus.woff'), os.path.join(FONTS_SRC, 'marcellus', 'Marcellus-Regular.ttf'))).target,
        maestro.add(WebifyTarget(os.path.join(FONTS_DEST,'pokoljaro', 'pokoljaro.woff'), os.path.join(FONTS_SRC, 'pokoljaro', 'Pokoljaro.otf'))).target,
        maestro.add(WebifyTarget(os.path.join(FONTS_DEST,'bebas-neue', 'bebas-neue.woff'), os.path.join(FONTS_SRC, 'bebas neue', 'BebasNeue.otf'))).target,
    ]
    fonts+=CopyAdobeFontFromNPM(maestro, yarn, 'source-code-pro', 'SourceCodePro', 'source-code-pro', variants=['Black', 'BlackIt', 'Bold', 'BoldIt', 'ExtraLight', 'ExtraLightIt', 'It', 'Light', 'LightIt', 'Medium', 'MediumIt', 'Regular', 'Semibold', 'SemiboldIt'])
    fonts+=CopyAdobeFontFromNPM(maestro, yarn, 'source-serif-pro', 'SourceSerifPro', 'source-serif-pro', variants=['Black', 'Bold', 'ExtraLight', 'Light', 'Regular', 'Semibold'])

    mochaSCSS2CSS = DartSCSSBuildTarget(
        target=os.path.join(PROJ_BASE, 'tmp','mocha.css'),
        files=[os.path.join(MOCHA_THEME_DIR, 'mocha.scss')],
        output_style='compressed' if args.release else 'expanded',
        sass_path=os.path.join(NODE_MODULES_DIR,'.bin','sass'+CMD_SUFFIX))
    maestro.add(mochaSCSS2CSS)

    jquerySCSS2CSS = DartSCSSBuildTarget(
        target=os.path.join(PROJ_BASE,'tmp', 'jquery-ui.relurls.css'),
        files=[os.path.join(JQUERY_UI_THEME_DIR, 'jquery-ui.scss')],
        output_style='compressed' if args.release else 'expanded',
        sass_path=os.path.join(NODE_MODULES_DIR,'.bin','sass'+CMD_SUFFIX))
    maestro.add(jquerySCSS2CSS)

    mainSCSS2CSS = DartSCSSBuildTarget(
        target=os.path.join(PROJ_BASE, 'tmp', 'main.css'),
        files=[os.path.join(MAIN_STYLE_DIR, 'main.scss')],
        output_style='compressed' if args.release else 'expanded',
        sass_path=os.path.join(NODE_MODULES_DIR,'.bin','sass'+CMD_SUFFIX),
        dependencies=fonts)
    maestro.add(mainSCSS2CSS)

    jqueryUIIcons = DownloadJQueryUIIconsTarget(
        target=os.path.join('tmp', 'jquery-ui.icons.yml'),
        origfile=jquerySCSS2CSS.target,
        basedir=JQUERY_UI_THEME_DIR
    )
    maestro.add(jqueryUIIcons)

    fixJQueryUICSS = DatafyImagesTarget(os.path.join(
        'tmp', 'jquery-ui.css'), jquerySCSS2CSS.target, JQUERY_UI_THEME_DIR, dependencies=[jqueryUIIcons.target])
    maestro.add(fixJQueryUICSS)

    concatCSS = ConcatenateBuildTarget(
        target=os.path.join(PROJ_BASE,'tmp','bundle.css'),
        files=[
            mainSCSS2CSS.target,
            fixJQueryUICSS.target,
            mochaSCSS2CSS.target,
            os.path.join(NODE_MODULES_DIR, 'codemirror', 'lib', 'codemirror.css'),
            os.path.join(NODE_MODULES_DIR, 'codemirror', 'theme', 'twilight.css'),
        ],
        dependencies=[
            mainSCSS2CSS.target,
            fixJQueryUICSS.target,
            mochaSCSS2CSS.target,
        ])
    maestro.add(concatCSS)

    dataFileHeader = GenNylonHeaderTarget(os.path.join(PROJ_BASE, 'src', 'tiddler','data','__UNIVERSE__.header.yml'), '__UNIVERSE__', tags=['data', 'json'], ext='json')
    maestro.add(dataFileHeader)
    dataFiles = MapDataTarget(
        target=os.path.join(PROJ_BASE, 'src', 'tiddler','data','__UNIVERSE__.json'),
        source_dir=os.path.join(PROJ_BASE, 'src', 'data', 'world'))
    maestro.add(dataFiles)

    storyTitle = ReplaceTextTarget(
        target=os.path.join(PROJ_BASE, 'src', 'tiddler','passages','menus','special','StoryTitle.tw'),
        filename=os.path.join(PROJ_BASE, 'src', 'tiddler','passages','menus','special', 'StoryTitle.tw.in'),
        replacements={
            r'\{\{MAJOR\}\}': '{}'.format(VERSION_MAJOR),
            r'\{\{MINOR\}\}': '{}'.format(VERSION_MINOR),
            r'\{\{PATCH\}\}': '{}'.format(VERSION_PATCH),
        },
        dependencies=[])
    maestro.add(storyTitle)

    libJS = ConcatenateBuildTarget(
        target=os.path.join(PROJ_BASE, 'tmp', 'GameLibs.js'),
        files=[
            #os.path.join('lib', 'jquery-ui', 'jquery-ui.min.js'),
            #os.path.join('lib', 'filesaver', 'FileSaver.min.js'),
            #os.path.join(PROJ_BASE, 'node_modules', 'chai', 'chai.js'),
            #os.path.join(PROJ_BASE, 'node_modules', 'mocha', 'mocha.js'),
            browserify.target,
        ],
        dependencies=[browserify.target],
    )
    maestro.add(libJS)


    finalScriptingJS = scriptingJS
    #finalMacroJS = macroCoffeeJS
    finalMochaJS = os.path.join(PROJ_BASE, 'node_modules', 'mocha', 'mocha.js')
    finalChaiJS = os.path.join(PROJ_BASE, 'node_modules', 'chai', 'chai.js')

    if args.release:
        finalScriptingJS = UglifyJSTarget(os.path.join(PROJ_BASE, 'tmp', 'GameScript.min.js'), inputfile=scriptingJS.target, mangle=False, compress_opts=['keep_fnames,unsafe'])
        maestro.add(finalScriptingJS)

        #finalMacroJS = UglifyJSTarget(os.path.join(PROJ_BASE, 'src', 'tiddler', 'scripts', 'Macros.min.js'), inputfile=macroCoffeeJS.target, mangle=False, compress_opts=['keep_fnames,unsafe'])
        #maestro.add(finalMacroJS)

        finalMochaJSBT = UglifyJSTarget(os.path.join(PROJ_BASE, 'tmp', 'mocha.min.js'), inputfile=finalMochaJS, mangle=False, compress_opts=['keep_fnames,unsafe'])
        maestro.add(finalMochaJSBT)
        finalMochaJS=finalMochaJSBT.target

        finalChaiJSBT = UglifyJSTarget(os.path.join(PROJ_BASE, 'tmp', 'chai.min.js'), inputfile=finalChaiJS, mangle=False, compress_opts=['keep_fnames,unsafe'])
        maestro.add(finalChaiJSBT)
        finalChaiJS=finalChaiJSBT.target

    buildstep = BuildNylonProjectTarget(
        os.path.join(DIST_DIR, 'latest.html'),
        project_file=os.path.join(PROJ_BASE, 'story.yml'),
        dependencies=[finalScriptingJS.target, storyTitle.target] + svgTargets)
    # jQuery
    buildstep.addEmbeddedJS(os.path.join(NYLON_BASE, 'lib', 'jquery', 'jquery.min.js'), id='JQuery')
    buildstep.addEmbeddedJS(os.path.join(NYLON_BASE, 'lib', 'jquery-ui', 'jquery-ui.min.js'), id='JQueryUI')
    # Various backports
    # Needed for frosted glass effect I used in BOUNDLESS 4CE.
    buildstep.addEmbeddedJS(os.path.join(PROJ_BASE, 'node_modules', 'clip-path-polygon', 'build', 'clip-path-polygon.min.js'), id='clip-path-polygon')
    # Standardizes saving files to disk
    buildstep.addEmbeddedJS(os.path.join(PROJ_BASE, 'node_modules', 'filesaver', 'src', 'FileSaver.js'), id='FileSaver')
    # CodeMirror text editor, used in various in-game editors and viewers.
    buildstep.addEmbeddedJS(os.path.join(PROJ_BASE, 'node_modules', 'codemirror', 'lib', 'codemirror.js'), id='CodeMirrorJS')
    # the various CodeMirror syntaxes we want.
    buildstep.addEmbeddedJS(os.path.join(PROJ_BASE, 'node_modules', 'codemirror', 'mode', 'tiddlywiki', 'tiddlywiki.js'), id='CodeMirrorJS-tiddlywiki')
    buildstep.addEmbeddedJS(os.path.join(PROJ_BASE, 'node_modules', 'codemirror', 'mode', 'yaml', 'yaml.js'), id='CodeMirrorJS-tiddlywiki')
    # Testing frameworks
    buildstep.addEmbeddedJS(finalMochaJS, id="mochajs")
    buildstep.addEmbeddedJS(finalChaiJS, id='chaijs')
    # Browserify'd libraries.
    buildstep.addEmbeddedJS(libJS.target, id='Libraries')
    buildstep.addEmbeddedJS(finalScriptingJS.target, id='GameScript')
    # All of our CSS.
    buildstep.addEmbeddedCSS(concatCSS.target, id='css')
    # Internally, the various skins for Twine are called targets.
    buildstep.setTarget(os.path.join(UMBRELLA_DIR, 'twine4nylon', 'targets', 'sugarcane'))
    maestro.add(buildstep)

    if args.release:
        os_utils.ensureDirExists(os.path.join(DIST_DIR, 'versions'))
        os_utils.single_copy(buildstep.target, os.path.join(DIST_DIR, 'versions','Edenbound-{}.html'.format(VERSION)), verbose=True)

    maestro.as_app(argp)


main()
