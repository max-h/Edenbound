import yaml
import os
from buildtools import os_utils,maestro,log

class GenerateEnumTarget(maestro.base_target.SingleBuildTarget):
    BT_LABEL = 'ENUM'
    def __init__(self, target, source, dependencies=[], provides=[], name=None):
        self.filename = source
        name = target
        super().__init__(target, files=[self.filename, os.path.abspath(__file__)], dependencies=dependencies, name=name)

    def _get_value_for(self, vpak):
        if isinstance(vpak, dict):
            return vpak['value']
        else:
            return vpak

    def _get_meaning_for(self, vpak):
        if isinstance(vpak, dict):
            return vpak.get('meaning','')
        else:
            return ''

    def build(self):
        definition = {}
        with open(self.filename, 'r') as r:
            definition=yaml.load(r)['enum']

        if 'auto-value' in definition:
            autoval = definition['auto-value']
            i=autoval.get('start',0)
            for k in definition['values'].keys():
                if definition[k].get('auto', True):
                    definition[k]['value']=1 >> i if definition.get('flags', False) else i
                    i += 1

        if 'tests' in definition:
            with log.info('Testing %s....', definition['name']):
                tests = definition['tests']
                if 'increment' in tests:
                    incrdef = tests['increment']
                    start = incrdef.get('start',0)
                    stop = incrdef.get('stop', len(definition['values']))

                    vals = []
                    for k,vpak in definition['values'].items():
                        vals += [self._get_value_for(vpak)]

                    for i in range(start,stop):
                        if i not in vals:
                            log.error('Increment: Missing value %d!', i)
                if 'unique' in tests and tests['unique']:
                    vals={}
                    for k,vpak in definition['values'].items():
                        val = self._get_value_for(vpak)
                        if val in vals:
                            log.error('Unique: Entry %s is not using a unique value!', k)
                        vals[val]=True

        os_utils.ensureDirExists(os.path.dirname(self.target), noisy=True)
        with open(self.target, 'w') as w:
            w.write('# @GENERATED BY BUILD.py ({})'.format(__file__))
            w.write('\n###')
            if 'notes' in definition:
                for line in definition['notes'].split('\n'):
                    w.write('\n# {}'.format(line))
            w.write('\n# @enumdef: {}'.format(definition['name']))
            w.write('\n###')
            w.write('\nclass {}'.format(definition['name']))

            w.write('\n  @_DEFAULT: {}'.format(definition.get('default',0)))
            w.write('\n  @_ERROR: -1')

            if definition.get('flags', False):
                w.write('\n  @NONE: 0')

            for k,vpak in definition['values'].items():
                v=self._get_value_for(vpak)
                meaning=self._get_meaning_for(vpak)
                padding = '\n  '
                if meaning != '':
                    w.write('{PAD}###{PAD}# {MEANING}{PAD}###'.format(PAD=padding, MEANING=meaning))
                w.write('\n  @{}: {}'.format(k,repr(v)))

            w.write('\n\n  @ValueToString: (val, sep=", ", start_end="") ->')
            if definition.get('flags', False):
                w.write('\n    o=[]')
                w.write('\n    for bitidx in [1...32]')
                w.write('\n      switch(1 << bitidx)')
                written=[]
                for k,vpak in definition['values'].items():
                    v=self._get_value_for(vpak)
                    if v in written:
                        continue
                    written+=[v]
                    w.write('\n        when {}'.format(repr(v)))
                    w.write('\n          o.push {}'.format(repr(k)))
                w.write('\n    o = o.join(sep)')
            else:
                w.write('\n    o=null')
                w.write('\n    switch(val)')
                written=[]
                for k,vpak in definition['values'].items():
                    v=self._get_value_for(vpak)
                    if v in written:
                        continue
                    written+=[v]
                    w.write('\n      when {}'.format(repr(v)))
                    w.write('\n        o = {}'.format(repr(k)))

            w.write('\n    if start_end.length == 2')
            w.write('\n      o = start_end[0]+o+start_end[1]')
            w.write('\n    return o\n')

            w.write('\n  @StringToValue: (key) ->')
            w.write('\n    switch(key)')
            written=[]
            for k,vpak in definition['values'].items():
                if k in written:
                    continue
                written+=[k]
                v=self._get_value_for(vpak)
                w.write('\n      when {}'.format(repr(k)))
                w.write('\n        return {}'.format(repr(v)))
            w.write('\n    return -1;\n')


            w.write('\n  @Keys: ->')
            w.write('\n    return [{}]\n'.format(', '.join([repr(x) for x in definition['values'].keys()])))

            w.write('\n  @Values: ->')
            w.write('\n    return [{}]\n'.format(', '.join([repr(self._get_value_for(x)) for x in definition['values'].values()])))

            w.write('\n  @Count: ->')
            w.write('\n    return {}\n'.format(len(definition['values'].keys())))

            if not definition.get('flags', False):
                w.write('\n  @Min: ->')
                w.write('\n    return {!r}\n'.format(min([self._get_value_for(x) for x in definition['values'].values()])))
                w.write('\n  @Max: ->')
                w.write('\n    return {!r}\n'.format(max([self._get_value_for(x) for x in definition['values'].values()])))
            else:
                allofem=0
                for x in definition['values'].values():
                    allofem |= self._get_value_for(x)
                w.write('\n  @All: ->')
                w.write('\n    #  b{0:032b}'.format(allofem))
                w.write('\n    # 0x{0:0X}'.format(allofem))
                w.write('\n    return {}\n'.format(allofem))

        self.touch(self.target)
