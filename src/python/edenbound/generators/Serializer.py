import os
import codecs
import yaml
import enum
import shutil
import json
from buildtools import log, os_utils
from buildtools.indentation import IndentWriter
from buildtools.maestro.base_target import SingleBuildTarget

'''
@serializer-start VAR
@serializer-end
@deserializer-start VAR
@deserializer-end
@cloner-start VAR
@cloner-end

@serialize: [yes|no] [scalar|object=TYPE|map=VALUETYPE] [name=...] [new-instance=new...;] [required] [array] [pass-self] [...]

@gen-all-serializers
'''
DEFAULT_SERIALIZE = '''
  serialize: ->
    data = super()
    # @serializer-start: data
    # @serializer-end
    return data
'''

DEFAULT_DESERIALIZE = '''
  deserialize: (data) ->
    super(data)
    # @deserializer-start: data
    # @deserializer-end
    return
'''

DEFAULT_CLONE = '''
  clone: (c=null) ->
    # If c was left unspecified, we act as the originating caller.
    if c == null or c == undefined or typeof(c) == typeof(true)
      c = new @constructor()
    super(c)
    # @cloner-start: c
    # @cloner-end
    return c
'''

DEFAULT_EQ = '''
  equals: (other) ->
    if not super(other)
      return false
    # @equalizer-start: other
    # @equalizer-end
    return true
'''

class VariableMappingType(enum.IntEnum):
    SCALAR = 0
    OBJECT = 1
    MAP = 2

class VariableMapping(object):
    def __init__(self):
        self.ID=''
        self.Required=False
        self.Serialize=True
        self.SerializedName = ''
        self.VarType = VariableMappingType.SCALAR
        self.VarTypeName = None
        self.DefaultValue = None
        self.IsArray = False
        self.Equalizer =True
        self.PassSelf = False
        self.NewInstanceCode = None
        self._inNIC=False
        self._IsDefaultValueSet=False
        self.Clone = True
        self.line = ''
        self.ln=0
        self.filename=''
        self.Debug=False

    def autodetect_var(self, varname, default):
        self.ID=varname
        if not self._IsDefaultValueSet:
            self.DefaultValue = default
        if self.DefaultValue == '[]':
            self.IsArray = True
        elif self.DefaultValue == '{}':
            self.VarType = VariableMappingType.MAP
        if self.SerializedName == '':
            n=0
            for c in varname:
                n+=1
                if c in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
                    if n > 1:
                        c = '_'+c.lower()
                    else:
                        c = c.lower()
                elif c not in 'abcdefghijklmnopqrstuvwxyz':
                    c = '_'
                self.SerializedName += c

    def process_directive(self, directive):
        if self._inNIC:
            if directive.endswith(';'):
                self._inNIC=False
                directive=directive[:-1]
            self.NewInstanceCode+=' '+directive
        else:
            if directive == 'yes':
                self.Serialize=True
                self.Clone=True
            elif directive == 'no':
                self.Serialize=False
                self.Clone=False
                self.Equalizer = False
            elif directive == 'debug':
                self.Debug = True
            elif directive == 'clone-only':
                self.Serialize=False
                self.Clone=True
            elif directive == 'array':
                self.IsArray = True
            elif directive == 'pass-self':
                self.PassSelf = True
            elif directive == 'required':
                self.Required = True
                self.DefaultValue=None
                self._IsDefaultValueSet=True
            elif directive == 'no-cmp':
                self.Equalizer = False
            elif directive.startswith('clone='):
                self.Clone = directive[6:] in ['yes', 'true']
            elif directive.startswith('optional='):
                self.Required=False
                self.DefaultValue=directive[9:]
                self._IsDefaultValueSet=True
            elif directive.startswith('default='):
                self.DefaultValue=directive[8:]
                self._IsDefaultValueSet=True
            elif directive.startswith('name='):
                self.SerializedName = directive[5:]
            elif directive.startswith('object='):
                self.VarType = VariableMappingType.OBJECT
                self.VarTypeName = directive[7:]
            elif directive.startswith('map='):
                self.VarType = VariableMappingType.MAP
                self.VarTypeName = directive[4:]
            elif directive.startswith('new-instance='):
                self._inNIC=True
                if directive.endswith(';'):
                    self._inNIC=False
                    directive=directive[:-1]
                self.NewInstanceCode=directive[13:]
            elif directive == 'scalar':
                self.VarType = VariableMappingType.SCALAR
                self.VarTypeName = None

    def _startwrite(self, _w):
        w=IndentWriter(_w)
        w.indent_chars='  '
        w.indent_level=2
        return w

    def __str__(self):
        intvars={}
        intvars["ID"] = self.ID
        #intvars["_inNIC"] = self._inNIC
        #intvars["_IsDefaultValueSet"] = self._IsDefaultValueSet
        intvars["Clone"] = self.Clone
        intvars["DefaultValue"] = self.DefaultValue
        intvars["Equalizer"] = self.Equalizer
        intvars["IsArray"] = self.IsArray
        intvars["NewInstanceCode"] = self.NewInstanceCode
        intvars["PassSelf"] = self.PassSelf
        intvars["Required"] = self.Required
        intvars["Serialize"] = self.Serialize
        intvars["SerializedName"] = self.SerializedName
        intvars["VarType"] = self.VarType
        intvars["VarTypeName"] = self.VarTypeName
        intvars["line"] = self.line
        intvars["ln"] = self.ln
        intvars["filename"] = self.filename
        return f'#VarMapping{json.dumps(intvars)}'

    def write_cloner(self, _w, storage_varname):
        if not self.Clone:
            return
        w = self._startwrite(_w)
        if self.Debug:
            w.writeline(str(self))
        if self.VarType == VariableMappingType.SCALAR:
            w.writeline(f'{storage_varname}.{self.ID} = @{self.ID}')
        else:
            with w.writeline(f'if @{self.ID} != undefined and @{self.ID} != null'):
                clone_args = '@' if self.PassSelf else ''
                if self.IsArray:
                    w.writeline(f'{storage_varname}.{self.ID} = []')
                    with w.writeline(f'for _orig_obj in @{self.ID}'):
                        if self.VarType == VariableMappingType.OBJECT:
                            w.writeline(f'{storage_varname}.{self.ID}.push _orig_obj.clone({clone_args})')
                        else:
                            w.writeline(f'{storage_varname}.{self.ID}.push(clone(_orig_obj))')
                elif self.VarType == VariableMappingType.MAP:
                    w.writeline(f'{storage_varname}.{self.ID} = {{}}')
                    with w.writeline(f'for key of @{self.ID}'):
                        if self.VarTypeName == 'scalar':
                            w.writeline(f'{storage_varname}.{self.ID}[key] = clone @{self.ID}[key]')
                        else:
                            w.writeline(f'{storage_varname}.{self.ID}[key] = @{self.ID}[key].clone({clone_args})')
                else:
                    w.writeline(f'{storage_varname}.{self.ID} = @{self.ID}.clone({clone_args})')

    def write_serializer(self, _w, storage_varname):
        if not self.Serialize:
            return
        w = self._startwrite(_w)
        if self.Debug:
            w.writeline(str(self))
        if self.VarType == VariableMappingType.SCALAR:
            if self.DefaultValue is not None:
                if self.IsArray:
                    w.writeline(f'if @{self.ID} != null and @{self.ID}.length > 0')
                else:
                    w.writeline(f'if @{self.ID} != {self.DefaultValue}')
                w.indent_level+=1
            w.writeline(f'{storage_varname}[\'{self.SerializedName}\'] = @{self.ID}')
            if self.DefaultValue is not None:
                w.indent_level-=1
        else:
            with w.writeline(f'if @{self.ID} != undefined and @{self.ID} != null'):
                if self.IsArray:
                    w.writeline(f'{storage_varname}[\'{self.SerializedName}\'] = []')
                    with w.writeline(f'for _orig_obj in @{self.ID}'):
                        w.writeline(f'{storage_varname}[\'{self.SerializedName}\'].push _orig_obj.serialize()')
                elif self.VarType == VariableMappingType.MAP:
                    w.writeline(f'{storage_varname}[\'{self.SerializedName}\'] = {{}}')
                    with w.writeline(f'for key of @{self.ID}'):
                        if self.VarTypeName != 'scalar':
                            w.writeline(f'{storage_varname}[\'{self.SerializedName}\'][key] = _orig_obj[key].serialize()')
                        else:
                            w.writeline(f'{storage_varname}[\'{self.SerializedName}\'][key] = _orig_obj[key]')
                else:
                    w.writeline(f'{storage_varname}[\'{self.SerializedName}\'] = @{self.ID}.serialize()')

    def write_equalizer(self, _w, other_varname):
        if not self.Equalizer:
            return
        w = self._startwrite(_w)
        if self.Debug:
            w.writeline(str(self))
        else:
            w.writeline(f'# {self.ID}')
        if self.VarType == VariableMappingType.SCALAR:
            with w.writeline(f'if @{self.ID} != {other_varname}.{self.ID}'):
                w.writeline('return false')
        else:
            with w.writeline(f'if typeof(@{self.ID}) != typeof({other_varname}.{self.ID})'):
                w.writeline('return false')
            with w.writeline(f'if (@{self.ID} == null or @{self.ID} == undefined) != ({other_varname}.{self.ID} == null or {other_varname}.{self.ID} == undefined)'):
                w.writeline('return false')
            with w.writeline(f'if @{self.ID} != null and @{self.ID} != undefined and {other_varname}.{self.ID} != null and {other_varname}.{self.ID} != undefined'):
                if self.IsArray:
                    with w.writeline(f'if @{self.ID}.length != {other_varname}.{self.ID}.length'):
                        w.writeline('return false')
                    w.writeline(f'_i = 0')
                    with w.writeline(f'while _i < @{self.ID}.length'):
                        if self.VarTypeName != 'scalar':
                            with w.writeline(f'if not @{self.ID}[_i].equals {other_varname}.{self.ID}[_i]'):
                                w.writeline('return false')
                        else:
                            with w.writeline(f'if @{self.ID}[_i] != {other_varname}.{self.ID}[_i]'):
                                w.writeline('return false')
                        w.writeline('_i++')
                elif self.VarType == VariableMappingType.MAP:
                    with w.writeline(f'if Object.keys(@{self.ID}).length != Object.keys({other_varname}.{self.ID}).length'):
                        w.writeline('return false')
                    with w.writeline(f'for key of @{self.ID}'):
                        if self.VarTypeName != 'scalar':
                            with w.writeline(f'if not @{self.ID}[key].equals {other_varname}.{self.ID}[key]'):
                                w.writeline('return false')
                        else:
                            with w.writeline(f'if @{self.ID}[key] != {other_varname}.{self.ID}[key]'):
                                w.writeline('return false')
                else:
                    if self.VarTypeName != 'scalar':
                        with w.writeline(f'if not @{self.ID}.equals {other_varname}.{self.ID}'):
                            w.writeline('return false')
                    else:
                        with w.writeline(f'if @{self.ID} != {other_varname}.{self.ID}'):
                            w.writeline('return false')

    def getNewInstance(self, datavar, outvar):
        if self.VarType == VariableMappingType.SCALAR:
            return self.DefaultValue
        if self.NewInstanceCode is None:
            if self.VarTypeName is None:
                log.error('%s:%d: %s does not have VarTypeName set!', self.filename, self.ln, self.line.strip())
            assert self.VarTypeName is not None
            return 'new {}()'.format(self.VarTypeName)
        return self.NewInstanceCode.replace('{data}', datavar).replace('{out}', outvar)

    def write_deserializer(self, _w, storage_varname):
        if not self.Serialize:
            return
        w = self._startwrite(_w)
        if self.Debug:
            w.writeline(str(self))
        if not self.Required:
            w.writeline(f'if \'{self.SerializedName}\' of {storage_varname}')
            w.indent_level+=1
        else:
            with w.writeline(f'if \'{self.SerializedName}\' not of {storage_varname}'):
                w.writeline(f'console and console.error "DESERIALIZATION ERROR in #{{@constructor.name}}: {self.SerializedName} is missing from the serialized data!"')
                w.writeline(f'_deser_errs++')
        if self.VarType == VariableMappingType.SCALAR:
            w.writeline(f'@{self.ID} = {storage_varname}[\'{self.SerializedName}\']')
        else:
            if self.IsArray:
                w.writeline(f'@{self.ID} = []')
                with w.writeline(f'for _ser_obj in {storage_varname}[\'{self.SerializedName}\']'):
                    newi = self.getNewInstance('_ser_obj','_new_obj')
                    w.writeline(f'_new_obj={newi}')
                    nextline_args=['_ser_obj']
                    if self.PassSelf:
                        nextline_args+=['@']

                    w.writeline('_new_obj.deserialize '+', '.join(nextline_args))
                    w.writeline(f'@{self.ID}.push _new_obj')
            elif self.VarType == VariableMappingType.MAP:
                w.writeline(f'@{self.ID} = {{}}')
                with w.writeline(f'for _ser_key of {storage_varname}[\'{self.SerializedName}\']'):
                    if self.VarTypeName == 'scalar':
                        w.writeline(f'@{self.ID}[_ser_key] = {storage_varname}[\'{self.SerializedName}\'][_ser_key]')
                    else:
                        serobj = f'{storage_varname}[\'{self.SerializedName}\'][_ser_key]'
                        newi = self.getNewInstance(serobj,'_new_obj')
                        w.writeline(f'_new_obj={newi}')
                        nextline_args=[serobj]
                        if self.PassSelf:
                            nextline_args+=['@']
                        w.writeline('_new_obj.deserialize '+', '.join(nextline_args))
                        w.writeline(f'@{self.ID}[_ser_key] = _new_obj')

            else:
                serobj = f'{storage_varname}[\'{self.SerializedName}\']'
                newi=self.getNewInstance(serobj, '@'+self.ID)
                w.writeline(f'@{self.ID}={newi}')
                nextline_args=[serobj]
                if self.PassSelf:
                    nextline_args+=['@']
                w.writeline(f'@{self.ID}.deserialize '+', '.join(nextline_args))
        if not self.Required:
            w.indent_level-=1
            if self.DefaultValue is not None:
                with w.writeline('else'):
                    w.writeline(f'@{self.ID} = {self.DefaultValue}')




class SerializerGenerator(SingleBuildTarget):
    BT_LABEL = 'SERIALIZERS'
    def __init__(self, target, sourcefiles, dependencies=[]):
        super().__init__(target=target, files=sourcefiles, dependencies=dependencies)

    def processFile(self, filename):
        try:
            mappings={}
            next_mapping=None
            in_class=None
            in_ctor=False
            block_until=None
            storage_varname=None
            reprocess=True
            while reprocess == True:
                reprocess=False
                log.debug("Processing %s", filename)
                # UTF-8-SIG means no BOM.
                with open(filename+'.tmp', 'w', encoding='utf-8-sig') as w:
                    ln=0
                    with open(filename, 'r', encoding='utf-8-sig') as r:
                        for line in r:
                            ln+=1
                            sline=line.strip()
                            # Detect class start
                            # class Hurf (extends Derp)?
                            if line.startswith('class'):
                                mappings.clear()
                                in_ctor=False
                                block_until=None
                                storage_varname=None
                                in_class = line.split(' ')[1]
                            # Detect constructor.
                            if sline.endswith('->') or sline.endswith('=>'):
                                in_ctor = sline.startswith('constructor:')

                            # We're inside a constructor, start capturing vars.
                            if in_ctor:
                                # Handle @serialize.
                                if sline.startswith('# @serialize:'):
                                    next_mapping = VariableMapping()
                                    for d in sline.split(':')[1].split(' '):
                                        next_mapping.process_directive(d)
                                    next_mapping.line = line
                                    next_mapping.ln = ln
                                    next_mapping.filename = filename
                                if sline.startswith('@') and '=' in sline:
                                    if next_mapping is None:
                                        next_mapping = VariableMapping()
                                    var, default = sline[1:].split('=', 1)
                                    if '#' in default:
                                        default, _ = default.split('#', 1)
                                    varname = var.strip()
                                    next_mapping.autodetect_var(varname, default.strip())
                                    next_mapping.line = line
                                    next_mapping.ln = ln
                                    next_mapping.filename = filename
                                    mappings[varname] = next_mapping
                                    next_mapping = None
                            # NOT in constructor.
                            else: #in_ctor
                                if sline.startswith('# @serializer-start:'):
                                    storage_varname = sline.split(':')[1].strip()
                                    block_until = '# @serializer-end'
                                    w.write(line)
                                    continue
                                if sline == '# @serializer-end':
                                    w.write('    #AUTOGENERATED\n')
                                    for mapping in mappings.values():
                                        mapping.write_serializer(w,storage_varname)

                                if sline.startswith('# @deserializer-start:'):
                                    storage_varname = sline.split(':')[1].strip()
                                    block_until = '# @deserializer-end'
                                    w.write(line)
                                    continue
                                if sline == '# @deserializer-end':
                                    w.write('    #AUTOGENERATED\n')
                                    iw=IndentWriter(w)
                                    iw.indent_chars='  '
                                    iw.indent_level=2
                                    iw.writeline('_deser_errs = 0')
                                    for mapping in mappings.values():
                                        mapping.write_deserializer(w,storage_varname)
                                    with iw.writeline('if console and console.error and _deser_errs > 0'):
                                        iw.writeline(f'console and console.error "#{{_deser_errs}} DESERIALIZATION ERRORS in #{{@constructor.name}}: DATA: ", {storage_varname}')

                                if sline.startswith('# @cloner-start:'):
                                    storage_varname = sline.split(':')[1].strip()
                                    block_until = '# @cloner-end'
                                    w.write(line)
                                    continue
                                if sline == '# @cloner-end':
                                    w.write('    #AUTOGENERATED\n')
                                    for mapping in mappings.values():
                                        mapping.write_cloner(w,storage_varname)

                                if sline.startswith('# @equalizer-start:'):
                                    storage_varname = sline.split(':')[1].strip()
                                    block_until = '# @equalizer-end'
                                    w.write(line)
                                    continue
                                if sline == '# @equalizer-end':
                                    w.write('    #AUTOGENERATED\n')
                                    for mapping in mappings.values():
                                        mapping.write_equalizer(w,storage_varname)
                            if sline == '# @gen-serializer':
                                w.write(DEFAULT_SERIALIZE)
                                reprocess=True
                                continue
                            if sline == '# @gen-deserializer':
                                w.write(DEFAULT_DESERIALIZE)
                                reprocess=True
                                continue
                            if sline == '# @gen-cloner':
                                w.write(DEFAULT_CLONE)
                                reprocess=True
                                continue
                                continue
                            if sline == '# @gen-eq' or sline == '# @gen-equalizer':
                                w.write(DEFAULT_EQ)
                                reprocess=True
                                continue
                            if sline == '# @gen-all-serializers':
                                w.write(DEFAULT_SERIALIZE)
                                w.write(DEFAULT_DESERIALIZE)
                                w.write(DEFAULT_CLONE)
                                w.write(DEFAULT_EQ)
                                reprocess=True
                                continue
                            if block_until is not None and sline==block_until:
                                block_until = None
                            if block_until is None:
                                w.write(line)
                os.remove(filename)
                shutil.move(filename+'.tmp', filename)
        except Exception as e:
            log.error('In %s:', filename)
            raise e

    def build(self):
        for filename in self.files:
            self.processFile(filename)
