import os
from buildtools import os_utils
from buildtools.maestro.base_target import SingleBuildTarget


class WebifyTarget(SingleBuildTarget):
    BT_TYPE = 'Webify'
    BT_LABEL = 'Webify'

    def __init__(self, destination, source, dependencies=[], webify_base_path='bin/', webify_win32='webify-win-32.exe', webify_linux='webify-linux-x86_64'):
        executable = webify_win32 if os_utils.is_windows() else webify_linux
        self.webify = os.path.abspath(os.path.join(webify_base_path, executable))
        self.source = source
        self.destination = destination
        destbasename, _ = os.path.splitext(self.destination)
        _, srcext = os.path.splitext(self.source)
        self.intermediate_filename = destbasename + '.' + (srcext.strip('.'))
        super(WebifyTarget, self).__init__(destination, dependencies=dependencies, files=[os.path.abspath(__file__), self.source, self.webify])

    def get_config(self):
        return {
            'source': self.source,
            'dest': self.destination,
            'webify': self.webify,
            'intermediate_filename': self.intermediate_filename,
        }

    def provides(self):
        base,_ = os.path.splitext(self.intermediate_filename)
        o=[]
        for ext in ['eot', 'woff', 'ttf', 'svg']:
            o += [base + '.' + ext]
        return o

    def build(self):
        os_utils.ensureDirExists(os.path.dirname(self.destination), noisy=True)
        os_utils.single_copy(self.source, self.intermediate_filename, verbose=True)
        os_utils.cmd([self.webify, self.intermediate_filename], echo=True, critical=True)
