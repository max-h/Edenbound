﻿class StupidSugarcubeFormatter
  constructor: ->
    @LastChar=''
    @CurChar=''
    @LineNumber=0
    @Filename=0

    @InHTML=true
    @InSugarcubeStatement=false
    @InComment=false
    @StatementStack=[]

  format: (code, filename) ->
    for char in code
      @CurChar=char
      if @InComment
        if @LastChar+@CurChar=='%/'
          @InComment=no
      else
        if @LastChar+@CurChar=='/%'
          @Buffer=@Buffer.splice(0,-1)

class PassageEditor
  @THEME: 'twilight'
  constructor: ->
    @CodeMirror=null
    @CodeMirrorElement = null
    @CodeMirrorInput = null

  @Edit: (passage=null) ->
    window.currentEditedPassage = null
    if passage == null
      window.currentEditedPassage = Nylon.GetCurrentPassage()
    else
      window.currentEditedPassage = passage

    pe = new PassageEditor()
    dlgEditor = $('#passage-editor-dialog').dialog
      title: 'Passage Editor'
      autoOpen: false
      height: 600
      width: 800
      modal: true
      buttons:
        'Save': ->
          window.currentEditedPassage.text = pe.Value
          $(@).dialog('close')
          return
        'Cancel': ->
          $(@).dialog('close')
          return
    pe.Initialize  '#codemirror-editor', 'tiddlywiki', window.currentEditedPassage.text
    dlgEditor.dialog 'open'
    pe.Value = window.currentEditedPassage.text.toString()
    return pe

  Initialize: (jqselector, lang, contents='') ->
    @CodeMirror = CodeMirror.fromTextArea $(jqselector)[0],
      theme: PassageEditor.THEME
      value: contents
      mode:  lang
      lineNumbers: yes

    @CodeMirrorElement = @CodeMirror.getWrapperElement()
    @CodeMirrorInput   = @CodeMirror.getInputField()

  @property 'Value',
    get: ->
      return @CodeMirror.getValue()
    set: (v) ->
      return @CodeMirror.setValue(v)
