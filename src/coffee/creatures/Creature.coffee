﻿class Creature extends EdenObject
  @CurrentID: 0
  constructor: ->
    super()

    ###
    # Age of the creature in T-years.
    # @serialize: scalar required
    ###
    @Age = 0

    ###
    # Who you think you are, what you think you are, etc.
    ###
    @Mind = new Mind(@)

    ###
    # List of all body parts. Treated as a component box.
    # @serialize: array object=BodyPart pass-self new-instance=Effect.CreateAndDeserialize({data});
    ###
    @BodyParts = []

    ###
    # Reference to the species.
    # @serialize: name=species
    ###
    @SpeciesID = HumanSpecies.ID

    ###
    # Effects currently active.
    # @serialize: array object=Effect new-instance=Effect.CreateAndDeserialize({data}); pass-self
    ###
    @Effects = []

    ###
    # "A blue human!"
    # @serialize: scalar
    ###
    @Color = null

    ###
    # Height in cm
    # @serialize: scalar required default=0
    ###
    @Height = 182

    # IMPORTANT: Set these as though you had DR=1!
    # @serialize: object=Stat
    @Health = new Stat @, 'Health', 0, 100, false
    # @serialize: object=Stat
    @Special = new Stat @, 'Special', 0, 100, false

    # @serialize: object=DNA name=dna pass-self
    @DNA = null

    # @serialize: no no-cmp
    @Puck = null

    # @serialize: no no-cmp
    @UI={
      Health: new StatBar null, 'HP', 'Health'
      Special: new StatBar null, 'SP', 'Special Points'
    }

    ## Events

    # @serialize: clone-only no-cmp
    @PreGenerateOrgans = new Event()
    # @serialize: clone-only no-cmp
    @GenerateOrgans = new Event()
    # @serialize: clone-only no-cmp
    @PostGenerateOrgans = new Event()


    # @serialize: clone-only no-cmp
    @AddingBodyPart = new Event()
    # @serialize: clone-only no-cmp
    @AddedBodyPart = new Event()

    # @serialize: clone-only no-cmp
    @RemovingBodyPart = new Event()
    # @serialize: clone-only no-cmp
    @RemovedBodyPart = new Event()

    ###
    # Called during GetBodyPlan.  Allows effects and genes to screw with BodyPlan.
    # @serialize: clone-only no-cmp
    ###
    @ReceivedBodyPlan = new Event()

    ###
    # Called after Apparent Gender votes are tallied.
    # @serialize: clone-only no-cmp
    ###
    @TalliedAGVotes = new Event()

    @Hostile = false

    # @serialize: object=ClothingController pass-self required
    @Clothing = new ClothingController()

  ###
  # Tells genetics to generate organs.
  ###
  Generate: ->
    # DNA generated?
    @InitializeDNA reinit=no

    # Generate all organs
    @PreGenerateOrgans.Fire(@)
    @GenerateOrgans.Fire(@)
    @PostGenerateOrgans.Fire(@)

    return

  ApparentGenderWording: (masculine, feminine, neutral=undefined) ->
    switch @GetApparentGender()
      when EGenderVote.MASCULINE
        return masculine
      when EGenderVote.FEMININE
        return feminine
    return neutral or masculine

  UpdateStats: (full=no)->
    if full and @Puck != null
      @Puck.remove()
      @Puck = null

    if @Puck == null
      @_BuildPuck full

      @UI.Health.LinkStat @Health
      @UI.Special.LinkStat @Special

    # Yes, this needs to be redundant. Don't fuck with it, me. - VG
    if @Puck != null
      name = @Mind.Identity.FirstName+" "+@Mind.Identity.LastName

      gsymb = @Puck.find('span.gender-symb')
      symb = ''
      $player = getVariables().player

      switch @GetApparentGender()
        when EGenderVote.MASCULINE
          symb = 'symbol-male'
        when EGenderVote.FEMININE
          symb = 'symbol-female'
        else
          symb = 'symbol-neuter'

      if not $player.equals @
        # If we've met this guy
        if $player.Mind.Memory.NPCs.Met @
          # Rely on our memory.
          npcmem = $player.Mind.Memory.NPCs.GetMemoryOf @
          symb = npcmem.GetSexSymbol()
          name = npcmem.GetName()
          npcmem.ApplyClassesTo @, gsymb
      else # Oh we're talking about the player
        name = "You"

      @Puck.find('span.name').text name
      @Puck.find('span.level').text @Mind.Stats.Level.Value
      @Puck.find('span.xp').text @Mind.Stats.Experience.Value

      gsymb.html Nylon.GetPassageByID(symb).text

      max=0
      dam=0
      for bp in @BodyParts
        max += bp.MaxHP
        dam += bp.GetDamage()
      if @Health.Max != max
        @Health.Max = max
        @Health.Value = dam

      @Health.Set max - dam

      @UI.Health.Refresh()
      @UI.Special.Refresh()
    return


  _FindPuckParent: ->
    if @Hostile
      return $ '#storyMenu div#enemy-partymembers'
    else
      return $ '#storyMenu div#player-partymembers'

  _FindPucksInTarget: (target) ->
    return target.find "#creature-puck-#{@CreatureID}"

  _BuildPuck: (full_rebuild=no) ->
    console.log "Building puck for #{@Mind.Identity.FirstName} (full_rebuild=#{full_rebuild})"
    target = @_FindPuckParent()
    if target.length == 0
      console.error "Unable to find target.", target
      @Puck = null
      return

    foundPucks = @_FindPucksInTarget(target)
    if foundPucks.length > 0
      @Puck = null
      for puck, idx in foundPucks
        if idx > 0 or full_rebuild
          puck.remove()
      if !full_rebuild
        @Puck = $ foundPucks[0]

    if @Puck == null
      console.log "Creating puck from scratch!"
      @Puck = $ '<div class="partymember-puck"></div>'
      @Puck.attr 'id', "creature-puck-#{@CreatureID}"
      target.append @Puck

      metabar = $ '<div class="meta"></div>'
      @Puck.append metabar
      metabar.append $ '<span class="name"></span>'
      metabar.append $ '<span class="gender-symb"></span>'
      # @classref: Nylon
      metabar.append $ '<span class="examine">'+Nylon.GetPassageByID('symbol-magnifying-glass').text+'</span>'

      @Puck.append $ '<div class="level"><sub>LVL</sub><span class="level"></span></div>'
      @Puck.append $ '<div class="xp"><sub>XP</sub><span class="xp"></span></div>'
      @Puck.append $ '<div class="stats"></div>'

    console.log "@Puck=",@Puck
    stats = @Puck.find('.stats')
    stats.html ''
    stats.append @UI.Health.BuildHTML().attr('id',"statbar_hp")
    stats.append @UI.Special.BuildHTML().attr('id',"statbar_sp")
    return

  @property 'Weight',
    get: ->
      o = 0
      for bp in @BodyParts
        if bp != null
          o += bp.GetWeight()
      return o

  ###
  # Used for descriptions.
  ###
  GetBodyPartGroups: (clsID) ->
    bplist = @GetAllBodyPartsOfClass(clsID)
    if bplist.length == 0
      return []
    out=[]
    while bplist.length > 0
      oldlen = bplist.length
      bpg = new BodyPartGroup()
      bplist = bpg.CollectFrom bplist
      out.push bpg
      if oldlen == bplist.length
        break
    return out

  ###
  # Get the apparent gender of a Creature.
  #
  # This is a democratic system, where each body component has a "vote".
  # Votes are tallied and the one with the most votes wins.
  #
  # Ties result in an "Androgenous" gender.
  ###
  GetApparentGender: ->
    # @enumref: EGenderVote
    votes = {}
    votes[EGenderVote.MASCULINE] = 0
    votes[EGenderVote.FEMININE] = 0

    for bp in @GetVisibleBodyParts()
      g = bp.GetApparentGender()
      if g == EGenderVote.ANDROGYNOUS
        continue
      votes[g]++

    # Allow genes and effects to stuff the ballot box.
    @TalliedAGVotes.Fire votes

    m_votes = votes[EGenderVote.MASCULINE]
    f_votes = votes[EGenderVote.FEMININE]

    if m_votes + f_votes > 0
      if m_votes > f_votes
        return EGenderVote.MASCULINE
      if m_votes < f_votes
        return EGenderVote.FEMININE
    return EGenderVote.ANDROGYNOUS


  clone: (c=null) ->
    # If c was left unspecified, we act as the originating caller.
    if c == null or c == true or c == undefined
      c = new @constructor()
    super(c)
    # @cloner-start: c
    #AUTOGENERATED
    c.Age = @Age
    c.Mind = @Mind
    if @BodyParts != undefined and @BodyParts != null
      c.BodyParts = []
      for _orig_obj in @BodyParts
        c.BodyParts.push _orig_obj.clone(@)
    c.SpeciesID = @SpeciesID
    if @Effects != undefined and @Effects != null
      c.Effects = []
      for _orig_obj in @Effects
        c.Effects.push _orig_obj.clone(@)
    c.Color = @Color
    c.Height = @Height
    if @Health != undefined and @Health != null
      c.Health = @Health.clone()
    if @Special != undefined and @Special != null
      c.Special = @Special.clone()
    if @DNA != undefined and @DNA != null
      c.DNA = @DNA.clone(@)
    c.PreGenerateOrgans = @PreGenerateOrgans
    c.GenerateOrgans = @GenerateOrgans
    c.PostGenerateOrgans = @PostGenerateOrgans
    c.AddingBodyPart = @AddingBodyPart
    c.AddedBodyPart = @AddedBodyPart
    c.RemovingBodyPart = @RemovingBodyPart
    c.RemovedBodyPart = @RemovedBodyPart
    c.ReceivedBodyPlan = @ReceivedBodyPlan
    c.TalliedAGVotes = @TalliedAGVotes
    c.Hostile = @Hostile
    if @Clothing != undefined and @Clothing != null
      c.Clothing = @Clothing.clone(@)
    # @cloner-end
    return c

  serialize: ->
    data = super()
    # @serializer-start: data
    #AUTOGENERATED
    data['age'] = @Age
    if @Mind != new Mind(@)
      data['mind'] = @Mind
    if @BodyParts != undefined and @BodyParts != null
      data['body_parts'] = []
      for _orig_obj in @BodyParts
        data['body_parts'].push _orig_obj.serialize()
    if @SpeciesID != HumanSpecies.ID
      data['species'] = @SpeciesID
    if @Effects != undefined and @Effects != null
      data['effects'] = []
      for _orig_obj in @Effects
        data['effects'].push _orig_obj.serialize()
    if @Color != null
      data['color'] = @Color
    if @Height != 0
      data['height'] = @Height
    if @Health != undefined and @Health != null
      data['health'] = @Health.serialize()
    if @Special != undefined and @Special != null
      data['special'] = @Special.serialize()
    if @DNA != undefined and @DNA != null
      data['dna'] = @DNA.serialize()
    if @Hostile != false
      data['hostile'] = @Hostile
    if @Clothing != undefined and @Clothing != null
      data['clothing'] = @Clothing.serialize()
    # @serializer-end
    return data

  deserialize: (data) ->
    super(data)
    # @deserializer-start: data
    #AUTOGENERATED
    _deser_errs = 0
    if 'age' not of data
      console and console.error "DESERIALIZATION ERROR in #{@constructor.name}: age is missing from the serialized data!"
      _deser_errs++
    @Age = data['age']
    if 'mind' of data
      @Mind = data['mind']
    else
      @Mind = new Mind(@)
    if 'body_parts' of data
      @BodyParts = []
      for _ser_obj in data['body_parts']
        _new_obj=Effect.CreateAndDeserialize(_ser_obj)
        _new_obj.deserialize _ser_obj, @
        @BodyParts.push _new_obj
    else
      @BodyParts = []
    if 'species' of data
      @SpeciesID = data['species']
    else
      @SpeciesID = HumanSpecies.ID
    if 'effects' of data
      @Effects = []
      for _ser_obj in data['effects']
        _new_obj=Effect.CreateAndDeserialize(_ser_obj)
        _new_obj.deserialize _ser_obj, @
        @Effects.push _new_obj
    else
      @Effects = []
    if 'color' of data
      @Color = data['color']
    else
      @Color = null
    if 'height' not of data
      console and console.error "DESERIALIZATION ERROR in #{@constructor.name}: height is missing from the serialized data!"
      _deser_errs++
    @Height = data['height']
    if 'health' of data
      @Health=new Stat()
      @Health.deserialize data['health']
    else
      @Health = new Stat @, 'Health', 0, 100, false
    if 'special' of data
      @Special=new Stat()
      @Special.deserialize data['special']
    else
      @Special = new Stat @, 'Special', 0, 100, false
    if 'dna' of data
      @DNA=new DNA()
      @DNA.deserialize data['dna'], @
    else
      @DNA = null
    if 'hostile' of data
      @Hostile = data['hostile']
    else
      @Hostile = false
    if 'clothing' not of data
      console and console.error "DESERIALIZATION ERROR in #{@constructor.name}: clothing is missing from the serialized data!"
      _deser_errs++
    @Clothing=new ClothingController()
    @Clothing.deserialize data['clothing'], @
    if console and console.error and _deser_errs > 0
      console and console.error "#{_deser_errs} DESERIALIZATION ERRORS in #{@constructor.name}: DATA: ", data
    # @deserializer-end
    return


  equals: (other) ->
    if not super(other)
      return false
    # @equalizer-start: other
    #AUTOGENERATED
    # Age
    if @Age != other.Age
      return false
    # Mind
    if @Mind != other.Mind
      return false
    # BodyParts
    if typeof(@BodyParts) != typeof(other.BodyParts)
      return false
    if (@BodyParts == null or @BodyParts == undefined) != (other.BodyParts == null or other.BodyParts == undefined)
      return false
    if @BodyParts != null and @BodyParts != undefined and other.BodyParts != null and other.BodyParts != undefined
      if @BodyParts.length != other.BodyParts.length
        return false
      _i = 0
      while _i < @BodyParts.length
        if not @BodyParts[_i].equals other.BodyParts[_i]
          return false
        _i++
    # SpeciesID
    if @SpeciesID != other.SpeciesID
      return false
    # Effects
    if typeof(@Effects) != typeof(other.Effects)
      return false
    if (@Effects == null or @Effects == undefined) != (other.Effects == null or other.Effects == undefined)
      return false
    if @Effects != null and @Effects != undefined and other.Effects != null and other.Effects != undefined
      if @Effects.length != other.Effects.length
        return false
      _i = 0
      while _i < @Effects.length
        if not @Effects[_i].equals other.Effects[_i]
          return false
        _i++
    # Color
    if @Color != other.Color
      return false
    # Height
    if @Height != other.Height
      return false
    # Health
    if typeof(@Health) != typeof(other.Health)
      return false
    if (@Health == null or @Health == undefined) != (other.Health == null or other.Health == undefined)
      return false
    if @Health != null and @Health != undefined and other.Health != null and other.Health != undefined
      if not @Health.equals other.Health
        return false
    # Special
    if typeof(@Special) != typeof(other.Special)
      return false
    if (@Special == null or @Special == undefined) != (other.Special == null or other.Special == undefined)
      return false
    if @Special != null and @Special != undefined and other.Special != null and other.Special != undefined
      if not @Special.equals other.Special
        return false
    # DNA
    if typeof(@DNA) != typeof(other.DNA)
      return false
    if (@DNA == null or @DNA == undefined) != (other.DNA == null or other.DNA == undefined)
      return false
    if @DNA != null and @DNA != undefined and other.DNA != null and other.DNA != undefined
      if not @DNA.equals other.DNA
        return false
    # Hostile
    if @Hostile != other.Hostile
      return false
    # Clothing
    if typeof(@Clothing) != typeof(other.Clothing)
      return false
    if (@Clothing == null or @Clothing == undefined) != (other.Clothing == null or other.Clothing == undefined)
      return false
    if @Clothing != null and @Clothing != undefined and other.Clothing != null and other.Clothing != undefined
      if not @Clothing.equals other.Clothing
        return false
    # @equalizer-end
    return true

  GetVisibleBodyParts: ->
    # TODO
    return @BodyParts

  GetAllBodyPartsOfClass: (clsID) ->
    r=[]
    for bp in @BodyParts
      if bp.Class == clsID
        r.push bp
    return r

  InitializeDNA: (reinit=no)->
    if !@DNA or reinit
      if @DNA and reinit
        @DNA.Detach @ # Unhook events, if needed.
      dna_type = @GetSpecies().DNA
      @DNA = new dna_type @ # @depdetect: ignore
      @DNA.Fill()
      @DNA.Attach @ # Hook events

  # For NPCs and initial player character generation.
  RollNewCharacter: (species=HumanSpecies.ID) ->
    console.log "Rolling new character with species=#{species}!"

    # Set up a new mind with random stuff in it.
    @Mind = new Mind(@)

    @SetSpeciesID species

    @InitializeDNA reinit=yes

    @Mind.RollNewMind()

    # This does some important stuff, like select gender.
    @DNA.RollNewSequence()

    # Have the species set us up, no TF messages.
    @GetSpecies().InitialBodySetup @

    # Record our original setup.
    @Mind.Identity.OriginalSpecies = @SpeciesID

    # Body parts and stuff
    @Generate()

    # Update pronouns.
    @Mind.ReassessPronouns()
    return

  GetSpecies: ->
    return Species.GetByID(@SpeciesID)

  ###
  # Doesn't perform TF messages.
  ###
  SetSpeciesID: (val) ->
    if val instanceof Species
      @SpeciesID = val.ID
    if val instanceof String
      @SpeciesID = val

  GetBiologicalSex: ->
    if @DNA != null and @DNA.GenderGene != null
      return @DNA.GenderGene.GetBiologicalSex()
    return EGenderVote.ANDROGYNOUS

  GetBiologicalSexRaw: ->
    if @DNA != null and @DNA.GenderGene != null
      return @DNA.GenderGene.Value
    return 'ERROR'

  HasBodyPart: (cls) ->
    for bodypart in @BodyParts
      if bodypart == cls or bodypart instanceof cls
        return yes
    return no

  AddBodyPart: (part) ->
    if typeof part == 'function'
      part = new part() # @depdetect: ignore
    @AddingBodyPart.Fire part
    @BodyParts.push part
    part.Attach @
    @AddedBodyPart.Fire part

  RemoveBodyPart: (part) ->
    if part in @BodyParts
      @RemovingBodyPart.Fire part
      part.Detach @
      @BodyParts = @BodyParts.filter (element) ->
        return part != element
      @RemovedBodyPart.Fire part

  GetBodyPlan: ->
    return @GetSpecies().GetBodyPlanFor @


  ###
  # Adds {count} wounds to {bpcls} bodyparts. WoundFlags are XOR'd with wound_flags, weap_flags are used to find appropriate wounds.
  # @enumref: EWoundFlags
  # @enumref: EWeaponFlags
  # @enumref: EBodyPartClass
  ###
  AddWounds: (bpcls, count, wound_flags=EWoundFlags.NONE, weap_flags=EWeaponFlags.BLUNT) ->
    arglist = [
      "EBodyPartClass."+EBodyPartClass.ValueToString(bpcls),
      count.toString(),
      "EWoundFlags."+EWoundFlags.ValueToString(wound_flags),
      "EWeaponFlags."+EWeaponFlags.ValueToString(wound_flags)
    ]
    argdbg = "@AddWounds "+arglist.join(', ')
    console.log argdbg

    left = count
    while left > 0
      bpcoll = @GetAllBodyPartsOfClass(bpcls)
      if bpcoll.length == 0
        console.warn "Unable to AddWounds: bpcoll.length == 0. "+argdbg
        break
      bp = either bpcoll
      # @classref: Wound
      wcoll = Wound.TypeListByWeaponFlags(weap_flags)
      if wcoll.length == 0
        console.warn "Unable to AddWounds: wcoll.length == 0. "+argdbg
        break
      w_class = either wcoll
      wound = new w_class()
      wound.Flags ^= wound_flags
      console.log "Adding wound: ",wound
      bp.AddWound wound
      left--
    return

  ShowWeightBreakdown: ->
    weight = @Weight
    txt = "<p>Weight breakdown for #{@ID}: (#{UnitConvert.Mass.Medium(weight)})</p><table><tr><th>BodyPart</th><th>Weight (kg)</th><th>%</th></tr>"
    for bp in @BodyParts
      bpw = bp.GetWeight()
      txt += "<tr><th>#{bp.NameSingular}</th>"
      txt += "<td>#{bpw}</td>"
      txt += "<td>#{Math.round((bpw/weight)*100, 2)}%</td>"
      txt += "</tr>"
    txt += "</table>"
    JQConfirm
      title: "Weight Breakdown: #{@Mind.Identity.FirstName} #{@Mind.Identity.LastName}"
      text: txt

  SetGenderPreset: (g) ->
    @DNA.GenderGene.SetChromosomeByGenderPreset g
    @GetSpecies().InitialBodySetup @
    @Mind.Identity.OriginalSex = @DNA.GenderGene.DisplayText
    @Mind.ReassessPronouns()
    return
