﻿class HumanSpecies extends HumanoidSpecies
  @ID: 'human'
  constructor: ->
    super()
    @Name = 'Human'
    @ID = 'human'
    @Portrait = 'human'
    # @enumref: ESpeciesFlags
    @Flags = ESpeciesFlags.DEFAULTS_UNLOCKED | ESpeciesFlags.HAS_SEXES
    # @classref: HumanDNA
    @DNA = HumanDNA

    # https://en.wikipedia.org/wiki/Human_height#History_of_human_height
    @MinHeight = 55 # cm The shortest adult human on record was Chandra Bahadur Dangi of Nepal at 54.6 cm (1 ft 9.5 in).
    @MaxHeight = 272 # cm, Robert Pershing Wadlow

    @Skin.Types = [HumanSkinBodyPart]

  GenClothingSlotGroups: (creature, clothing) ->
    # Humans are bilaterally symmetrical.
    LR = [
      'Left'
      'Right'
    ]
    clothing.SlotGroups[EClothingSlot.EAR_RING].AvailableSlotNames = LR
    clothing.SlotGroups[EClothingSlot.RING].AvailableSlotNames = LR
    clothing.SlotGroups[EClothingSlot.ARM].AvailableSlotNames = LR
    clothing.SlotGroups[EClothingSlot.HAND].AvailableSlotNames = LR
    clothing.SlotGroups[EClothingSlot.SHOE].AvailableSlotNames = LR

    return

Species.Register(HumanSpecies)
