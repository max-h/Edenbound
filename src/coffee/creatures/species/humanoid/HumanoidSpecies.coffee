﻿###
# A species with a humanoid body plan.
###
class HumanoidSpecies extends Species
  constructor: ->
    super()

    # Convenient accessors.
    @Skin  = new SkinSpec()
    @Heads = new HeadSpec()
    @Ears  = new EarSpec()
    @Eyes  = new EyeSpec()
    @Arms  = new ArmSpec()
    @Legs  = new LegSpec()
    @Torso = new TorsoSpec()

    # Male body parts
    @Penises = new PenisSpec()
    @Testicles = new TesticleSpec()

    # Female body parts
    @Vaginas = new VaginaSpec()
    @Wombs = new WombSpec()
    @Ovaries = new OvarySpec()

    @BodyPlan = [
      @Skin
      @Heads
      @Ears
      @Eyes
      @Arms
      @Legs
      @Torso
    ]

    # @classref: HumanoidTorso
    @Torso.Types = [HumanoidTorso] # $playerBody new HumanoidTorso
    @Ears.SetCount 2 # $playerEarNumber
    @Eyes.SetCount 2 # $playerEyeNumber
    @Arms.SetCount 2 # $playerArmNumber
    @Legs.SetCount 2 # $playerLegNumber
    @Testicles.SetCount 2
    @Ovaries.SetCount 2
