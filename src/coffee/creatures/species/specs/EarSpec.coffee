﻿class EarSpec extends BodyPartSpec
  constructor: ->
    super()
    # @classref: EarBodyPart
    @Types = [EarBodyPart]
    # @enumref: EBodyPartClass
    @Class = EBodyPartClass.EAR
    # @enumref: EBodyPartLocation
    @Locations = [EBodyPartLocation.HEAD]
