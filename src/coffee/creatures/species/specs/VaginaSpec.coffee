﻿# @enumref: EBodyPartClass
# @enumref: EBodyPartLocation
class VaginaSpec extends BodyPartSpec
  constructor: ->
    super()
    # @classref: VaginaBodyPart
    @Types = [VaginaBodyPart]
    @Class = EBodyPartClass.VAGINA
    @Locations = [EBodyPartLocation.TORSO]
