﻿class ArmSpec extends BodyPartSpec
  constructor: ->
    super()
    # @classref: ArmBodyPart
    @Types = [ArmBodyPart]
    # @enumref: EBodyPartClass
    @Class = EBodyPartClass.ARM
    # @enumref: EBodyPartLocation
    @Locations = [EBodyPartLocation.TORSO]
