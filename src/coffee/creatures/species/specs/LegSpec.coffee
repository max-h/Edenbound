﻿# @enumref: EBodyPartClass
# @enumref: EBodyPartLocation
class LegSpec extends BodyPartSpec
  constructor: ->
    super()
    # @classref: LegBodyPart
    @Types = [LegBodyPart]
    @Class = EBodyPartClass.LEG
    @Locations = [EBodyPartLocation.TORSO]
