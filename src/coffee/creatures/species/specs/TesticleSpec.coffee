﻿# @enumref: EBodyPartClass
# @enumref: EBodyPartLocation
class TesticleSpec extends BodyPartSpec
  constructor: ->
    super()
    # @classref: TesticleBodyPart
    @Types = [TesticleBodyPart]
    @Class = EBodyPartClass.TESTICLE
    @Locations = [EBodyPartLocation.TORSO]
