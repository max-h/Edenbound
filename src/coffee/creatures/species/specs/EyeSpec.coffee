﻿class EyeSpec extends BodyPartSpec
  constructor: ->
    super()
    # @classref: EyeBodyPart
    @Types = [EyeBodyPart]
    # @enumref: EBodyPartClass
    @Class = EBodyPartClass.EYE

    @Color = 'brown'
    # @enumref: EBodyPartLocation
    @Locations = [EBodyPartLocation.HEAD]
    
