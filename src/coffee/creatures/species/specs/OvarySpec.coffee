﻿# @enumref: EBodyPartClass
# @enumref: EBodyPartLocation
class OvarySpec extends BodyPartSpec
  constructor: ->
    super()
    # @classref: OvaryBodyPart
    @Types = [OvaryBodyPart]
    @Class = EBodyPartClass.OVARY
    @Locations = [EBodyPartLocation.TORSO]
