﻿# @enumref: EBodyPartClass
# @enumref: EBodyPartLocation
class WombSpec extends BodyPartSpec
  constructor: ->
    super()
    # @classref: WombBodyPart
    @Types = [WombBodyPart]
    @Class = EBodyPartClass.WOMB
    @Locations = [EBodyPartLocation.TORSO]
