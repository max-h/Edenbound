﻿class TorsoSpec extends BodyPartSpec
  constructor: ->
    super()
    # @classref: TorsoBodyPart
    @Types = [TorsoBodyPart]
    @Class = EBodyPartClass.TORSO
    @Locations = [""]
