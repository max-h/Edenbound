﻿class AttachPoint extends GameObject
  constructor: ->
    super()
    # @serialize: name=id
    @ID = ''
    @Name = ''
    ###
    # References to BodyParts in Creature.
    # @serialize: array scalar
    ###
    @Children=[]
    #@Offset=[]


  serialize: ->
    data = super()
    # @serializer-start: data
    #AUTOGENERATED
    if @ID != ''
      data['id'] = @ID
    if @Name != ''
      data['name'] = @Name
    if @Children != null and @Children.length > 0
      data['children'] = @Children
    # @serializer-end
    return data

  deserialize: (data) ->
    super(data)
    # @deserializer-start: data
    #AUTOGENERATED
    _deser_errs = 0
    if 'id' of data
      @ID = data['id']
    else
      @ID = ''
    if 'name' of data
      @Name = data['name']
    else
      @Name = ''
    if 'children' of data
      @Children = data['children']
    else
      @Children = []
    if console and console.error and _deser_errs > 0
      console and console.error "#{_deser_errs} DESERIALIZATION ERRORS in #{@constructor.name}: DATA: ", data
    # @deserializer-end
    return

  clone: (c=null) ->
    # If c was left unspecified, we act as the originating caller.
    if c == null or c == true or c == undefined
      c = new @constructor()
    super(c)
    # @cloner-start: c
    #AUTOGENERATED
    c.ID = @ID
    c.Name = @Name
    c.Children = @Children
    # @cloner-end
    return c
