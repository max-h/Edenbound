﻿###
# This is basically the part of your DNA that defines what it is to be a member of your species.
#
# Static object, no need to fuck around with serialization
###
class Species
  @ALL_SPECIES: {}

  constructor: ->
    @ID = ''
    @Name = ''
    @Blurb = ''
    @Portrait = ''
    @PossibleSubspecies = []
    # @enumref: ESpeciesFlags
    @Flags = ESpeciesFlags.NONE

    @DNA = null

    # cm
    @MinHeight = 0
    @MaxHeight = 100

    ###
    # Possible {Item}s presented by GetLoot().
    ###
    @PossibleLoot=[]

    ###
    # Possible colors.
    ###
    @Colors = []

    ###
    # Bodyplan stuff
    ###
    @BodyPlan = [
      #@Heads
      #@Ears
      #@Eyes
      #@Arms
      #@Legs
      #@Torso
      # Additional organs go here.
    ]

  @Register: (speciesFunc) ->
    spec = new speciesFunc() # @depdetect: ignore
    @ALL_SPECIES[spec.ID]=spec

  @GetByID: (ID) ->
    return @ALL_SPECIES[ID]

  GetBodyPlanFor: (creature) ->
    r = []
    for bp in @BodyPlan
      if bp!=null
        r.push bp.clone()

    # vh is a holder for the list of BodyPartSpecs. Allows it to be passed by-ref.
    vh =
      BodyPlan: r
    creature.ReceivedBodyPlan.Fire(vh)
    r = vh.BodyPlan

    return r

  Become: (creature, skip_tf=false) ->
    creature.Species = @
    if @Colors.length > 0
      creature.Color = either @Colors
    plan  = @GetBodyPlanFor(creature)

    if plan == undefined
      console.error 'BUG: plan === undefined! Expect bugs!'
      return
    if plan.length == 0
      console.error 'BUG: plan is empty! Expect bugs!'
      return
    for spec in plan
      if spec != null
        [added,removed] = spec.ApplyTo creature, skip_tf
    return

  InitialBodySetup: (creature) ->
    @Become creature, true

  GenClothingSlotGroups: (creature, clothing) ->
    for sg in Object.values(clothing.SlotGroups)
      sg.SlotsProvided=0
    for bp in creature.BodyParts
      if bp.ClothingSlot != null
        ct = bp.ClothingSlot.SlotType
        sg = null
        if ct not of clothing.SlotGroups
          sg = new ClothingSlotGroup()
          sg.SlotType=ct
          sg.Creature=creature
          clothing.SlotGroups[ct] = sg
        else
          sg = clothing.SlotGroups[ct]
        sg.SlotsProvided++
        sg.BodyPartSlots.push bp.ClothingSlot
    @GenerateClothingSlots(creature, clothing)
    return

  GenClothingSlotGroups: (creature, clothing)->
    for sg in Object.values(clothing.SlotGroups)
      # More slots than there are providers
      while sg.SlotsProvided < sg.Slots
        id = either Object.keys(sg.Slots)
        slot = sg.Slots[id]
        if slot.Clothing != null
          slot.Clothing.Drop()
        slot.Clothing=null
        delete sg.Slots[id]
      # More providers than there are slots.
      # This will be slow, obviously.
      while sg.SlotsProvided > sg.Slots
        for bp in sg.BodyParts
          found=false
          for cs in Object.values(sg.Slots)
            if bp.ClothingSlot.equals cs
              found = true
              break
          if found
            continue
          sg.Slots.push bp.ClothingSlot.clone()
          break
    return
