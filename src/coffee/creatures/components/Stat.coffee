﻿STATTYPE_NUMERIC=0
STATTYPE_STRING=1

class Stat extends GameObject
  constructor: (@Creature, @Name, @Min, @Max, @Bad=false) ->
    super()
    @Value=@Min

    @Type=STATTYPE_NUMERIC

    @AlertLevel=0
    @Alert=false

    #EVENTS
    @OnChanged = new Event()
    @OnAlert = new Event()

    # Called when Get() is called, events return number defining the modification
    # they're making against @Value.
    # :param stat: This Stat.
    @OnGet = new Event()

  clone: (clone_creature) ->
    clone = new Stat clone_creature, @Name, @Min, @Max, @Bad
    clone.Value = @Value
    clone.Type = @Type
    clone.AlertLevel = @AlertLevel
    clone.Alert = @Alert

    # Direct transfer events.
    clone.OnChanged = @OnChanged
    clone.OnAlert = @OnAlert
    clone.OnGet = @OnGet
    return clone

  serialize: ->
    data = super()
    #data.Creature = @Creature
    data.Name = @Name
    data.Min=@Min
    data.Max=@Max
    data.Bad=@Bad
    data.Value=@Value
    data.Type=@Type
    data.AlertLevel = @AlertLevel
    data.Alert=@Alert
    return data

  deserialize: (data, @Creature) ->
    super(data)
    #if 'Creature' of data
    #  @Creature = data.Creature
    if 'Name' of data
      @Name = data.Name
    if 'Min' of data
      @Min = data.Min
    if 'Max' of data
      @Max = data.Max
    if 'Bad' of data
      @Bad = data.Bad
    if 'Value' of data
      @Value = data.Value
    if 'Type' of data
      @Type = data.Type
    if 'AlertLevel' of data
      @AlertLevel = data.AlertLevel
    if 'Alert' of data
      @Alert = data.Alert
    return data

  Set: (newVal) =>
    oldValue=@Get()
    if newVal < @Min
      newVal = @Min
    if newVal > @Max
      newval = @Max
    @Value=newVal
    if @Bad
      @Alert = @Value > @AlertLevel
    else
      @Alert = @Value < @AlertLevel
    @OnChanged.Fire(@,oldValue)

  Get: =>
    mod=0
    for handler in @OnGet.Listeners
      if handler
        mod += handler(@)
    return @Value + mod

  Add: (amount) =>
    @Set(@Value+amount)

  Take: (amount)=>
    @Add(-amount)

  Restore: ()=>
    @Set(if @Bad then @Min else @Max)
