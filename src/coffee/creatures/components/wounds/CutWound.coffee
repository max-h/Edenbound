﻿class CutWound extends Wound
  constructor: ->
    super()
    @ID = 'cut'
    @Name = 'Cut'
    @MaxHPHit = 2
    @Flags = EWoundFlags.BLEEDING

Wound.Register CutWound
