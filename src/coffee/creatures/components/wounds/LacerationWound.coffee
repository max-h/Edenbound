﻿class LacerationWound extends Wound
  constructor: ->
    super()
    @ID = 'laceration'
    @Name = 'Laceration'
    @MinHPHit = 5
    @MaxHPHit = 10
    @Flags = EWoundFlags.BLEEDING
    @CanScar = true

Wound.Register LacerationWound
