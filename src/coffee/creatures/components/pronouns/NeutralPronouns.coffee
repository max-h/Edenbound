﻿class NeutralPronouns extends Pronouns
  constructor: ->
    super()
    @ID = 'neutral'
    @Title = 'Individual'
    @TitleAbbr = 'Ind.'
    @Subject = 'They'
    @SubjectAre = "They're"
    @SubjectHas = "They've"
    @Object = 'Them'
    @Referential = 'Their'
    @DependentPossessive = 'Theirs'
    @IndependentPossessive = 'Themself'

  MatchesBody: (creature) ->
    return false

Pronouns.Register NeutralPronouns
