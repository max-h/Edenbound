﻿class FemininePronouns extends Pronouns
  constructor: ->
    super()
    @ID = 'feminine'
    # Not being presumptive as to marital status.
    @Title = 'Miss'
    @TitleAbbr = 'Ms.'
    @Subject = 'She'
    @SubjectAre = "She's"
    @SubjectHas = "She's"
    @Object = 'Her'
    @Referential = 'Her'
    @DependentPossessive = 'Hers'
    @IndependentPossessive = 'Herself'

  MatchesBody: (creature) ->
    return !creature.HasBodyPart(Penis) and creature.HasBodyPart(Vagina)

Pronouns.Register FemininePronouns
