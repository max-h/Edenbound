﻿class MasculinePronouns extends Pronouns
  constructor: ->
    super()
    @ID = 'masculine'
    @Title = 'Mister'
    @TitleAbbr = 'Mr.'
    @Subject = 'He'
    @SubjectAre = "He's"
    @SubjectHas = "He's"
    @Object = 'Him'
    @Referential = 'His'
    @DependentPossessive = 'His'
    @IndependentPossessive = 'Himself'

  MatchesBody: (creature) ->
    return creature.HasBodyPart(Penis) and !creature.HasBodyPart(Vagina)

Pronouns.Register MasculinePronouns
