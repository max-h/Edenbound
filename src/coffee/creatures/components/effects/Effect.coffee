﻿class Effect extends GameObject
  constructor: ->
    super()
    @ID=''
    # @serialize: no
    @Name=''
    # @serialize: no
    @Description=''

    @EndsAt=-1

    # @serialize: no
    @Creature = null
    # @serialize: no
    @BodyPart = null

  Attach: (part, cloned=no) ->
    @BodyPart = part
    @Creature = @BodyPart.Creature

  Detach: (part) ->
    @Creature = null
    @BodyPart = null
