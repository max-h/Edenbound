﻿# requires: _core/utils/General.coffee
class BodyPartGroup
  constructor: ->
    @Members = []
    @MatchConditionKeys = []

  @property 'Count',
    get: ->
      return @Members.length

  FillMatchConditions: (bp) ->
    @MatchConditionKeys = bp.MatchConditionKeys
    for key in @MatchConditionKeys
      @[key]=bp[key]

  Matches: (bp) ->
    master = @Members[0]
    for key in @MatchConditionKeys
      if master[key] != bp[key]
        return no
    return yes

  CollectFrom: (bodyparts) ->
    survivors = []
    for bp in bodyparts
      if @Members.length == 0
        @Members.push bp
        @FillMatchConditions bp
        continue
      if @Matches bp
        @Members.push bp
        continue
      survivors.push bp
    return survivors
