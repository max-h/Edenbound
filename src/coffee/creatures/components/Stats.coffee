﻿###
# This class is a handy structure for a Mind's stats.
###
class Stats extends GameObject
  constructor: (@Creature) ->
    super()

    ## CORE STATS (modified from a few different sources)
    # Smashing tomatoes
    @Strength     = new Stat @Creature, 'Strength', 0, 100, false
    # Not getting killed by a single tomato
    @Defense     = new Stat @Defense,   'Defense', 0, 100, false
    # Avoiding getting hit by a tomato
    @Dexterity    = new Stat @Creature, 'Dexterity', 0, 100, false
    # Ability to sell tomato salad
    @Charisma     = new Stat @Creature, 'Charisma', 0, 100, false
    # Knowing a tomato is a fruit AND knowing not to sell it in a fruit salad
    @Intelligence = new Stat @Creature, 'Intelligence', 0, 100, false
    # Ability to steal a tomato without getting caught
    @Stealth      = new Stat @Creature, 'Stealth', 0, 100, false

    @Experience = new Stat @Creature, 'Experience', 0, 10, false
    @Level      = new Stat @Creature, 'Level',      1, Stats.CalculateXPPerLevel(1), false

  @CalculateXPPerLevel: (level) ->
    return level * 25 # Linear difficulty curve, at least for now.

  Reset: ->
    @Strength.Set 5
    @Defense.Set 5
    @Dexterity.Set 5
    @Charisma.Set 5
    @Intelligence.Set 5
    @Stealth.Set 5

    @Experience.Set 0
    @Level.Set 1

  clone: (cloned_creature) ->
    s = new Stats cloned_creature
    s.Strength     = @Strength.clone(cloned_creature)
    s.Defense      = @Defense.clone(cloned_creature)
    s.Dexterity    = @Dexterity.clone(cloned_creature)
    s.Charisma     = @Charisma.clone(cloned_creature)
    s.Intelligence = @Intelligence.clone(cloned_creature)
    s.Stealth      = @Stealth.clone(cloned_creature)

    s.Experience = @Experience.clone(cloned_creature)
    s.Level      = @Level.clone(cloned_creature)

    return s

  serialize: ->
    o = super()
    o.Strength=@Strength.serialize()
    o.Defense=@Defense.serialize()
    o.Dexterity=@Dexterity.serialize()
    o.Charisma=@Charisma.serialize()
    o.Intelligence=@Intelligence.serialize()
    o.Stealth=@Stealth.serialize()

    o.Experience=@Experience.serialize()
    o.Level=@Level.serialize()

    return o

  deserialize: (data, creature) ->
    super(data)

    @Strength.deserialize data.Strength, creature
    @Defense.deserialize data.Defense, creature
    @Dexterity.deserialize data.Dexterity, creature
    @Charisma.deserialize data.Charisma, creature
    @Intelligence.deserialize data.Intelligence, creature
    @Stealth.deserialize data.Stealth, creature

    @Experience.deserialize data.Experience, creature
    @Level.deserialize data.Level, creature
