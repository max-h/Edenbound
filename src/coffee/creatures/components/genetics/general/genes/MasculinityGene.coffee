﻿# @enumref: EGeneType
# @enumref: EDNAType
class MasculinityGene extends ScalarGene
  @ID: 'masculinity'
  constructor: (creature) ->
    super(creature, EGeneType.DESCRIPTIVE, MasculinityGene.ID, 0)
    
    @CompatibleDNA=[
      EDNAType.HUMAN
    ]

    @Range = new MinMax -30, 30

    @Name='Masculinity'
    @Description = 'This changes how masculine or feminine you look.'

  Attach: (dna, creature, postclone) ->
    super(dna, creature, postclone)
    dna.MasculinityGene = @
    creature.TalliedAGVotes.Detach @OnTalliedAGVotes
    return

  Detach: (creature) ->
    if creature.DNA.MasculinityGene == @
      creature.DNA.MasculinityGene = null
    creature.TalliedAGVotes.Detach @OnTalliedAGVotes
    return

  IsCompatibleWith: (dna) ->
    # @depdetect: classref HumanDNA
    return dna instanceof HumanDNA

  @property 'DisplayText',
    get: ->
      if @Value > 0
        return "Masculine +#{@Value}"
      if @Value < 0
        return "Feminine +#{Math.abs(@Value)}"
      return "No Change"

  RollNewGene: ->
    @Value = 0

  OnTalliedAGVotes: (votes)=>
    # @enumref: EGenderVote
    if @Value > 0
      votes[EGenderVote.MASCULINE] += Math.abs @Value
    if @Value < 0
      votes[EGenderVote.FEMININE] += Math.abs @Value


# @depdetect: classref Gene
Gene.Register MasculinityGene
