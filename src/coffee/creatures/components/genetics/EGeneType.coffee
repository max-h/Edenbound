﻿
# @depdetect: enumdef EGeneType
###
# Defines the icon assigned to that gene.
###
EGeneType =
  DESCRIPTIVE:  0
  MUTATION:     1 # Exclamation mark, Purple BG
  FATAL:        2 # Skull, black BG
  ADVANTAGE:    3 # Thumbs up
  DISADVANTAGE: 4 # Thumbs down
