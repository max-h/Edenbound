﻿class HumanDNA extends DNA
  constructor: (creature)->
    super(creature, EDNAType.HUMAN)

  clone: (creature) ->
    dna = new @constructor(null)
    super(creature, dna)
    return dna
