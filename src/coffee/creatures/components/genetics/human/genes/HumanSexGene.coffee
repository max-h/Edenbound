﻿# @enumref: EGeneType
# @enumref: EDNAType
class HumanSexGene extends ScalarGene
  @ID: 'human-sex'
  constructor: (creature) ->
    super(creature, EGeneType.DESCRIPTIVE, HumanSexGene.ID, 'XX')
    @CompatibleDNA=[
      EDNAType.HUMAN
    ]
    @Name='Human Sex'
    @Description = 'The biological switch that makes changes to the endocrine system that paves the way for sex development.'
    @Possibilities =
      'XX':   'Female'
      'XY':   'Male'
      'XYXX': 'Hermaphrodite'
      '00':   'Neutral'
      # We can add in other possibilities here, but right now I want to focus on the basics.
    @CommonPossibilities = [
      'XX'
      'XY'
    ]
    @ApparentGender = EGenderVote.ANDROGYNOUS

  Attach: (dna, c, postclone) ->
    super(dna, c, postclone)
    dna.GenderGene = @
    #c.PreGenerateOrgans.Attach(@Pregenerate)
    c.GenerateOrgans.Attach @GenOrgans
    c.ReceivedBodyPlan.Attach @ReceivedBodyPlan
    c.PostGenerateOrgans.Attach @PostGenOrgans
    return

  Detach: (c) ->
    if c.DNA.GenderGene == @
      c.DNA.GenderGene = null
    c.GenerateOrgans.Detach @GenOrgans
    c.ReceivedBodyPlan.Detach @ReceivedBodyPlan
    c.PostGenerateOrgans.Detach @PostGenOrgans
    return

  IsCompatibleWith: (dna) ->
    # @depdetect: classref HumanDNA
    return dna instanceof HumanDNA

  @property 'DisplayText',
    get: ->
      if @Value of @Possibilities
        return @Possibilities[@Value]
      console.error "UNKNOWN @VALUE=#{@Value} FOR #{@constructor.name}!"
      return null

  RollNewGene: ->
    @Value = either @CommonPossibilities
    #console.log "DNA: Set #{@constructor.name}.Value to #{@Value}"

  ReceivedBodyPlan: (species) =>
    breasts = new BreastSpec()
    breasts.SetCount 2
    breasts.Flatten()
    species.BodyPlan.push breasts

    penis = new PenisSpec()
    penis.SetCount 0
    species.BodyPlan.push penis
    balls = new TesticleSpec()
    balls.SetCount 0
    species.BodyPlan.push balls

    vag = new VaginaSpec()
    vag.SetCount 0
    species.BodyPlan.push vag
    ovaries = new OvarySpec()
    ovaries.SetCount 0
    species.BodyPlan.push ovaries
    womb = new WombSpec()
    womb.SetCount 0
    species.BodyPlan.push womb

    if @HasPenis()
      # @classref: HumanPenisBodyPart
      penis.SetCount 1
      penis.Types = [HumanPenisBodyPart]

      balls.SetCount 2
      #balls.Types = [HumanTesticleBodyPart]

    if @HasVagina()
      vag.SetCount 1
      ovaries.SetCount 2
      womb.SetCount 1
      breasts.SizeMin = 17.8 # A
      breasts.SizeMax = 27.9 # DD

    return

  HasBoobs: ->
    switch @Value
      when 'XX', 'XYXX'
        return true
    return false

  HasPenis: ->
    switch @Value
      when 'XY', 'XYXX'
        return true
    return false

  HasVagina: ->
    switch @Value
      when 'XX', 'XYXX'
        return true
    return false

  GenOrgans: (skip_apparent_gender=no) =>
    # @enumref: EGenderVote
    if !skip_apparent_gender
      @ApparentGender=EGenderVote.NEUTRAL
    switch @Value
      when 'XY'
        if !skip_apparent_gender
          @ApparentGender=EGenderVote.MASCULINE
      when 'XX'
        if !skip_apparent_gender
          @ApparentGender=EGenderVote.FEMININE
    return

  IsSexOrgan: (bp) ->
    if bp instanceof PenisBodyPart
      return true
    if bp instanceof TesticleBodyPart
      return true
    if bp instanceof VaginaBodyPart
      return true
    if bp instanceof WombBodyPart
      return true
    if bp instanceof OvaryBodyPart
      return true
    return false

  RemoveOrgans: ->
    for bp in clone(@Creature.BodyParts)
      if @IsSexOrgan bp
        @Creature.RemoveBodyPart bp
    return

  # Apply effects to organs.
  PostGenOrgans: =>
    for bp in @Creature.BodyParts
      bp.ApparentGender = @ApparentGender
    return

  GetBiologicalSex: ->
    return @Value

  GetPronouns: ->
    switch @Value
      when 'XX'
        return new FemininePronouns()
      when 'XY'
        return new MasculinePronouns()
      else
        return new NeutralPronouns()

  # Should only be used during character setup.
  SetChromosome: (val) ->
    @Value=val

  SetApparentGender: (val) ->
    @ApparentGender = val
    @PostGenOrgans()

  SetChromosomeByGenderPreset: (g) ->
    switch g
      when ECommonGenders.MALE
        @SetChromosome 'XY'
      when ECommonGenders.FEMALE
        @SetChromosome 'XX'
      when ECommonGenders.NEUTER
        @SetChromosome '00'
      when ECommonGenders.HERMAPHRODITE
        @SetChromosome 'XYXX'
      else
        @SetChromosome either Object.keys(@Possibilities)

# @depdetect: classref Gene
Gene.Register HumanSexGene
