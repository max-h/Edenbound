﻿# @enumdef: EBodyPartLocation
EBodyPartLocation =
  NONE: ''
  ARM: 'arm'
  EAR: 'ear'
  EYE: 'eye'
  HEAD: 'head'
  LEG: 'leg'
  SKIN: 'skin'
  TORSO: 'torso'
