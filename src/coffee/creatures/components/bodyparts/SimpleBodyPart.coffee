﻿class SimpleBodyPart extends BodyPart
  constructor: (id, singular_name, plural_name) ->
    super id, singular_name, plural_name

  clone: (creature, c=null) ->
    super creature, c
