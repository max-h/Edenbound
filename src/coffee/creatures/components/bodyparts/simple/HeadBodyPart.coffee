﻿class HeadBodyPart extends SimpleBodyPart
  constructor: (id="head", singular_name="Head", plural_name='Heads') ->
    super id, singular_name, plural_name
    # @depdetect: enumref EBodyPartClass
    # @serialize: no
    @Class = EBodyPartClass.HEAD
    # @enumref: EGenderVote
    # @serialize: no
    @ApparentGender = EGenderVote.NEUTRAL

    # http://www.askabiologist.org.uk/answers/viewtopic.php?id=1477
    # @serialize: no
    @BaseWeight=6 #kg
