﻿class LegBodyPart extends SimpleBodyPart
  constructor: (id="leg", singular_name="Leg", plural_name='Legs') ->
    super id, singular_name, plural_name
    # @depdetect: enumref EBodyPartClass
    # @serialize: no
    @Class = EBodyPartClass.LEG
    # @enumref: EGenderVote
    # @serialize: no
    @ApparentGender = EGenderVote.NEUTRAL

    # http://www.askabiologist.org.uk/answers/viewtopic.php?id=1477
    # @serialize: no
    @BaseWeight=7 #kg
