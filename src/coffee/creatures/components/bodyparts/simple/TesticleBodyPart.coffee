﻿class TesticleBodyPart extends SimpleBodyPart
  constructor: (id="testicle", singular_name="Testicle", plural_name='Testes') ->
    super id, singular_name, plural_name
    # @depdetect: enumref EBodyPartClass
    # @serialize: no
    @Class = EBodyPartClass.TESTICLE

    ###
    # Diameter of testicle in cm
    ###
    @Diameter = 4.5

    ###
    # Chance of sperm from this testicle inducing pregnancy, 0-1.
    ###
    @Virility = 0

    # @serialize: no
    @Container = new Container(@)

  GetWeight: ->
    # Convert to meter radius
    rm = (@Diameter / 100)/2
    # PI*R^2*L = volume of cylinder
    return ((4/3)*Math.PI*Math.pow(rm,3))*DENSITY_OF_FLESH

  serialize: ->
    data = super()
    # @serializer-start: data
    #AUTOGENERATED
    if @Diameter != 4.5
      data['diameter'] = @Diameter
    if @Virility != 0
      data['virility'] = @Virility
    # @serializer-end
    return data

  deserialize: (data) ->
    super(data)
    # @deserializer-start: data
    #AUTOGENERATED
    _deser_errs = 0
    if 'diameter' of data
      @Diameter = data['diameter']
    else
      @Diameter = 4.5
    if 'virility' of data
      @Virility = data['virility']
    else
      @Virility = 0
    if console and console.error and _deser_errs > 0
      console and console.error "#{_deser_errs} DESERIALIZATION ERRORS in #{@constructor.name}: DATA: ", data
    # @deserializer-end


  clone: (creature, c=null) ->
    # If c was left unspecified, we act as the originating caller.
    if c == null or c == true or c == undefined
      c = new @constructor()
    super(creature, c)
    # @cloner-start: c
    #AUTOGENERATED
    c.Diameter = @Diameter
    c.Virility = @Virility
    # @cloner-end
    return c

  ###
  # Makes ApparentGender read-only.
  ###
  GetApparentGender: ->
    return EGenderVote.MASCULINE
