﻿class EyeBodyPart extends SimpleBodyPart
  constructor: (id="eye", singular_name="Eye", plural_name='Eyes') ->
    super id, singular_name, plural_name
    # @enumref: EBodyPartClass
    # @serialize: no
    @Class = EBodyPartClass.EYE
    # @enumref: EGenderVote
    # @serialize: no
    @ApparentGender = EGenderVote.NEUTRAL
    # @serialize: no
    @Container = new Container(@)
    # @serialize: no
    @BaseWeight=0.05
