﻿class ArmBodyPart extends SimpleBodyPart
  constructor: (id="arm", singular_name="Arm", plural_name='Arms') ->
    super id, singular_name, plural_name
    # @enumref: EBodyPartClass
    @Class = EBodyPartClass.ARM
    # @enumref: EGenderVote
    # @serialize: no
    @ApparentGender = EGenderVote.NEUTRAL

    # http://www.askabiologist.org.uk/answers/viewtopic.php?id=1477
    # @serialize: no
    @BaseWeight=4.2 #kg
