﻿class EarBodyPart extends SimpleBodyPart
  constructor: (id="ear", singular_name="Ear", plural_name='Ears') ->
    super id, singular_name, plural_name
    # @depdetect: enumref EBodyPartClass
    # @serialize: no
    @Class = EBodyPartClass.EAR
    # @serialize: no
    @BaseWeight=0.01
