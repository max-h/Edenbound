﻿
# @enumdef: EBodyPartFlags
EBodyPartFlags =
  ###
  # Set when a body part is created by a mutation.
  ###
  MUTATED: 1
