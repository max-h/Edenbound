﻿# requires: _core/utils/Nylon.coffee
# requires: _core/utils/Random.coffee
# requires: _core/utils/General.coffee
window.postDisplay['New Game 2'] = (a,b,c) ->

  # Refreshes the crappy preview blurb with pronouns and shit.
  refreshPreview = ->
    pn = getVariables().player.Mind.Identity.Pronouns
    jqPronounSelector = $("#passages a.pronoun-preset")
    jqPronounSelector.removeClass 'selected'
    if pn instanceof MasculinePronouns
      $('#passages a#pronoun-masculine').addClass 'selected'
    if pn instanceof FemininePronouns
      $('#passages a#pronoun-feminine').addClass 'selected'
    if pn instanceof NeutralPronouns
      $('#passages a#pronoun-neutral').addClass 'selected'
    if pn and pn.Title and pn.TitleAbbr and pn.Subject and pn.SubjectAre and pn.SubjectHas and pn.Object and pn.Referential and pn.DependentPossessive and pn.IndependentPossessive
      if !window.formValid
        window.formValid=true
        $('#passages .next').prop('disabled', false)
        #set_playergen_progress 3, 3
      $('#passages #genderPreview').html(pn.ReplaceIn($('#passages #genderPreviewProto').html(), 'pp'))
    else
      if window.formValid
        window.formValid=false
        #set_playergen_progress 3, 2, true
        $('#passages .next').prop('disabled', true)
      $('#passages #genderPreview').html('<p><em>Select pronouns to preview!</em></p>')

    window.setPageElement 'player-desc-preview', 'PlayerDescription', ''
    updateStoryMenu()

    return

  refreshMasculinityRange = ->
    $('#passages #apparentsexRange').val getVariables().player.DNA.MasculinityGene.Value
    $('#passages #apparentsexReadout').text getVariables().player.DNA.MasculinityGene.DisplayText
    refreshPreview()
    return

  # Fills the pronouns with a bunch of preset pronouns.
  fillPronouns = (pronouns) ->
    getVariables().player.Mind.Identity.Pronouns = pronouns
    $('#passages input[data-pronoun-attr]').each ->
      pronounKey = $(@).attr('data-pronoun-attr')
      $(@).val pronouns[pronounKey]

  updateGenderSelect = () ->
    $player = getVariables().player
    $('#passages a.biosex-preset').each ->
      sex = $(@).attr('data-preset').toUpperCase()
      $(@).removeClass 'selected'
      if $player.DNA.GenderGene.Value == sex
        $(@).addClass 'selected'

  # Tracks changes to all pronoun inputs.
  $('#passages input[type=text]').on 'input change', ->
    if !getVariables().player.Mind.Identity.Pronouns
      getVariables().player.Mind.Identity.Pronouns = new Pronouns()
    pronounKey = $(@).attr('data-pronoun-attr')
    pronounValue = $(@).val()
    getVariables().player.Mind.Identity.Pronouns[pronounKey] = pronounValue
    refreshPreview()
    getVariables().player.UpdateStats true
    return

  # Toggles auto-pronouns.
  $('#passages #automaticPronouns').on 'change', ->
    getVariables().player.Mind.Identity.AutomaticPronouns = $(@).prop('checked')
    $('#passages input[data-pronoun-attr]').prop('disabled', getVariables().player.Mind.Identity.AutomaticPronouns)
    if getVariables().player.Mind.Identity.AutomaticPronouns
      # Do organ detection here.
      refreshPreview()
      getVariables().player.UpdateStats true
    return

  # Handles clicking default pronoun presets.
  $('#passages a.pronoun-preset').on 'click', ->
    presetKey = $(@).attr('data-preset')
    pronounSet = Pronouns.ALL[presetKey]
    getVariables().player.Mind.Identity.Pronouns = new pronounSet()
    fillPronouns getVariables().player.Mind.Identity.Pronouns
    refreshPreview()
    getVariables().player.UpdateStats true
    return

  # Handle biological sex selection.
  $('#passages a.biosex-preset').on 'click', ->
    presetKey = $(@).attr('data-preset').toUpperCase()
    $player = getVariables().player
    #$player.DNA.GenderGene.RemoveOrgans()
    $player.DNA.GenderGene.SetChromosome presetKey
    $player.GetSpecies().InitialBodySetup $player
    $player.Mind.Identity.OriginalSex = $player.DNA.GenderGene.DisplayText
    $player.Mind.ReassessPronouns()
    updateGenderSelect()
    refreshPreview()
    $player.UpdateStats true
    return

  $('#passages #apparentsexRange').on 'input propertychange', ->
    console.log "#apparentsexRange!"
    getVariables().player.DNA.MasculinityGene.Value = parseInt $(@).val()
    refreshPreview()
    refreshMasculinityRange()
    getVariables().player.UpdateStats true
    return

  if !getVariables().player.Mind.Identity
    getVariables().player.Mind.Identity = new Identity()
  #set_playergen_progress 3, 2
  getVariables().player.Mind.Identity.AutomaticPronouns = true
  $('#passages input[data-pronoun-attr]').prop('disabled', true)
  fillPronouns getVariables().player.Mind.Identity.Pronouns
  refreshPreview()
  updateGenderSelect()
  refreshMasculinityRange()
  cmdPrev=$('#passages #cmdPrev')
  cmdPrev.on 'click', ->
    window.state.display $(@).attr('data-goto'), null, null, null
    return
  cmdPrev.disable no
  cmdNext=$('#passages #cmdNext')
  cmdNext.on 'click', ->
    window.state.display $(@).attr('data-goto'), null, null, null
    return
  cmdNext.disable no
  $('.center-block').center()
  return
