﻿# requires: _core/utils/Nylon.coffee
# requires: _core/utils/Random.coffee
# requires: _core/utils/General.coffee
window.postDisplay['New Game 3'] = (a,b,c) ->
  $('.center-block').center()

  cmdPrev=$('#passages #cmdPrev')
  cmdPrev.on 'click', ->
    window.state.display $(@).attr('data-goto'), null, null, null
    return
  cmdPrev.disable no

  cmdNext=$('#passages #cmdNext')
  cmdNext.on 'click', ->
    button = $(@)
    JQConfirm
      title: "Ready?"
      text: "<p>Are you happy with your settings?</p><p>If not, <b>go back and change them now</b>.</p><div class='warning'>This is your last warning before the game starts!</div>"
      yes_text: "Yes"
      no_text: 'No'
      yes: (ui, e) ->
        window.state.display button.attr('data-goto'), null, null, null
        return
    return
  cmdNext.disable no
  return
