﻿# requires: _core/utils/Nylon.coffee
# requires: _core/utils/Random.coffee
# requires: _core/utils/General.coffee

window.postDisplay['New Game 1'] = (a,b,c) ->
  $('.center-block').center()
  maleNames = loadLinesFrom('MaleNames')
  femaleNames = loadLinesFrom('FemaleNames')
  surnames = loadLinesFrom('Surnames')
  txtFirstName = $('#passages #txtFirstName')
  txtLastName = $('#passages #txtLastName')
  cmdNext = $('#passages #cmdNext')
  rollForMasculineName = ->
    txtFirstName.val pickndrop(maleNames)
    validateNames()
    return
  rollForFeminineName = ->
    txtFirstName.val pickndrop(femaleNames)
    validateNames()
    return
  rollForSurname = ->
    txtLastName.val pickndrop(surnames)
    validateNames()
    return
  $('#passages #cmdRollFirstNameMale').on 'click', rollForMasculineName
  $('#passages #cmdRollFirstNameFemale').on 'click', rollForFeminineName
  #$('#cmdRollFirstNameAndrogynous').on 'click', ->
  #  $('#passages #textinput|0').val pickndrop(androgynousNames)
  #  return
  $('#passages #cmdRollLastName').on 'click', rollForSurname

  doValidateNames = ->
    passed = true
    highlightBad(txtFirstName, false)
    highlightBad(txtLastName, false)
    if txtFirstName.val().length == 0
      highlightBad(txtFirstName, true)
      passed = false
    if txtLastName.val().length == 0
      highlightBad(txtLastName, true)
      passed = false
    player = getVariables().player
    console.log player
    if passed
      player.Mind.Identity.FirstName = txtFirstName.val()
      player.Mind.Identity.LastName = txtLastName.val()
      $player.UpdateStats()
    return passed

  validateNames = ->
    cmdNext.disable !doValidateNames()
    return

  txtFirstName.on 'change', validateNames
  txtLastName.on 'change', validateNames
  cmdNext.on 'click', ->
    if doValidateNames()
      $player = getVariables().player
      $player.Mind.Identity.FirstName = $('#txtFirstName').val()
      $player.Mind.Identity.LastName = $('#txtLastName').val()
      window.state.display $(@).attr('data-goto'), null, null, null
      $player.UpdateStats()

  if 'player' not of getVariables()
    # Create Creature.
    $player = new Creature()
    # Set em up. Player is always human.
    $player.RollNewCharacter(HumanSpecies.ID)
    # Assign to $player inside Twine/Nylon
    getVariables().player = $player
    #console.log 'Rolled new player!'

    # Setup universe.
    getVariables().universe = new Universe()
    getVariables().universe.Player = $player
    getVariables().universe.ForceLocation(avatar_id='avatar-unknown', avatar_pip=EGenderVote.MASCULINE, subject_name='???', zone_name='Pluto Station', region_name='Pluto, Sol')
    $player.Health.Restore()
    $player.Special.Restore()

  txtFirstName.val getVariables().player.Mind.Identity.FirstName
  txtLastName.val getVariables().player.Mind.Identity.LastName
  validateNames()
