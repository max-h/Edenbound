﻿class RenderUtils
  @RenderBodyGroupForPlayerDescription: (bpcls, clsname_singular, clsname_plural, hide_missing=no)->
    # @enumref: EBodyPartClass
    $player = getVariables().player
    bpgroups = $player.GetBodyPartGroups(bpcls)
    o = ''
    if bpgroups.length == 0
      if !hide_missing
        o = "You have no #{clsname_plural}."
    else
      i=0
      for group in bpgroups
        if group.Count == 0
          continue
        i++
        o += " You "
        if i > 1
          o += "also "
        o += "#{either("have", "possess", "lay claim to")} #{group.Count} "
        if group.Count > 1
          o += group.NamePlural.toLowerCase()
        else
          o += group.NameSingular.toLowerCase()
        if group.AttachPoint
          o += " attached to your #{_group.AttachPoint.Name.toLowerCase()}"
        o += ". "
        j=0
        for _arm in group.Members
          if _arm.Wounds.length > 0
            j++
            if j == 1
              o += " One of your #{_arm.NamePlural.toLowerCase()}"
            else
              o += " Another #{_arm.NameSingular.toLowerCase()}"
            o+= " has"
            _activewounds = _arm.GetActiveWounds()
            o += " #{_activewounds.length} wounds:"
            k=0
            for _wound in _arm.Wounds
              k++
              if k > 1
                o += ", "
              if k == _activewounds.length
                o += "and"
              o += " #{_wound.GetDescription()}."
    return o
