﻿# @classref: Nylon
Nylon.AddPostRenderHook (a,b,c) ->
  # <ul class="choices">
  #   <li class="neutral recommended">[[...]]</li>

  # Relation:
  #  neutral    - Signals no effect on relations with the other party.
  #  nice       - Improves relations
  #  evil       - Degrades relations
  # Flags:
  #  recommended - This is the recommended move to make (useful in tutorials)
  #  combat      - Will propel you into combat.
  #  bad-end     - Bad end.
  #  nsfw        - Not Safe For Work
  #  mutation    - Content contains transformation.
  #  rape        - Content contains rape.
  # ADD NEW CLASSES HERE SO WE KNOW WHAT'S AVAILABLE.
  $('#passages ul.choices li').each (idx, dom_li) ->
    console.log idx, dom_li
    li = $(dom_li)
    li.find('span.badges').remove()
    badges = $ '<span class="badges"></span>'
    li.prepend badges
    for cssClass in dom_li.classList
      psgID = 'badge-'+cssClass
      if psgID of window.tale.passages
        badge = $ "<span class=\"badge #{cssClass}\"></span>"
        badge.html getPassageByID(psgID).text
        badges.append badge
    return
