﻿# @classref: Nylon
Nylon.AddPostRenderHook (a,b,c) ->
  # <input id="chkShowDebug" name="chkDebugMessages" type="checkbox" class="togglevar" data-var="showDebug">
  $('#passages input.togglevar').each ->
    varName = $(this).attr('data-var')
    dt = $(this).attr('data-type')
    [trueVal, falseVal] = getTrueAndFalseForType dt
    inverted = $(this).hasClass('inverted')
    currentValue = getVariables()[varName] == trueVal
    if inverted
      currentValue = !currentValue
    $(this).prop 'checked', currentValue

  $('#passages input.togglevar').on 'change', (event) ->
    varName = $(this).attr('data-var')
    [trueVal, falseVal] = getTrueAndFalseForType $(this).attr('data-type')
    inverted = $(this).hasClass('inverted')
    currentValue = $(this).prop 'checked'
    if inverted
      currentValue = !currentValue
    getVariables()[varName] = if currentValue then trueVal else falseVal
    console.log "$#{varName} = ", getVariables()[varName]
