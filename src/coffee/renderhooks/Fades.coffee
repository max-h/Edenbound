﻿scrambleText = (orig, charmask=null, class_scrambled='scrambled', class_descrambled='descrambled', add_spans=true) ->
  o = ''
  for i in [0...orig.length]
    c = orig[i]
    if c == ' ' or (charmask and charmask[i])
      if c != ' '
        o += "<span class=\"#{class_descrambled}\">"
      o += c
      if c != ' '
        o += "</span>"
      continue
    else
      o += "<span class=\"#{class_scrambled}\">"
      o += random_char(CHARS_ALPHA_NUMERIC)
      o += "</span>"
  return o

getDescrambleCharTimeInMS = (duration, targetText) ->
  return Math.max(1, Math.floor((duration*1000)/targetText.length))

handleDescrambleTimeout = (data) ->
  return ->
    [subject,duration,delay,targetText,charmask,nextDescramble,lastDescramble] = data
    keysToDescramble = []
    ###
    if Date.now() >= lastDescramble
      console.log "OUT OF TIME. @ ", Date.now(), nextDescramble, lastDescramble
      for k of charmask
        charmask[k] = true
      subject.html(scrambleText(targetText, charmask))
      return
    ###

    if Date.now() >= nextDescramble
      for k,v of charmask
        if !v
          keysToDescramble.push k
      if keysToDescramble.length == 0
        return
      keyToFix = window.either keysToDescramble
      console.log('Descrambling at pos #',keyToFix,keysToDescramble)
      if !charmask[keyToFix]
        charmask[keyToFix]=true
        # start @ 13903
        # duration=3
        # strlen=5
        # durInMS=3000
        # timePerChar=3000/5=600
        # nextDescramble @
      subject.html(scrambleText(targetText, charmask))
      nextDescramble = getDescrambleCharTimeInMS(duration, targetText) + Date.now()
    data = [
      subject
      duration
      delay
      targetText
      charmask
      nextDescramble
      lastDescramble
    ]
    setTimeout(handleDescrambleTimeout(data), 10)
    return

# requires: _core/utils/Nylon.coffee
Nylon.AddPostRenderHook (a,b,c) ->
  if $('.fadein').exists()
    $('.fadein').each (i) ->
      subject = $(@)
      delay = parseInt(subject.attr('data-fadein-delay')) or 0
      duration = parseInt(subject.attr('data-fadein-duration')) or 1
      console.log "Found fadein with delay=#{delay}, duration=#{duration}"
      subject.fadeTo 0, 0.01
      if delay > 0
        subject.delay delay*1000
      subject.fadeTo duration*1000, 1
      return

  if $('.descramble').exists()
    $('.descramble').each (i) ->
      subject = $(@)
      delay = parseInt(subject.attr('data-descramble-delay')) or 0
      duration = parseInt(subject.attr('data-descramble-duration')) or 1
      console.log "Found descramble with delay=#{delay}, duration=#{duration}"
      targetText = subject.text().trim()
      charmask = (false for [0...targetText.length])
      #charmask = []
      #charmask.push(false) while charmask.length isnt targetText.length
      nextDescramble = getDescrambleCharTimeInMS(duration, targetText) + Date.now() + (delay*1000)
      lastDescramble = Date.now() + (duration*1000) + (delay*1000)
      data = [
        subject
        duration
        delay
        targetText
        charmask
        nextDescramble
        lastDescramble
      ]
      subject.html scrambleText(targetText)
      setTimeout(handleDescrambleTimeout(data), delay*1000)
      if delay > 0
        subject.delay delay*1000
      return
