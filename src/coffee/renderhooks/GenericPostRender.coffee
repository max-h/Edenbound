﻿handlerForStatBars = ->
  if 'universe' of getVariables() and getVariables().universe != 0
    $universe = getVariables().universe
    #console.log "$universe=",$universe
    $universe.DrawStatBars(full=yes)
    $universe.UpdateHeader()
    if !$universe.TimeLocked
      $universe.Timestamp += $universe.TimeDelta
    $universe.TimeDelta=60

  $('#edit-passage').on 'click', ->
    # @classref: PassageEditor
    PassageEditor.Edit()
    return

# @classref: Nylon
Nylon.AddPostRenderHook handlerForStatBars
Nylon.PostSetPageElements.Attach handlerForStatBars

Nylon.PreSetPageElements.Attach (a,b,c) ->
  $('#edit-passage').off()
  return
