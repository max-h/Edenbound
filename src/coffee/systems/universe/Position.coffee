﻿# Not serialized to save (yet), just data-driven.
class Position extends GameObject
  constructor: ->
    super()
    # @serialize: name=id
    @ID=''
    # @serialize: no
    @X=0
    # @serialize: no
    @Y=0
    @Name=''
    @Flags = EPositionFlags.NONE
    # @serialize: name=zone_id scalar
    @ZoneID=''
    # @serialize: no
    @Zone=null
    # @serialize: default=null object=Atmosphere
    @Atmosphere = new Atmosphere()
    # @serialize: default=null object=AreaExposureInfo
    @Radiation = new AreaExposureInfo()
    # @serialize: array scalar
    @ConnectedPositions=[]
    # @serialize: object=EncounterManager
    @Encounters = new EncounterManager()

    # @serialize: name=avatar_id
    @AvatarID = null
    @AvatarPip = null

    # @serialize: array scalar
    @Overlays = []
    @Background = null

    # @serialize: no
    @_combinedAtmosphere=null
    # @serialize: no
    @_combinedRadiation=null
    # @serialize: no
    @_DisplayedObjects=[]

  Propogate: ->
    return

  serialize: ->
    data = super()
    # @serializer-start: data
    #AUTOGENERATED
    if @ID != ''
      data['id'] = @ID
    if @Name != ''
      data['name'] = @Name
    if @Flags != EPositionFlags.NONE
      data['flags'] = @Flags
    if @ZoneID != ''
      data['zone_id'] = @ZoneID
    if @Atmosphere != undefined and @Atmosphere != null
      data['atmosphere'] = @Atmosphere.serialize()
    if @Radiation != undefined and @Radiation != null
      data['radiation'] = @Radiation.serialize()
    if @ConnectedPositions != null and @ConnectedPositions.length > 0
      data['connected_positions'] = @ConnectedPositions
    if @Encounters != undefined and @Encounters != null
      data['encounters'] = @Encounters.serialize()
    if @AvatarID != null
      data['avatar_id'] = @AvatarID
    if @AvatarPip != null
      data['avatar_pip'] = @AvatarPip
    if @Overlays != null and @Overlays.length > 0
      data['overlays'] = @Overlays
    if @Background != null
      data['background'] = @Background
    # @serializer-end
    return data

  deserialize: (data) ->
    super(data)
    # @deserializer-start: data
    #AUTOGENERATED
    _deser_errs = 0
    if 'id' of data
      @ID = data['id']
    else
      @ID = ''
    if 'name' of data
      @Name = data['name']
    else
      @Name = ''
    if 'flags' of data
      @Flags = data['flags']
    else
      @Flags = EPositionFlags.NONE
    if 'zone_id' of data
      @ZoneID = data['zone_id']
    else
      @ZoneID = ''
    if 'atmosphere' of data
      @Atmosphere=new Atmosphere()
      @Atmosphere.deserialize data['atmosphere']
    else
      @Atmosphere = null
    if 'radiation' of data
      @Radiation=new AreaExposureInfo()
      @Radiation.deserialize data['radiation']
    else
      @Radiation = null
    if 'connected_positions' of data
      @ConnectedPositions = data['connected_positions']
    else
      @ConnectedPositions = []
    if 'encounters' of data
      @Encounters=new EncounterManager()
      @Encounters.deserialize data['encounters']
    else
      @Encounters = new EncounterManager()
    if 'avatar_id' of data
      @AvatarID = data['avatar_id']
    else
      @AvatarID = null
    if 'avatar_pip' of data
      @AvatarPip = data['avatar_pip']
    else
      @AvatarPip = null
    if 'overlays' of data
      @Overlays = data['overlays']
    else
      @Overlays = []
    if 'background' of data
      @Background = data['background']
    else
      @Background = null
    if console and console.error and _deser_errs > 0
      console and console.error "#{_deser_errs} DESERIALIZATION ERRORS in #{@constructor.name}: DATA: ", data
    # @deserializer-end
    return

  clone: (c=null) ->
    # If c was left unspecified, we act as the originating caller.
    if c == null or c == true or c == undefined
      c = new @constructor()
    super(c)
    # @cloner-start: c
    #AUTOGENERATED
    c.ID = @ID
    c.Name = @Name
    c.Flags = @Flags
    c.ZoneID = @ZoneID
    if @Atmosphere != undefined and @Atmosphere != null
      c.Atmosphere = @Atmosphere.clone()
    if @Radiation != undefined and @Radiation != null
      c.Radiation = @Radiation.clone()
    c.ConnectedPositions = @ConnectedPositions
    if @Encounters != undefined and @Encounters != null
      c.Encounters = @Encounters.clone()
    c.AvatarID = @AvatarID
    c.AvatarPip = @AvatarPip
    c.Overlays = @Overlays
    c.Background = @Background
    # @cloner-end
    return c
