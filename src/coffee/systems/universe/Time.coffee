﻿class TimeController extends GameObject
  constructor: ->
    super()

    @CalendarID='gregorian'
    # @serialize: no
    @Calendar = Calendar.GetCalendarByID 'gregorian'
    # @serialize: no
    @Planet = null

    @Timestamp = 1 # Seconds total since gamestart (epoch)
    @Delta = 60 # Seconds the next frame.
    @TimeLocked=true

    # @serialize: no
    @OnTimeAdvanced = new Event()

  SetCalendar: (id) ->
    @Calendar = Calendar.GetCalendarByID id
    return

  GetDaysInMonth: ->
    return @Calendar.DaysInMonth @Year, @Month

  Advance: (seconds=0, minutes=0, hours=0, days=0, weeks=0, months=0, years=0) ->
    oT = @Timestamp

    @Timestamp += seconds
    @Timestamp += minutes * MINUTE
    @Timestamp += hours * HOUR
    @Timestamp += days * @Calendar.HoursPerDay * HOUR
    @Timestamp += weeks * @Calendar.DaysPerWeek * @Calendar.HoursPerDay * HOUR
    @Timestamp += months * @Calendar.AverageDaysPerMonth * @Calendar.HoursPerDay * HOUR
    @Timestamp += years * @Calendar.DaysPerYear * @Calendar.HoursPerDay * HOUR

    @OnTimeAdvanced.Fire
      startTime:      oT
      endTime:        @Timestamp
      timeController: @
    return

  FormatTime: () ->
    return @Calendar.FormatTime(@Calendar.GetTime(@Timestamp))
  FormatDate: () ->
    return @Calendar.FormatDate(@Calendar.GetTime(@Timestamp))

  FormatDateTime: ->
    return @FormatDate()+' '+@FormatTime()

  GetSunrise: ->
    return @Calendar.HoursPerDay * 0.25
  GetSunset: ->
    return @Calendar.HoursPerDay * 0.75
  GetNoon: ->
    return @Calendar.HoursPerDay * 0.5

  @property 'IsDay',
    get: ->
      # @enumref: ETimeOfDay
      H = @Hour
      if H < @GetSunrise() or H > @GetSunset()
        return yes
      else
        return no

  GetTimeOfDay: ->
    # @enumref: ETimeOfDay
    H = @Hour
    if H < @GetSunrise() or H > @GetSunset()
      return ETimeOfDay.NIGHT
    else
      if H < @GetNoon()
        return ETimeOfDay.MORNING
      else
        if H < (@Calendar.HoursPerDay*0.625) # 3PM on earth (3+12 / 24)
          return ETimeOfDay.AFTERNOON
        else
          return ETimeOfDay.EVENING
    return ETimeOfDay.ERROR

  GetTODText: ->
    # @enumref: ETimeOfDay
    switch @GetTimeOfDay()
      when ETimeOfDay.NIGHT
        return "Night"
      when ETimeOfDay.MORNING
        return "Morning"
      when ETimeOfDay.AFTERNOON
        return "Afternoon"
      when ETimeOfDay.EVENING
        return "Evening"
      else
        return '???'

  @property 'Second',
    get: ->
      return @Calendar.GetTime(@Timestamp).Seconds
  @property 'Minute',
    get: ->
      return @Calendar.GetTime(@Timestamp).Minutes
  @property 'Hour',
    get: ->
      return @Calendar.GetTime(@Timestamp).Hours
  @property 'Day',
    get: ->
      return @Calendar.GetTime(@Timestamp).Days
  @property 'Week',
    get: ->
      return @Calendar.GetTime(@Timestamp).Weeks
  @property 'Month',
    get: ->
      return @Calendar.GetTime(@Timestamp).Months
  @property 'Year',
    get: ->
      return @Calendar.GetTime(@Timestamp).Years
