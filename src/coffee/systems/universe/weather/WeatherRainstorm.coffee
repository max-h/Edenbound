﻿class WeatherRainstorm extends BaseWeather
  constructor: ->
    super 'RAINSTORM'
    @Text='Rain'
    @Overlays = ['weather-rain', 'weather-wind']
    @Class=EWXClass.PRECIPITATION
    @WindSpeed.Min=5
    @WindSpeed.Min=15

  CanInstantiate: (u, wxc) ->
    return wxc.IsPrecipitating and not wxc.IsFreezing

BaseWeather.Register WeatherRainstorm
