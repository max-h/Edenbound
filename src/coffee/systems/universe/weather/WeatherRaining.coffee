﻿class WeatherRaining extends BaseWeather
  constructor: ->
    super 'RAIN'
    @Text='Rain'
    @Overlays = ['weather-rain']
    @Class=EWXClass.PRECIPITATION

  CanInstantiate: (u, wxc) ->
    return wxc.IsPrecipitating and not wxc.IsFreezing
BaseWeather.Register WeatherRaining
