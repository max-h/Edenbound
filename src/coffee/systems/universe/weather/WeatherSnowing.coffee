﻿class WeatherSnow extends BaseWeather
  constructor: ->
    super 'SNOW'
    @Text='Snow'
    @Overlays = ['weather-snowing']
    @Class=EWXClass.PRECIPITATION

  CanInstantiate: (u, wxc) ->
    return u.Weather.IsPrecipitating and u.Weather.IsFreezing
BaseWeather.Register WeatherSnow
