﻿class WeatherThunderstorm extends BaseWeather
  constructor: ->
    super 'THUNDERSTORM'
    @Text='Thunderstorm'
    @Overlays = ['weather-rain', 'weather-wind', 'weather-lightning']
    @Class=EWXClass.PRECIPITATION
    @Flags=EWXFlags.LIGHTNING
    @WindSpeed.Min=5
    @WindSpeed.Min=15

  CanInstantiate: (u, wxc) ->
    # TODO: Needs high humidity and high temperature.
    return false # @Universe.Weather.IsPrecipitating and not @Universe.Weather.IsFreezing

BaseWeather.Register WeatherThunderstorm
