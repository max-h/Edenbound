﻿class WeatherPartialClouds extends BaseWeather
  constructor: ->
    super 'PARTIALLY-CLOUDY'
    @Text='Partial Clouds'
    @Overlays = ['weather-partially-cloudy']
    @Class=EWXClass.CLOUD_COVER

  CanInstantiate: (u, wxc) ->
    return wxc.IsPartiallyCloudy

BaseWeather.Register WeatherPartialClouds
