﻿class WeatherOvercast extends BaseWeather
  constructor: ->
    super 'OVERCAST'
    @Text='Overcast'
    @BaseIcon='weather-overcast'
    @ImpliedOverlays = ['weather-partially-cloudy']
    @Class=EWXClass.CLOUD_COVER

  CanInstantiate: (u, wxc) ->
    return wxc.CloudCover >= 33

BaseWeather.Register WeatherOvercast
