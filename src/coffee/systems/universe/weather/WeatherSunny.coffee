﻿class WeatherSunny extends BaseWeather
  constructor: ->
    super 'CLEAR'
    @Name = 'Clear'

  CanInstantiate: (u, wxc) ->
    return wxc.CloudCover < 33

  Init: (u)->
    super(u)
    if @Universe.Time.IsDay
      @BaseIcon='weather-day-sun'
    else
      @BaseIcon='weather-night-moon'

BaseWeather.Register WeatherSunny
