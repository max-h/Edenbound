﻿class EncounterSpec extends GameObject
  @ALL: {}
  @TESTERS: []

  @Register: (cls) ->
    instance = new cls() # @depdetect: ignore
    @ALL[instance.Type] = cls
    @TESTERS.push cls

  @CreateAndDeserialize: (data) ->
    for ecls in @TESTERS
      espec = new ecls()
      if espec.matches data
        espec.deserialize data
        return espec
    return null
    
  constructor: (typeID)->
    super()

    # @enumref: EEncounterType
    # @serialize: no
    @Type = typeID
    ###
    # How many tickets this gets to put into the hat.
    ###
    @Weight = 1

  matches: (record) ->
    return @Type of record

  execute: ->
    return

  serialize: ->
    data = super()
    # @serializer-start: data
    #AUTOGENERATED
    if @Weight != 1
      data['weight'] = @Weight
    # @serializer-end
    return data

  deserialize: (data) ->
    super(data)
    # @deserializer-start: data
    #AUTOGENERATED
    _deser_errs = 0
    if 'weight' of data
      @Weight = data['weight']
    else
      @Weight = 1
    if console and console.error and _deser_errs > 0
      console and console.error "#{_deser_errs} DESERIALIZATION ERRORS in #{@constructor.name}: DATA: ", data
    # @deserializer-end
    return

  clone: (c=null) ->
    # If c was left unspecified, we act as the originating caller.
    if c == null or c == true or c == undefined
      c = new @constructor()
    super(c)
    # @cloner-start: c
    #AUTOGENERATED
    c.Weight = @Weight
    # @cloner-end
    return c
