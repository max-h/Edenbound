﻿class EdeneseCalendar extends SimplifiedGregorianCalendar
  constructor: ->
    super('eden')

    # Real life unix epoch would be [1970, 1, 1]
    @Epoch = new TimeInstance 157, 14, 10, 0, 0, 0

    # Orbital stuff.
    @DaysPerYear=480
    # Each season is exactly 120 days long.
    # To easily subdivide that, each season is divided into 4 months.
    # This means each month is 25 days long.
    month_length = 25
    @DaysPerWeek = 5
    # 28-hour days, will be fun.
    @HoursPerDay = 28

    @AverageDaysPerMonth=month_length

    # In case I decide to fuck with time
    @MinutesPerHour=60
    @SecondsPerMinute=60


    @MonthNames=[
      null
      'Janis'     # 1 - WINTER
      'Febris'    # 2
      'Chis'      # 3
      'Mart'      # 4 - SPRING
      'Apis'      # 5
      'Maius'     # 6 - From Roman calendar
      'Longis'    # 7 - Eric Long
      'Jun'       # 8 - SUMMER
      'Jul'       # 9
      'Geris'     # 10 - Gerald Solomon
      'Aagist'    # 11
      'September' # 12 - AUTUMN
      'Oktob'     # 13
      'Novim'     # 14
      'Welkim'    # 15 - Welkis' month
      'Decim'     # 16 - WINTER
    ]

    @DaysPerMonth = [-1]
    for i in [0...16]
      @DaysPerMonth.push month_length

    @DayNames = [
      null
      "Godsday"
      "Lab",
      "Tos",
      "Thur",
      "Ebb",
    ]

Calendar.Register EdeneseCalendar
