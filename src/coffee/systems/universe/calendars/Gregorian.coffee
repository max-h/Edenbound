﻿# Largely based on:
#   https://github.com/python/cpython/blob/master/Lib/datetime.py
class GregorianCalendar extends Calendar
  constructor: ->
    super('gregorian')

    ###
    # This is the moment the game starts. (New Game 1.tw)
    # Translates to July 9th, 2572 at 13:36:22 (1:36:22PM)
    ###
    @Epoch = new TimeInstance 2572, 7, 9, 13, 36, 22

    # Orbital stuff.
    @DaysPerYear=365
    @DaysPerWeek=7
    @HoursPerDay=24
    @AverageDaysPerMonth=31
    # In case I decide to fuck with time
    @MinutesPerHour=60
    @SecondsPerMinute=60

    @LeapYearInterval = 4
    @LeapYearPatternRepeatsInYears = 400
    @LeapMonth = 2
    @LeapMonthDays = 29
    @LeapYearsPerRepetitionPeriod = 24 # Every 400 years has 24 leap years.

    # Unexplained magic numbers in python's datetime.py
    @Magic1 = 50
    @Magic2 = 5

    # Calendar
    @MonthNames=[
      null
      'January'
      'February'
      'March'
      'April'
      'May'
      'June'
      'July'
      'August'
      'September'
      'October'
      'November'
      'December'
    ]
    # @serialize: no
    @DaysPerMonth = [
      -1,
      31,
      28,
      31,
      30,
      31,
      30,
      31,
      31,
      30,
      31,
      30,
      31
    ]
    # @serialize: no
    @DayNames = [
      null,
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday"
    ]

    @_DaysSinceJan1=[]
    @_EpochSecs=0
    @_SecondsPerHour=0
    @_SecondsPerDay=0
    @_DI400Y =0
    @_DI100Y=0
    @_DI4Y=0


  IsLeap: (year) ->
    ###
    _is_leap
    year -> 1 if leap year, else 0.
    ###
    return (year % @LeapYearInterval) == 0 and ((year % 100) != 0 or (year % @LeapYearPatternRepeatsInYears) == 0)

  DaysBeforeYear: (year) ->
    # _days_before_year from python.
    console.log "DaysBeforeYear: year=#{year}, @DaysPerYear=#{@DaysPerYear}, @LeapYearPatternRepeatsInYears=#{@LeapYearPatternRepeatsInYears}"
    y = year - 1
    return y*@DaysPerYear + divF(y,@LeapYearInterval) - divF(y,100) + divF(y,@LeapYearPatternRepeatsInYears)

  DaysInMonth: (year, month) ->
    if month == @LeapMonth and @IsLeap(year)
      return @LeapMonthDays
    return @DaysPerMonth[month]

  DaysBeforeMonth: (year, month) ->
    ###
    _days_before_month

    year, month -> number of days in year preceding first day of month.
    ###
    console.log "DaysBeforeMonth: year=#{year}, month=#{month}, @_DaysSinceJan1=", @_DaysSinceJan1
    return @_DaysSinceJan1[month] + (if month>2 and @IsLeap(year) then 1 else 0)

  DateToOrdinal: (year, month, day) ->
    ###
    _ymd2ord

    year, month, day -> ordinal, considering 01-Jan-0001 as day 1.
    ###
    #dim = @DaysInMonth year, month
    return @DaysBeforeYear(year) + @DaysBeforeMonth(year, month) + day

  @property 'MonthsPerYear',
    get: ->
      return @MonthNames.length - 1


  DaysToDate: (n) ->
    console.log "GregorianCalendar.DaysToDate(#{n})"
    ###
    _ord2ymd(n):

    ordinal -> (year, month, day), considering 01-Jan-0001 as day 1.

    # n is a 1-based index, starting at 1-Jan-1.  The pattern of leap years
    # repeats exactly every 400 years.  The basic strategy is to find the
    # closest 400-year boundary at or before n, then work with the offset
    # from that boundary to n.  Life is much clearer if we subtract 1 from
    # n first -- then the values of n at 400-year boundaries are exactly
    # those divisible by _DI400Y:
    #
    #     D  M   Y            n              n-1
    #     -- --- ----        ----------     ----------------
    #     31 Dec -400        -_DI400Y       -_DI400Y -1
    #      1 Jan -399         -_DI400Y +1   -_DI400Y      400-year boundary
    #     ...
    #     30 Dec  000        -1             -2
    #     31 Dec  000         0             -1
    #      1 Jan  001         1              0            400-year boundary
    #      2 Jan  001         2              1
    #      3 Jan  001         3              2
    #     ...
    #     31 Dec  400         _DI400Y        _DI400Y -1
    #      1 Jan  401         _DI400Y +1     _DI400Y      400-year boundary
    ###
    n -= 1
    [n400, n] = divmod(n, @_DI400Y)
    year = n400 * @LeapYearPatternRepeatsInYears + 1   # ..., -399, 1, 401, ...

    # Now n is the (non-negative) offset, in days, from January 1 of year, to
    # the desired date.  Now compute how many 100-year cycles precede n.
    # Note that it's possible for n100 to equal 4!  In that case 4 full
    # 100-year cycles precede the desired day, which implies the desired
    # day is December 31 at the end of a 400-year cycle.
    [n100, n] = divmod(n, @_DI100Y)

    # Now compute how many 4-year cycles precede it.
    [n4, n] = divmod(n, @_DI4Y)

    # And now how many single years.  Again n1 can be 4, and again meaning
    # that the desired day is December 31 at the end of the 4-year cycle.
    [n1, n] = divmod(n, @DaysPerYear)

    # Added parentheses in case of weird OOO stuff in JS. - VG
    year += (n100 * 100) + (n4 * @LeapYearInterval) + n1
    if n1 == @LeapYearInterval or n100 == @LeapYearInterval
      #assert n == 0
      #return year-1, 12, 31
      return [year-1, @MonthsPerYear, @DaysInMonth(year-1, @MonthsPerYear)]

    # Now the year is correct, and n is the offset from January 1.  We find
    # the month via an estimate that's either exact or one too large.
    leapyear = n1 == (@LeapYearInterval - 1) and (n4 != @LeapYearsPerRepetitionPeriod or n100 == (@LeapYearInterval - 1))
    #assert leapyear == _is_leap(year)
    # TODO: What the fuck is going on here? I understand we're adding and then barrel-shifting to the right, but why these numbers? - VG
    #month = (n + 50) >> 5
    month = (n + @Magic1) >> @Magic2
    preceding = @_DaysSinceJan1[month] + (month > 2 and leapyear)
    if preceding > n  # estimate is too large
      month -= 1
      preceding -= @DaysPerMonth[month] + (month == 2 and leapyear)
    n -= preceding
    #assert 0 <= n < @DaysInMonth(year, month)

    # Now the year and month are correct, and n is the offset from the
    # start of that month:  we're done!
    return [year, month, n+1]

  CalcStatics: ->
    @_DI400Y = @DaysBeforeYear(401) # number of days in 400 years
    @_DI100Y = @DaysBeforeYear(101) # number of days in 400 years
    @_DI4Y =   @DaysBeforeYear(5) # number of days in 400 years

    @_SecondsPerHour=@SecondsPerMinute*@MinutesPerHour
    @_SecondsPerDay=@_SecondsPerHour*@HoursPerDay

    ###
    _DAYS_BEFORE_MONTH = [-1]  # -1 is a placeholder for indexing purposes.
    dbm = 0
    for dim in _DAYS_IN_MONTH[1:]:
        _DAYS_BEFORE_MONTH.append(dbm)
        dbm += dim
    del dbm, dim
    ###
    @_DaysSinceJan1 = [-1]
    daysFromYearStart=0
    for daysInThisMonth in @DaysPerMonth.slice(1)
      @_DaysSinceJan1.push daysFromYearStart
      daysFromYearStart += daysInThisMonth

    @_EpochSecs=@TimeToSecs @Epoch
    return

  # This is going to be an ugly hack but I'm tired of digging through glibc. - VG
  GetTime: (secs, offset=true) ->
    ts = new TimeInstance()
    if offset
      secs += @_EpochSecs
      ts.Offset = @_EpochSecs

    [ts.Day, hours]        = divmod secs, @_SecondsPerDay
    [ts.Hour, minutes]     = divmod hours, @_SecondsPerHour
    [ts.Minute, ts.Second] = divmod minutes, @SecondsPerMinute

    [ts.Year, ts.Month, ts.Day] = @DaysToDate ts.Day

    ts.Weekday = Math.floor((secs / @_SecondsPerDay + 1) % @DaysPerWeek) # day of week
    ts.WeekdayName = @DayNames[ts.Weekday+1]

    return ts

  TimeToSecs: (ts) ->
    o = ts.Second
    o += ts.Minute * @SecondsPerMinute
    o += ts.Hour * @_SecondsPerHour
    o += @DateToOrdinal(ts.Year, ts.Month, ts.Day) * @_SecondsPerDay
    if ts.Offset > 0
      o -= ts.Offset
    return o

  GetDayOfWeek: (ts) ->
    return (@DateToOrdinal(ts) % @DaysPerWeek)+1
  GetWeekDayName: (day) ->
    return @DayNames[day]

  GetDayOfYear: (ts) ->
    return @DaysBeforeMonth(ts.Year, ts.Month) + ts.Day


Calendar.Register GregorianCalendar
