﻿###
# Horribly butchered version of
#     https://github.com/python/cpython/blob/master/Lib/datetime.py
# And https://stackoverflow.com/questions/11188621/how-can-i-convert-seconds-since-the-epoch-to-hours-minutes-seconds-in-java/11197532#11197532
# Does not handle leap years.
###
class SimplifiedGregorianCalendar extends Calendar
  constructor: (id) ->
    super(id)

    ###
    # This is the day the game starts.
    # Real life unix epoch would be [1970, 1, 1]
    ###
    @Epoch = new TimeInstance 2572, 1, 1, 0, 0, 0

    # Orbital stuff.
    @DaysPerYear=365
    @DaysPerWeek=7
    @HoursPerDay=24
    # In case I decide to fuck with time
    @MinutesPerHour=60
    @SecondsPerMinute=60

    # Calendar
    @MonthNames=[
      null
      'January'
      'February'
      'March'
      'April'
      'May'
      'June'
      'July'
      'August'
      'September'
      'October'
      'November'
      'December'
    ]
    # @serialize: no
    @DaysPerMonth = [
      -1,
      31,
      28,
      31,
      30,
      31,
      30,
      31,
      31,
      30,
      31,
      30,
      31
    ]
    # @serialize: no
    @DayNames = [
      null,
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday"
    ]

    @_DaysSinceJan1=[]
    @_EpochSecs = 0
    @_SecondsPerHour=0
    @_SecondsPerDay=0

  IsLeap: (year) ->
    return no

  DaysBeforeYear: (year) ->
    console.log "DaysBeforeYear: year=#{year}, @DaysPerYear=#{@DaysPerYear}"
    y = year - 1
    return y*@DaysPerYear

  DaysInMonth: (year, month) ->
    return @DaysPerMonth[month]

  DaysBeforeMonth: (year, month) ->
    console.log "DaysBeforeMonth: year=#{year}, month=#{month}, @_DaysSinceJan1=", @_DaysSinceJan1
    return @_DaysSinceJan1[month]

  DateToOrdinal: (year, month, day) ->
    ###
    _ymd2ord

    year, month, day -> ordinal, considering 01-Jan-0001 as day 1.
    ###
    #dim = @DaysInMonth year, month
    return @DaysBeforeYear(year) + @DaysBeforeMonth(year, month) + day


  GetTime: (n, offset=true) ->
    ts = new TimeInstance()
    if offset
      ts.Offset = @_EpochSecs
      n += @_EpochSecs

    ts.Weekday = Math.floor((n / @_SecondsPerDay + 1) % @DaysPerWeek) # day of week
    ts.WeekdayName = @DayNames[ts.Weekday+1]

    [ts.Year,  n1]  = divmod n, @_SecondsPerDay*@DaysPerYear
    [yday, n2]      = divmod n1, @_SecondsPerDay
    [ts.Hour, n3]   = divmod n2, @_SecondsPerHour
    [ts.Minute, n4] = divmod n3, @SecondsPerMinute
    ts.Second = n4

    ts.Day=1
    for month in [1..@MonthsPerYear]
      if yday < @_DaysSinceJan1[month]
        ts.Day += yday - @_DaysSinceJan1[month - 1]
        ts.Month = month - 1
        break

    return ts

  TimeToSecs: (ts) ->
    o = ts.Second
    o += ts.Minute * @SecondsPerMinute
    o += ts.Hour * @_SecondsPerHour
    o += @DateToOrdinal(ts.Year, ts.Month, ts.Day) * @_SecondsPerDay
    if ts.Offset > 0
      o -= ts.Offset
    return o

  CalcStatics: ->

    @_SecondsPerHour = @SecondsPerMinute*@MinutesPerHour
    @_SecondsPerDay = @_SecondsPerHour*@HoursPerDay
    @_SecondsPerMonth = @_SecondsPerDay*@DaysPerMonth

    @_DaysSinceJan1 = [-1]
    daysFromYearStart=0
    for daysInThisMonth in @DaysPerMonth.slice(1)
      @_DaysSinceJan1.push daysFromYearStart
      daysFromYearStart += daysInThisMonth

    @_EpochSecs = @TimeToSecs @Epoch
    return

  GetDayOfWeek: (ts) ->
    return (@DateToOrdinal(ts) % @DaysPerWeek)+1
  GetWeekDayName: (day) ->
    return @DayNames[day]

  GetDayOfYear: (ts) ->
    return @DaysBeforeMonth(ts.Year, ts.Month) + ts.Day
