﻿class Calendar
  @ALL: {}

  @Register: (cls) ->
    i=new cls() # @depdetect: ignore
    i.CalcStatics()
    @ALL[i.ID]=i

  @GetCalendarByID: (id) ->
    return @ALL[id]

  constructor: (@ID) ->
    @Epoch = null

    # Orbital stuff.
    @DaysPerYear=0
    @DaysPerWeek=0
    @DaysPerMonth=[-1]
    @HoursPerDay=24
    @AverageDaysPerMonth=0

    # In case I decide to fuck with time
    @MinutesPerHour=60
    @SecondsPerMinute=60

    # Calendar
    # https://github.com/python/cpython/blob/master/Lib/datetime.py
    @MonthNames=[-1]
    @DaysPerMonth = [-1]
    @DayNames = [null]

  GetDayOfWeek: (ts) ->
    return 0

  GetMonthName: (month) ->
    return @MonthNames[month]
  GetDayName: (day) ->
    return @DayNames[day]
  GetDayOfYear: (ts) ->
    return 0

  FormatDate: (ts) ->
    return "#{@GetDayName(ts.Weekday)}, #{@GetMonthName(ts.Month)} #{ts.Day}, #{ts.Year}"
  FormatTime: (ts) ->
    return "#{ts.Hour.toString().padStart(2, '0')}:#{ts.Minute.toString().padStart(2, '0')}:#{ts.Second.toString().padStart(2, '0')}"

  GetTime: (secs, offset=true) ->
    return new TimeInstance(1,1,1,1,1,1,1)
  TimeToSecs: (ts) ->
    return 0

  @property 'MonthsPerYear',
    get: ->
      return @MonthNames.length - 1
