﻿# A purely political concept with geographical borders.
class Country extends GameObject
  constructor: ->
    super()
    # @serialize: no
    @Universe = null
    # @serialize: name=id required
    @ID = ''
    # @serialize: required
    @Name=''
    @ShortName=''
    @Acronym=''
    # @serialize: name=home_region
    @HomeRegionID=''
    # @serialize: name=capital_zone
    @CapitalZoneID=''
    # @serialize: name=planet required
    @PlanetID=''
    # @serialize: array scalar
    @Rulers=[]
    # @serialize: scalar required
    @RuleType=ERuleType.ANARCHY
    # @serialize: name=gdp required
    @GDP=0

    # @serialize: scalar
    @Flags = ECountryFlags.NONE


  @property 'HomeRegion',
    get: ->
      return @Universe.Regions[@HomeRegionID]

  @property 'CapitalZone',
    get: ->
      return @Universe.Zones[@CapitalZoneID]

  @property 'Planet',
    get: ->
      return @Universe.Planets[@PlanetID]


  serialize: ->
    data = super()
    # @serializer-start: data
    #AUTOGENERATED
    data['id'] = @ID
    data['name'] = @Name
    if @ShortName != ''
      data['short_name'] = @ShortName
    if @Acronym != ''
      data['acronym'] = @Acronym
    if @HomeRegionID != ''
      data['home_region'] = @HomeRegionID
    if @CapitalZoneID != ''
      data['capital_zone'] = @CapitalZoneID
    data['planet'] = @PlanetID
    if @Rulers != null and @Rulers.length > 0
      data['rulers'] = @Rulers
    data['rule_type'] = @RuleType
    data['gdp'] = @GDP
    if @Flags != ECountryFlags.NONE
      data['flags'] = @Flags
    # @serializer-end
    return data

  deserialize: (data) ->
    super(data)
    # @deserializer-start: data
    #AUTOGENERATED
    _deser_errs = 0
    if 'id' not of data
      console and console.error "DESERIALIZATION ERROR in #{@constructor.name}: id is missing from the serialized data!"
      _deser_errs++
    @ID = data['id']
    if 'name' not of data
      console and console.error "DESERIALIZATION ERROR in #{@constructor.name}: name is missing from the serialized data!"
      _deser_errs++
    @Name = data['name']
    if 'short_name' of data
      @ShortName = data['short_name']
    else
      @ShortName = ''
    if 'acronym' of data
      @Acronym = data['acronym']
    else
      @Acronym = ''
    if 'home_region' of data
      @HomeRegionID = data['home_region']
    else
      @HomeRegionID = ''
    if 'capital_zone' of data
      @CapitalZoneID = data['capital_zone']
    else
      @CapitalZoneID = ''
    if 'planet' not of data
      console and console.error "DESERIALIZATION ERROR in #{@constructor.name}: planet is missing from the serialized data!"
      _deser_errs++
    @PlanetID = data['planet']
    if 'rulers' of data
      @Rulers = data['rulers']
    else
      @Rulers = []
    if 'rule_type' not of data
      console and console.error "DESERIALIZATION ERROR in #{@constructor.name}: rule_type is missing from the serialized data!"
      _deser_errs++
    @RuleType = data['rule_type']
    if 'gdp' not of data
      console and console.error "DESERIALIZATION ERROR in #{@constructor.name}: gdp is missing from the serialized data!"
      _deser_errs++
    @GDP = data['gdp']
    if 'flags' of data
      @Flags = data['flags']
    else
      @Flags = ECountryFlags.NONE
    if console and console.error and _deser_errs > 0
      console and console.error "#{_deser_errs} DESERIALIZATION ERRORS in #{@constructor.name}: DATA: ", data
    # @deserializer-end
    return

  clone: (c=null) ->
    # If c was left unspecified, we act as the originating caller.
    if c == null or c == undefined or typeof(c) == typeof(true)
      c = new @constructor()
    super(c)
    # @cloner-start: c
    #AUTOGENERATED
    c.ID = @ID
    c.Name = @Name
    c.ShortName = @ShortName
    c.Acronym = @Acronym
    c.HomeRegionID = @HomeRegionID
    c.CapitalZoneID = @CapitalZoneID
    c.PlanetID = @PlanetID
    c.Rulers = @Rulers
    c.RuleType = @RuleType
    c.GDP = @GDP
    c.Flags = @Flags
    # @cloner-end
    return c
