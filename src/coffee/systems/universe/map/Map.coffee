﻿class TileMap
  constructor: ->
    @Zoom = 0
    @Background = ''
    @Overlay = ''
    @Tiles = []

    @_MapBox = null
    @_MapInterior = null
    @_Context = null

    @TileSize = 64
    @Height=0
    @Width=0

  Connect: ->
    @_MapBox = $ '#map-box'
    @_MapInterior = $ '<canvas>'
    @_MapBox.append @_MapInterior
    @_Context = @_MapInterior.get(0).getContext '2d'
    return

  Disconnect: ->
    if not @_MapBox.empty()
      @_MapBox.html()
    @_Context = null
    @_MapInterior = null
    @_MapBox = null
    @Tiles=[]

  GetTileIndex: (x,y) ->
    return x+(y*@Width)

  GetTileAt: (x,y) ->
    return @Tiles[@GetTileAt(x,y)]

  Update: ->
    if @_MapBox is null
      @Connect()

    $universe = Universe.GetInstance()
    @Tiles = []
    map = @_MapInterior
    map.html ''
    if $universe.Zone == null or $universe.Zone.Map == null
      map.css 'background-image', getImageBlob('static')
      @Height = @Width = 0
    else
      @Height = $universe.Zone.Height
      @Width = $universe.Zone.Width

      map.css 'background-image', getImageBlob(@Background)
      map.css 'height', @Height
      map.css 'width', @Width

      for idx, position in $universe.Zone.Map or []
        t = new Tile @_Context, position
        @Tiles.push t
    return

  Draw: () ->
    $universe = Universe.GetInstance()
    $player = $universe.Player
    $position = $universe.Position or {X: 0, Y: 0}

    tile = null

    startCol = Math.floor($position.X / @TileSize)
    endCol = startCol + (@ViewportWidth / @TileSize)
    startRow = Math.floor($position.Y / @TileSize)
    endRow = startRow + (@ViewportHeight / @TileSize)

    for ty in [startRow..endRow]
      for tx in [startCol..endCol]
        if tile = @GetTileAt tx, ty
          tile.RenderForCanvas @, @_Context, tx, ty
        else
          # Red square if tile is empty.
          @_Context.fillStyle='#ff0000'
          @_Context.fillRect tx, ty, @TileSize, @TileSize
