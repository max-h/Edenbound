﻿class Tile
  constructor: (@Map, @Position) ->
    @_DisplayedObjects=[]

  Update: ->
    @_DisplayedObjects=[]
    if @Position.Background
      bgimg = new Image()
      bgimg.src = @Position.Background
      @_DisplayedObjects.push bgimg
    for imgsrc in @Position.Overlays
      img = new Image()
      img.src = imgsrc
      @_DisplayedObjects.push img
    return

  RenderForCanvas: (map, ctx, x, y) ->
    for d_o in @_DisplayedObjects
      ctx.drawImage d_o,
        x*@Map.TileSize
        y*@Map.TileSize
    return
