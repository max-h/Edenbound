﻿class Universe extends GameObject
  constructor: ->
    super()
    # @serialize: clone-only scalar
    @Planets = {}
    # @serialize: clone-only scalar
    @Countries = {}
    # @serialize: clone-only scalar
    @Regions = {}
    # @serialize: clone-only scalar
    @Zones = {}

    ###
    # Current planet
    # @serialize: object=Planet
    ###
    @Planet = null
    ###
    # Current country
    # @serialize: object=Country
    ###
    @Country = null
    ###
    # Current Region
    # @serialize: object=Region
    ###
    @Region = null
    ###
    # Current zone
    # @serialize: object=Zone
    ###
    @Zone = null
    ###
    # Current Position
    # @serialize: object=Position
    ###
    @Position = null

    # @serialize: no
    @Player = null

    # @serialize: name=npcs object=NPCStore
    @NPCs=new NPCStore()

    # @serialize: object=TimeController required pass-self
    @Time = new TimeController()

    # @serialize: no
    @_header = $('#header')

    ###
    # NPC we're interacting with.
    # @serialize: no
    ###
    @Subject = null

    # @serialize: scalar
    @OverrideZone = null
    # @serialize: scalar
    @OverrideAvatar = null
    # @serialize: scalar
    @OverrideSubject = null
    # @serialize: scalar
    @OverrideRegion = null

    # @serialize: object=WeatherController required pass-self
    @Weather = new WeatherController()
    @Weather.HookInto @

    # @serialize: no
    @Map = new TileMap()

    @Load()
    return

  @GetInstance: ->
    return window.getVariables().universe

  _checkPlanet: (planet) ->
    for wxID in planet.Climate.WeatherTypes
      if wxID not of BaseWeather.ALL
        console.error "Planet %s - Climate.WeatherTypes: Invalid weather ID %s", planet.ID, wxID
    return

  Load: ->
    universe = JSON.parse Nylon.GetPassageByID('__UNIVERSE__').text
    for data in universe.planets
      console.log "Loading Planet #{data.id}: #{data.name}"
      t = Planet
      if 'type' of data
        t = window[data.type]
      i=new t() # @depdetect: ignore
      i.deserialize data
      i.Universe = @
      @_checkPlanet i
      @Planets[i.ID] = i

    for data in universe.regions
      console.log "Loading Region #{data.id}: #{data.name}"
      t = Region
      if 'type' of data
        t = window[data.type]
      i=new t() # @depdetect: ignore
      i.deserialize data
      i.Universe = @
      @Regions[i.ID] = i
      if i.PlanetID not of @Planets
        console.warn "Region #{i.ID} has incorrect planet ==", i.PlanetID

    for data in universe.countries
      console.log "Loading Country #{data.id}: #{data.name}"
      t = Country
      if 'type' of data
        t = window[data.type]
      i=new t() # @depdetect: ignore
      i.deserialize data
      i.Universe = @
      @Countries[i.ID] = i
      if i.PlanetID not of @Planets
        console.warn "Country #{i.ID} has incorrect planet ==", i.PlanetID

    for data in universe.zones
      console.log "Loading Zone #{data.id}: #{data.name}"
      t = Zone
      if 'type' of data
        t = window[data.type]
      i=new t() # @depdetect: ignore
      i.deserialize data
      i.Universe = @
      @Zones[i.ID] = i
      if i.RegionID not of @Regions
        console.warn "Zone #{i.ID} has incorrect region ==", i.RegionID

    return

  SetLocation: (planet=undefined, country=undefined, region=undefined, zone=undefined, position=undefined) ->
    if planet != undefined
      @SetPlanet   planet
    if country != undefined
      @SetCountry  country
    if region != undefined
      @SetRegion   region
    if zone != undefined
      @SetZone     zone
    if position != undefined
      @SetPosition position
    @UpdateHeader()
    return

  ForceLocation: (avatar_id=undefined, pip_value=undefined, subject_name=undefined, zone_name=undefined, region_name=undefined) ->
    #console.log "ForceLocation(avatar_id=#{avatar_id}, pip_value=#{pip_value}, subject_name=#{subject_name}, zone_name=#{zone_name}, region_name=#{region_name})"
    if avatar_id != undefined
      @OverrideAvatar  = avatar_id
    if pip_value != undefined
      @OverridePip     = pip_value
    if subject_name != undefined
      @OverrideSubject = subject_name
    if zone_name != undefined
      @OverrideZone    = zone_name
    if region_name != undefined
      @OverrideRegion  = region_name
    @UpdateHeader()

  UnforceLocation: () ->
    @OverrideAvatar  = null
    @OverridePip     = null
    @OverrideSubject = null
    @OverrideZone    = null
    @OverrideRegion  = null
    @UpdateHeader()

  GetZoneText: ->
    if @OverrideZone != null
      return @OverrideZone

    if @Zone == null
      if @Region != null
        return @Region.toString()
    else
      return @Zone.toString()
    return '???'

  GetRegionText: ->
    if @OverrideRegion != null
      return @OverrideRegion
    if @Region == null
      if @Planet and @Planet.Name
        return @Planet.Name
    else
      return @Region.toString()
    return '???'

  GetAvatarID: ->
    if @OverrideAvatar != null
      return @OverrideAvatar
    if @Subject != null
      if @Subject.AvatarID != null
        return @Subject.AvatarID
      else
        return 'avatar-unknown'
    if @Position != null and @Position.AvatarID != null
      return @Position.AvatarID
    if @Zone != null and @Zone.AvatarID != null
      return @Zone.AvatarID
    if @Region != null and @Region.AvatarID
      return @Region.AvatarID
    return 'avatar-error'

  GetAvatarPip: ->
    if @OverridePip != null
      return @OverridePip
    if @Subject != null
      if @Player.Mind.Memory.Met(@Subject)
        return @Player.Mind.Memory.GetMemoryOf(@Subject).ObservedGender
      else
        switch @Subject.GetApparentGender()
          when EGenderVote.MASCULINE
            return ECommonGenders.MALE
          when EGenderVote.FEMININE
            return ECommonGenders.FEMALE
          else
            return ECommonGenders.NEUTER
    ###
    if @Position != null and @Position.AvatarPip != null
      return @Position.AvatarPip
    if @Zone != null and @Zone.AvatarPip != null
      return @Zone.AvatarPip
    if @Region != null and @Region.AvatarPip
      return @Region.AvatarPip
    ###
    return EGenderVote.NEUTRAL

  GetSubjectText: ->
    if @OverrideSubject != null
      return @OverrideSubject
    if @Subject != null
      if @Player.Mind.Memory.Met(@Subject)
        return @Player.Mind.Memory.GetMemoryOf(@Subject).GetName()
      else
        return 'Unknown'
    if @Position != null
      return @Position.toString()
    return '???'


  UpdateHeader: ->
    $('#subjectName').text(@GetSubjectText())
    $('#subjectZone').text(@GetZoneText())
    $('#subjectRegion').text(@GetRegionText())

    svgPassage = Nylon.GetPassageByID(@GetAvatarID())
    if svgPassage == null or svgPassage == undefined
      svgPassage = Nylon.GetPassageByID('avatar-error')
      console.error "#{@GetAvatarID()} not found in passages!"
    $('#subjectAvatarSVG').removeClass 'glitch'

    if 'glitch' in svgPassage.tags
      $('#subjectAvatarSVG').addClass 'glitch'

    if 'png' in svgPassage.tags
      $('#subjectAvatarSVG').html('')
      $img = $('<img>')
      $img.attr 'src', svgPassage.text
      $('#subjectAvatarSVG').append $img
    else
      data = svgPassage.text
      if 'glitch' in svgPassage.tags
        for i in [0...2]
          data += svgPassage.text
        data = "<span class=\"placeholdericon\">#{svgPassage.text}</span><div id=\"glitchyicon\">#{data}</div>"
      $('#subjectAvatarSVG').html data

    pip = @GetAvatarPip()
    ePip = $('#subjectAvatarPip')
    allPossiblePipClasses=[
      'masculine'
      'feminine'
      'hermaphrodite'
      'neutral'
      'sexless'
    ]
    for cls in allPossiblePipClasses
      ePip.removeClass cls
    if pip
      switch pip
        when ECommonGenders.MALE
          ePip.addClass 'masculine'
          ePip.text '♂'
        when ECommonGenders.FEMALE
          ePip.addClass 'feminine'
          ePip.text '♀'
        when ECommonGenders.HERMAPHRODITE
          ePip.addClass 'hermaphrodite'
          ePip.text '⚥'
        else
          ePip.addClass 'neutral'
          ePip.text '\u26B2'

    $('#time').text @Time.FormatDateTime()
    $('#weather').text [@Time.GetTODText(), @Weather.Text].join(', ')

    return

  SetPlanet: (val) ->
    @Country=null
    @Region=null
    @Zone=null
    @Position=null

    if val == null
      @Planet = null # SPACE!

    switch typeof val
      when "string"
        if val not of @Planets
          throw new Error("Planet with ID #{val} not found!")
        @SetPlanet @Planets[val]
      when "object"
        @Planet = val
      else
        throw new Error("Cannot find a planet with this data type.")

  SetCountry: (val) ->
    if val == null
      @Country = null

    switch typeof val
      when "string"
        if val not of @Planet.Countries
          throw new Error("Country #{val} not found in @Planet.ID=#{@Planet.ID}!")
        @SetCountry @Planet.FindCountryByID(val)
      when "object"
        @Country = val
      else
        throw new Error("Cannot find a country with this data type.")
    return

  SetRegion: (val) ->
    @Zone=null
    @Position=null
    if val == null
      @Region = null
    switch typeof val
      when "string"
        if val not of @Planet.Regions
          throw new Error("Region #{val} not found in @Planet.ID=#{@Planet.ID}!")
        @SetRegion @Planet.FindRegionByID val
      when "object"
        @Region = val
      else
        throw new Error("Cannot find a region with this data type.")
    return

  SetZone: (val) ->
    @Position=null
    if val == null
      @Zone = null
    switch typeof val
      when "string"
        if val not of @Region.Zones
          throw new Error("Zone #{val} not found in @Region.ID=#{@Region.ID}!")
        @SetZone @Region.FindZoneByID val
      when "object"
        @Zone = val
      else
        throw new Error("Cannot find a locale with this data type.")
    return

  SetPosition: (x, y) ->
    if x == null and y == null
      @Position = null
    switch typeof(x)
      when "array"
        @SetPosition x[0], x[1]

      when "number"
        pos = @Zone.GetPosition(x, y)
        if pos == undefined
          throw new Error("Cannot find a position at <#{x}, #{y}> in #{@Zone.ID}.")
        @SetPosition pos
      when "object"
        @Position = x
      else
        tx = typeof(x)
        ty = typeof(y)
        throw new Error("Cannot find a position with data typeof(x)=#{tx}, typeof(y)=#{ty}. Acceptable: SetPosition(x, y), SetPosition([x, y]), SetPosition(new Position()).")
    return

  SetSubject: (val) ->
    @Subject = null
    if val == null
      return
    switch typeof val
      when 'string', 'function'
        @SetSubject @NPCs.Get(val)
      when 'object'
        @Subject = val
      else
        tval = typeof val
        throw new Error("Cannot find a position with data typeof(val)=#{tval}.")
  DrawStatBars: (full=no)->
    @Player.UpdateStats(full=full)

  AdvanceTime: (seconds=0,minutes=0,hours=0,days=0, weeks=0, months=0, years=0) ->
    @Time.Advance seconds, minutes, hours, days, weeks, months, years

  serialize: ->
    data = super()
    # @serializer-start: data
    #AUTOGENERATED
    if @Planet != undefined and @Planet != null
      data['planet'] = @Planet.serialize()
    if @Country != undefined and @Country != null
      data['country'] = @Country.serialize()
    if @Region != undefined and @Region != null
      data['region'] = @Region.serialize()
    if @Zone != undefined and @Zone != null
      data['zone'] = @Zone.serialize()
    if @Position != undefined and @Position != null
      data['position'] = @Position.serialize()
    if @NPCs != undefined and @NPCs != null
      data['npcs'] = @NPCs.serialize()
    if @Time != undefined and @Time != null
      data['time'] = @Time.serialize()
    if @OverrideZone != null
      data['override_zone'] = @OverrideZone
    if @OverrideAvatar != null
      data['override_avatar'] = @OverrideAvatar
    if @OverrideSubject != null
      data['override_subject'] = @OverrideSubject
    if @OverrideRegion != null
      data['override_region'] = @OverrideRegion
    if @Weather != undefined and @Weather != null
      data['weather'] = @Weather.serialize()
    # @serializer-end
    if @Subject != null
      data['subject'] = @Subject.ID
    return data

  deserialize: (data) ->
    super(data)
    # @deserializer-start: data
    #AUTOGENERATED
    _deser_errs = 0
    if 'planet' of data
      @Planet=new Planet()
      @Planet.deserialize data['planet']
    else
      @Planet = null
    if 'country' of data
      @Country=new Country()
      @Country.deserialize data['country']
    else
      @Country = null
    if 'region' of data
      @Region=new Region()
      @Region.deserialize data['region']
    else
      @Region = null
    if 'zone' of data
      @Zone=new Zone()
      @Zone.deserialize data['zone']
    else
      @Zone = null
    if 'position' of data
      @Position=new Position()
      @Position.deserialize data['position']
    else
      @Position = null
    if 'npcs' of data
      @NPCs=new NPCStore()
      @NPCs.deserialize data['npcs']
    else
      @NPCs = new NPCStore()
    if 'time' not of data
      console and console.error "DESERIALIZATION ERROR in #{@constructor.name}: time is missing from the serialized data!"
      _deser_errs++
    @Time=new TimeController()
    @Time.deserialize data['time'], @
    if 'override_zone' of data
      @OverrideZone = data['override_zone']
    else
      @OverrideZone = null
    if 'override_avatar' of data
      @OverrideAvatar = data['override_avatar']
    else
      @OverrideAvatar = null
    if 'override_subject' of data
      @OverrideSubject = data['override_subject']
    else
      @OverrideSubject = null
    if 'override_region' of data
      @OverrideRegion = data['override_region']
    else
      @OverrideRegion = null
    if 'weather' not of data
      console and console.error "DESERIALIZATION ERROR in #{@constructor.name}: weather is missing from the serialized data!"
      _deser_errs++
    @Weather=new WeatherController()
    @Weather.deserialize data['weather'], @
    if console and console.error and _deser_errs > 0
      console and console.error "#{_deser_errs} DESERIALIZATION ERRORS in #{@constructor.name}: DATA: ", data
    # @deserializer-end
    @Weather.HookInto @
    @Subject = null
    if 'subject' of data
      @Subject = NPCs.GetByID data.subject
    return

  clone: () ->
    return @

window.advanceTime = (seconds=0, minutes=0, hours=0, days=0, weeks=0, months=0, years=0) ->
  getVariables().universe.Time.Advance seconds, minutes, hours, days, weeks, months, years
  return
