﻿class DisplayElement
  constructor: (@Parent, @Element) ->

  Layout: (x,y,height,width) =>
    # Signal sent from the parent, should resize/position child element(s).

  SetPos: (@X,@Y) ->
    if @Element
      setElementPosition(@Element,@X,@Y)
    return

  Refresh: () =>
    # Stat thing


  Hide: (duration=400,delay=0) =>
    if @Element
      @Element.delay(delay).fadeOut(duration)

  Show: (duration=400,delay=0) =>
  #console.log("Fading in after #{delay/1000} seconds.")
    if @Element
      @Element.delay(delay).fadeIn(duration)
