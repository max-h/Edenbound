﻿###
[=[HP: 50%]====>      ]
Needs 4 spans:
<span class="statbar">
  <span class="label">HP:</span>
  <span class="value">50%</span>
  <span class="bar"></span>
</span>
###
TICKS_PER_SECOND=20
class StatBar extends DisplayElement
  constructor: (@View, @Name, @Label, @Format="{value}") ->
    super(null,null)
    @Callback = null
    @Element=null
    #super(element,X,Y)
    @BuildHTML()
    @Element.click =>
      if @Callback != null
        @Callback(@)
    @Value=0
    @Set 0, 100

    @Stat=null

    @_CurrentMax=0 # Used in bindings

  BuildHTML: =>
    @Element=$('<div>')
    @Element.addClass('statbar')
    @LabelName=$('<span>').addClass('label').attr('title',@Name)
    @LabelValue=$('<span>').addClass('value').attr('title',@Name)
    @Bar=$('<span>').addClass('bar')
    @Element.append(
      @LabelName,
      @LabelValue
      @Bar
    )

  Layout: (x,y,height,width) =>
    setElementPosition @Element, x, y
    @Element.outerHeight height
    @Element.outerWidth width

    setElementPosition @Label, x + 16, y
    setElementPosition @Bar, x, y

  LinkStat: (@Stat) =>
    @Stat.OnChanged.Attach(@Refresh)
    @Refresh()

  Refresh: () =>
    #@Element.attr('id','stat_'+@Stat.Name.toLowerCase())
    if @Stat
      #console.log "Refreshing #{@Name}: Type #{@Stat.Type}"
      if @Stat.Type == EStatType.NUMERIC
        console.log("Refreshing #{@Name}: #{@Stat.Value} #{@Stat.Max}")
        @Set @Stat.Value, @Stat.Max
      if @Stat.Type == EStatType.STRING
        console.log("Refreshing #{@Name}: #{@Stat.Value}")
        @SetString @Stat.Value
      @SetAlert @Stat.Alert

  Clear: =>
    #console.log("Clearing #{@Name}")
    @Set "", ""

  SetText: (text) =>
    @Element.text(text)
    if text == ''
      @Hide()
    else
      @Show()

  GetText: =>
    @Label.text()

  SetText: (text)=>
    @Label.text(text)

  SetTooltip: (text) =>
    @Element.attr('title', text)
    @LabelName.attr('title', text)
    @LabelValue.attr('title', text)

  GetTooltip: =>
    @Element.attr('title')

  Enable: =>
    @Element.removeClass('ghost')
    @Element.disabled=false

  Disable: =>
    @Element.disable=true
    @Element.addClass('ghost')

  FormatValue: (value,max)=>
    return @Format.format
      value: value
      max: max

  SetAlert: (newValue) =>
    @Element.removeClass('alert')
    if newValue
      @Element.addClass('alert')

  Set: (value, max, immediate=false, time=1) =>
    @LabelName.html(@Name)
    #console.log("#{@Name}: #{@Value} -> #{value} #{max}")
    #if max == undefined
    #  console.error max
    if immediate
      percent = 0
      if max > 0
        percent=value/max
      @LabelValue.html(@FormatValue(+value.toFixed(2), +max.toFixed(2)))
      W = @Element.innerWidth()*percent
      @Bar.outerWidth W
    else
      nTicks = TICKS_PER_SECOND*time
      valPerTick=1
      if nTicks > 0
        valPerTick=(value-@Value)/nTicks
      cval=@Value
      @_CurrentMax=max
      timer = setInterval( =>
        hittarget=false
        cval += valPerTick
        if valPerTick > 0 # Positive
          if cval >= value
            hittarget=true
        else
          if cval <= value
            hittarget=true
        if hittarget
          cval=value
          clearInterval(timer)
        cpercent = 0
        if max > 0
          cpercent=cval/max
        @Bar.outerWidth @Element.innerWidth()*cpercent
        @LabelValue.html(@FormatValue(+cval.toFixed(2), +max.toFixed(2)))
      , TICKS_PER_SECOND)

    @Value=value
    #console.log "Setting width to %d", W
