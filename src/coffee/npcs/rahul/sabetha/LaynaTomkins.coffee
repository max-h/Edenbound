﻿class LaynaTomkins extends NPC
  @ID: 'layna_tomkins'
  SetupNPC: ->
    super()
    @ID = LaynaTomkins.ID

    @Mind.Hostile = no
    @Mind.Identity.FirstName = 'Layna'
    @Mind.Identity.LastName = 'Tomkins'

    # @classref: HumanSpecies
    @SetSpeciesID HumanSpecies.ID
    @InitializeDNA reinit=yes
    @GetSpecies().InitialBodySetup @

    # @enumref: ECommonGenders
    @SetGenderPreset ECommonGenders.FEMALE

    @Age = 24

    @GetAllBodyPartsOfClass(EBodyPartClass.EYE).forEach (eye) ->
      eye.Color = 'Blue'
    @GetAllBodyPartsOfClass(EBodyPartClass.HAIR).forEach (hair) ->
      hair.Color = 'Red'
    skin = @GetAllBodyPartsOfClass(EBodyPartClass.SKIN)[0]
    skin.Color = 'Pale'
    # @enumref: ESkinFlags
    skin.SkinFlags |= ESkinFlags.FRECKLED

    @Clothing.Wear FarmerShirt
    @Clothing.Wear FarmerOveralls
    @Clothing.Wear CottonPanties
    #@Clothing.Wear CottonSocks
    @Clothing.Wear FarmerBoots

NPC.Register LaynaTomkins
