﻿class FarmerOveralls extends Pants
  constructor: ->
    super 'farmer-pants'
    @Name='Farmer Overalls'
    @BaseValue   = 1
    @Weight      = 0.25 #kg
    @Description = 'Overalls made from the dingiest and coarsest wool you can imagine. Fit for peasants and farmers.'
# @classref: Item
Item.Register FarmerOveralls
