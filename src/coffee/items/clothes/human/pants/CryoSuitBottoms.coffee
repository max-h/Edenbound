﻿class CryoSuitBottoms extends Pants
  constructor: ->
    super 'cryo-pants'
    @Name = 'Cryo Suit Bottoms'
    # Inquisition will flip their SHIT
    @ItemFlags |= EItemFlags.HERETICAL
    # People who don't know you will freak out.
    @ItemFlags |= EItemFlags.TECHNOLOGICAL

    @BaseValue   = 0 # NOPE
    @Weight      = 1 # kg, neoprene is heavy.
    @Description = 'Form-fitting neoprene, plastic, tubes, wires, and nanogel.  The perfect thing for getting an Inquisitor\'s attention.'
# @classref: Item
Item.Register CryoSuitBottoms
