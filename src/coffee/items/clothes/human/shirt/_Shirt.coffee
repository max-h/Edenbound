﻿class Shirt extends Clothing
  constructor: (id) ->
    super id, EClothingSlot.SHIRT
    @Covers = [
      TorsoBodyPart
      BreastBodyPart
    ]
