﻿class FarmerShirt extends Shirt
  constructor: ->
    super 'farmer-shirt'
    @Name = 'Farmer tunic'
    @BaseValue   = 1
    @Weight      = 0.25 #kg
    @Description = 'A tunic made from coarse hemp.  It feels like sandpaper on your skin, but keeps the bugs off, at least.'
# @classref: Item
Item.Register FarmerShirt
