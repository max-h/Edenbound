﻿class TShirt extends Shirt
  constructor: ->
    super 't-shirt'
    @Name = 'Modern T-Shirt'
    # Inquisition will flip their SHIT
    @ItemFlags |= EItemFlags.HERETICAL
    # People who don't know you will freak out.
    @ItemFlags |= EItemFlags.TECHNOLOGICAL
    @BaseValue   = 0 # NOPE
    @Weight      = 0.01 #maybe ten grams
    @Description = '300 years of research and some amount of bleach has gone into this 100% cotton modern marvel, which you picked up at Wellgreen\'s for a buck a year ago.'
# @classref: Item
Item.Register TShirt
