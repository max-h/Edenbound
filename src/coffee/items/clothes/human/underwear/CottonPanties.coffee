﻿class CottonPanties extends BasePanties
  constructor: ->
    super 'cotton-panties'
    @Name = 'Cotton Panties'
    @BasePrice = 20
    @Weight = 0.01 #kg
    @Description = "A relatively new invention here on Eden.  Far coarser than what you'd find in the Alliance."
# @classref: Item
Item.Register CottonPanties
