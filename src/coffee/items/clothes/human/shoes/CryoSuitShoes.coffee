﻿class CryoSuitShoes extends Shoes
  constructor: ->
    super 'cryo-shoes'
    @Name = 'Cryo Suit Boots'
    # Inquisition will flip their SHIT
    @ItemFlags |= EItemFlags.HERETICAL
    # People who don't know you will freak out.
    @ItemFlags |= EItemFlags.TECHNOLOGICAL

    @BaseValue   = 0 # NOPE
    @Weight      = 1 # kg, neoprene is heavy.
    @Description = 'Form-fitting neoprene, plastic, tubes, wires, and nanogel.  The perfect thing for getting an Inquisitor\'s attention.'
# @classref: Item
Item.Register CryoSuitShoes
