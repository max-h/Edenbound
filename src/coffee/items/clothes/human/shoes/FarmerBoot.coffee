﻿class FarmerBoots extends Shoes
  constructor: ->
    super 'farmer-boots'
    @Name = 'Farmer Boots'
    @BasePrice=20 # 2$ per boot
    @Weight = 2 #kg
    @Description = 'Shoes are the ultimate consumable for a medieval peasant, as their loss can result in a massive loss of
    mobility and productivity.  These leather farming boots are even more sought-after, for their ability to keep feet warm
    and dry while wading through mud in pastures and fields.'
# @classref: Item
Item.Register FarmerBoots
