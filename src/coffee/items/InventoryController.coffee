﻿class InventoryController extends GameObject
  @ALL_TYPES=[]
  @TYPE_2_ID={}
  @ID_2_TYPE={}

  constructor: (@Creature) ->
    super()
    @Items = {}

  _getItemAndIDFromArg: (item) ->
    id = null
    switch typeof item
      when 'string'
        id = item
        item = InventoryController.CreateFromID(id)
        if item == null
          return [null,null]
      when 'function'
        item = new item()
        id = item.ID
      when 'object'
        id = item.ID
      else
        console.error "%s is %s, not a string, function, or object!", item, typeof item
    return [id, item]

  _getItemIDFromArg: (item) ->
      id = null
      switch typeof item
        when 'string'
          id = item
        when 'function'
          item = new item()
          id = item.ID
        when 'object'
          id = item.ID
        else
          console.error "%s is %s, not a string, function, or object!", item, typeof item
      return id

  _getIDFromArg: (arg) ->
    id = null
    switch typeof arg
      when 'string'
        id = arg
        if !(arg of InventoryController.ID_2_TYPE)
          return null
      when 'function'
        item = new arg()
        id = item.ID
      when 'object'
        id = arg.ID
      else
        console.error "%s is %s, not a string, function, or object!", arg, typeof arg
        return null
    return id

  AddItem: (arg, count=0) ->
    [id, item] = @_getItemAndIDFromArg(arg)
    if id == null
      console.log "Invalid argument to AddItem: id=%s, item=%s, count=%d", id, item, count
      return
    if item == null
      console.log "Invalid argument to AddItem: id=%s, item=%s, count=%d", id, item, count
      return
    if !(id of @Items)
      @Items[id]=item
    if count == 0
      count = item.Count
    if count == 0
      count = 1
    @Items[id].Count += count
    return @Items[id]

  RemoveItem: (item, count=1) ->
    id = @_getIDFromArg(item)
    if id == null
      console.log "Invalid argument to RemoveItem: id=%s, item=%s, count=%d", id, item, count
      return
    if !(id of @Items)
      console.log "Invalid ID in RemoveItem: id=%s", id
      return
    @Items[id].Count -= count
    if @Items[id].Count <= 0
      delete @Items[id]

    return

  Empty: ->
    @Items={}

  GetItem: (item) ->
    id = @_getIDFromArg(item)
    return @Items[id]

  GetOrEmpty: (item) ->
    id = @_getIDFromArg(item)
    if id of @Items
      return @Items[id]
    else
      return Item.CreateFromID(id)


  GetItemCount: (item) ->
    id = @_getIDFromArg(item)
    if !(id of @Items)
      return 0
    else
      return @Items[id].Count

  serialize: () ->
    data = {}
    for own key, item of @Items
      data[item.ID]=item.Count
    return data
  deserialize: (data) ->
    @Items.length=0
    for own id, count of data
      if id == '_type'
        continue
      @AddItem(id, count)
  clone: ->
    inv = new InventoryController(@Creature)
    for own id, item of @Items
      inv.Items[id] = clone(item)
    return inv
