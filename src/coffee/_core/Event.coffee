﻿class Event
  constructor: ->
    @Listeners=[]

  Fire: ->
    for listener in @Listeners
      listener(arguments...)

  Attach: (listener) ->
    @Listeners.push listener

  Detach: (listener) ->
    remove(@Listeners, listener)
