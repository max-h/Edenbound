﻿class CollectionEvent extends Event
  constructor: ->
    super()

  Fire: ->
    results = []
    for listener in @Listeners
      results.concat listener(arguments...)
    return results
