﻿class GameObject
  constructor: ->

  serialize: () ->
    return
      '_type': @constructor.name

  clone: (c) ->
    return

  deserialize: (data) ->
    return

  equals: (other) ->
    return true

  decompile: ->
    return @serialize()

  toJSON: ->
    return @serialize() #JSON.stringify(@serialize())
