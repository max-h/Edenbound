﻿# @classdef: Calculate
window.Calculate=
  Volume:
    OfCylinder: (length, radius) ->
      return Math.PI*Math.pow(radius,2)*length

    OfSphere: (radius) ->
      return (4/3)*Math.PI*Math.pow(radius,3)
