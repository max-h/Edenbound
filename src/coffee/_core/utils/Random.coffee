﻿window.pick = window.either

window.pickndrop = (arr) ->
  item = either(arr)
  index = arr.indexOf(item)
  if (index > -1)
    arr.splice(index, 1)
    return item
  return null

###
# Takes a map with the key being the possibility, and the value being the weight in decimal [0,1).
# @example How to use
#   ... = weighted_random {"fuck":0.25, "dicks": 0.75}
###
window.weighted_random = (spec) ->
  i = undefined
  sum = 0
  r = Math.random()
  for i, w of spec
    sum += w
    if r <= sum
      return i
  return either(Object.keys(spec)) # Failover



window.random_char = (CHAR_CLASS) ->
  return CHAR_CLASS.charAt(Math.floor(Math.random() * CHAR_CLASS.length))

###*
# Shuffles array in place.
# @param {Array} a items An array containing the items.
###
window.shuffle = (a) ->
  j = undefined
  x = undefined
  i = a.length - 1
  while i > 0
    j = Math.floor(Math.random() * (i + 1))
    x = a[i]
    a[i] = a[j]
    a[j] = x
    i--
  return



###
# Given a pct in [0,1], will return true (pct * 100)% of the time.
# @example Usage
#  if(roll(0.8))
#    console.log("80%% of the time, this block is called.");
# @param {float} pct Percent of the time that this roll will pass.
# @return {Boolean} True if random number is below pct.
###
window.roll = (pct) ->
  if pct == 0
    return false
  if pct == 1
    return true
  return Math.random() <= pct
