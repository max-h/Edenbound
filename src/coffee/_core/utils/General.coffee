﻿window.isnull = (a) ->
  return a == null
window.isundefined = (a) ->
  return a == undefined

window.jq2html = (a) ->
  return a.prop('outerHTML')

window.clamp = (value, min, max) ->
  return Math.min(Math.max(value, min), max)

window.count = window.len = (obj) ->
  return Object.keys(obj).length

###
# Gets the classname of an object.
###
window.getClass = (obj) ->
  if typeof obj == 'undefined'
    return 'undefined'
  if obj == null
    return 'null'
  Object::toString.call(obj).match(/^\[object\s(.*)\]$/)[1]

window.romanize = (num) ->
  if ! +num
    return NaN
  digits = String(+num).split('')
  key = [
    ''
    'C'
    'CC'
    'CCC'
    'CD'
    'D'
    'DC'
    'DCC'
    'DCCC'
    'CM'
    ''
    'X'
    'XX'
    'XXX'
    'XL'
    'L'
    'LX'
    'LXX'
    'LXXX'
    'XC'
    ''
    'I'
    'II'
    'III'
    'IV'
    'V'
    'VI'
    'VII'
    'VIII'
    'IX'
  ]
  roman = ''
  i = 3
  while i--
    roman = (key[+digits.pop() + i * 10] or '') + roman
  return Array(+digits.join('') + 1).join('M') + roman


CHARS_ALPHA_LC = 'abcdefghijklmnopqrstuvwxyz'
CHARS_ALPHA_UC = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
CHARS_ALPHA = CHARS_ALPHA_LC+CHARS_ALPHA_UC
CHARS_NUMERIC = '0123456789'
CHARS_ALPHA_NUMERIC = CHARS_ALPHA+CHARS_NUMERIC

window.CHAR_REDACT='█'

window.redacted = (nChars) ->
  return repeat_char(CHAR_REDACT, nChars)

window.redacted_sentence = (minWords,maxWords) ->
  words = []
  for _ in [0...random(minWords,maxWords)]
    words.push(redacted(random(1,10)))
  return words.join(' ')

window.repeat_char = (char, nChars) ->
  return Array(nChars+1).join(char)



window.remove = (array, element) ->
  index = array.indexOf(element)
  if index != -1
    array.splice index, 1
  return
if !Array::remove
  Array::remove = (element) ->
    index = @indexOf(element)
    if index != -1
      @splice index, 1
    return
window.escapeJS = (str) ->
  return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');

window.LOCALE_STRING_CONSIEE_ARGS =
  maximumFractionDigits: 2
  minimumFractionDigits: 2

window.formatConsiee = (value) ->
  return CONSIEE_SYMBOL + value.toLocaleString(undefined, LOCALE_STRING_CONSIEE_ARGS)

###
TODO
window.ENEMY = null
window.getEnemy: () ->
  if ENEMY == null
    ENEMY = new EnemyCharacter()
  return getPlayer()
###


EAlertType =
  WARNING: 'AddWarning'
  CRITICAL: 'AddCritical'
  GAIN: 'AddGain'
  ALERT: 'AddAlert'
  INFO: 'AddInfo'
  SUCCESS: 'AddSuccess'

###
# Utility for easily adding a message to the alert queue.
###
class AlertUtils
  @AddRaw: (raw_message) ->
    if getVariables().alerts == undefined or getVariables().alerts == 0
      getVariables()["alerts"] = []
    window.getVariables().alerts.push(raw_message)

  @Add: (alert_type, message) ->
    switch alert_type
      when EAlertType.WARNING then @AddWarning message
      when EAlertType.CRITICAL then @AddCritical message
      when EAlertType.GAIN then @AddGain message
      when EAlertType.ALERT then @AddAlert message
      when EAlertType.INFO then @AddInfo message
      when EAlertType.SUCCESS then @AddSuccess message


  #Yes, all these names are nonsensical as fuck.

  @AddWarning: (message) ->
    @AddRaw "[img[icon_alert_critical]]<brightalert>#{message}</brightalert><hr>"

  @AddCritical: (message) ->
    @AddRaw "[img[icon_alert_red]]<deepalert>#{message}</deepalert><hr>"

  @AddGain: (message) ->
    @AddRaw "[img[icon_gain]]#{message}<hr>"

  @AddAlert: (message) ->
    @AddRaw "[img[icon_alert]]#{message}<hr>"

  @AddSuccess: (message) ->
    @AddRaw "<affirmative>#{message}</affirmative><hr>"

  @AddInfo: (message) ->
    @AddAlert message

EMutbarWarningClasses =
  WARNING: 'stat-warn'
  BAD:     'stat-bad'
  VERYBAD: 'stat-very-bad'
AllMutbarWarnClasses=[EMutbarWarningClasses.WARNING, EMutbarWarningClasses.BAD, EMutbarWarningClasses.VERYBAD]

window.deserialize = (data) ->
  if data == null
    return null
  switch typeof data
    when 'Map', 'object', 'Object'
      if '_type' of data
        type_found = window[data['_type']]
        o = new type_found() # @depdetect: ignore
        o.deserialize(data)
        return o
      for k,v of data
        data[k] = deserialize v
    when 'Array'
      for i in [0...data.length]
        data[i] = deserialize data[i]
  return data

class MutBar
  constructor: (@ID, @Title, @Icon, @Mutagenic=false) ->
    @Text=''
    @WarnClass=null

  setWarnClass: (cls) ->
    @WarnClass = cls

  buildHTML: () ->
    barbody = $('<div class="sidebar-pip">')
    barbody.attr('id', @ID)
    if @WarnClass != null
      barbody.addClass @WarnClass
    head = $('<span>')
    head.addClass('head')
    if @Mutagenic
      mutagenspan = $('<span class="iconwrap mutagen">')
      mutagenspan.attr('title', 'Mutagen!')
      mutagenspan.append("[img[biohazard]]")
      head.append(mutagenspan)
    iconwrap = $('<span class="iconwrap">')
    iconwrap.attr('title', @Title)
    iconwrap.append("[img[#{@Icon}]]")
    head.append(iconwrap)

    barbody.append(head)

    barvalue = $('<span class="value">')
    barvalue.text(@Text)
    barbody.append(barvalue)
    return barbody

  toHTML: ->
    return jq2html(@buildHTML())

window.dropSpinner = (spinner) ->
  if spinner != null
    spinner.spinner("destroy")
    spinner.attr("id", null)

window.dropTabs = (control) ->
  if control != null
    control.tabs("destroy")
    control.attr("id", null)

window.dropButton = (button) ->
  if button != null
    button.attr("id", null)
    button.remove()

window.dropDialog = (dlg) ->
  if dlg != null
    dlg.dialog('destroy')
    dlg.attr("id", null)

window.andjoin = (entries, joinword='and') ->
  o = ''
  for i in [0...entries.length]
    if i > 0
      if i < (entries.length-1)
        o += ', '
      else
        o += ', '+joinword+' '
    o += entries[i]
  return o

window.orjoin = (entries) ->
  return andjoin(entries, 'or')

###
# Used for setter/getter support.
# @example
#   class MyClass
#      @property 'my_property',
#        get: ->
#          "return value"
#        set: (value) ->
#          # do something with value
###
Function::property = (prop, desc) ->
  Object.defineProperty @prototype, prop, desc

###
# Sanity check for numeric assignments.
###
window.assert_sane_number = (value, allow_nan=no, allow_null=no, allow_undefined=no) ->
  err_prefix = "assert_sane_number(value=#{value}, allow_nan=#{allow_nan}, allow_null=#{allow_null}, allow_undefined=#{allow_undefined}"
  if !allow_null and value == null
    console.error "#{err_prefix} - VALUE IS NULL"
    alert "BUG: Assignment to a variable was expected to be a number, but was actually null!\nDeets:\n  #{err_prefix}\nCheck console for stack trace."
    throw new Exception("Assertion failure.")# @depdetect: ignore
  if !allow_undefined and value == undefined
    console.error "#{err_prefix} - VALUE IS UNDEFINED"
    alert "BUG: Assignment to a variable was expected to be a number, but was actually undefined!\nDeets:\n  #{err_prefix}\nCheck console for stack trace."
    throw new Exception("Assertion failure.")# @depdetect: ignore
  if typeof value != 'number'
    console.error "#{err_prefix} - VALUE IS %s", typeof value
    alert "BUG: Assignment to a variable was expected to be a number, but was actually %s!\nDeets:\n  #{err_prefix}\nCheck console for stack trace.", typeof value
    throw new Exception("Assertion failure.")# @depdetect: ignore
  if !allow_nan and isNaN(value)
    console.error "#{err_prefix} - VALUE IS NULL"
    alert "BUG: Assignment to a variable was expected to be a number, but was actually null!\nDeets:\n  #{err_prefix}\nCheck console for stack trace."
    throw new Exception("Assertion failure.")# @depdetect: ignore
  return value

window.getTimeInMS = ->
  return (new Date()).getTime()



highlightBad = (el, isBad) ->
  el.removeClass 'invalid'
  if isBad
    el.addClass 'invalid'

loadLinesFrom = (passage) ->
  lines = []
  for line in window.tale.get(passage).text.split('\n')
    line=line.trim()
    if line == '' or line.startsWith('#')
      continue
    lines.push line
  return lines

class ValueHolder
  constructor: (val) ->
    @Value = val

window.getTrueAndFalseForType = (t) ->
  trueVal=1
  falseVal=0
  if trueVal == undefined
    t = "int"
  switch t
    when "int"
      trueVal = 1
      falseVal = 0
    when "str", "string"
      trueVal = "1"
      falseVal = "0"
    when "bool", "boolean"
      trueVal = true
      falseVal = false
  return [trueVal, falseVal]



if !String::format
  String::format = ->
    str = @toString()
    if !arguments.length
      return str
    args = typeof arguments[0]
    args = if 'string' == args or 'number' == args then arguments else arguments[0]
    for arg of args
      str = str.replace(RegExp('\\{' + arg + '\\}', 'gi'), args[arg])
    str
