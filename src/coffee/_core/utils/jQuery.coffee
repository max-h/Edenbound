﻿# jQuery Extensions

###
# Checks that elements actually exist in this selector.
###
$.fn.exists = ->
  return @length != 0

###
# Checks whether a selector has found an element with the given attribute present.
# @param {String} name Name of the attribute.
###
$.fn.hasAttr = (name) ->
  return @attr(name) != undefined

$.fn.extend disable: (state) ->
  @each ->
    @disabled = state
    return

###
# Center block vertically and horizontally within window.
###
jQuery.fn.center = ->
  @css 'position', 'absolute'
  @css 'top', Math.max(0, ($(window).height() - $(@).outerHeight()) / 2 + $(window).scrollTop()) + 'px'
  @css 'left', Math.max(0, ($(window).width() - $(@).outerWidth()) / 2 + $(window).scrollLeft()) + 'px'
  this


###
Basic garbage collection since Twine breaks fucking everything.
###
class JQHelper
  @Trash: []
  @GC: ->
    for entry in JQHelper.Trash
      [trash, cleanupFunc] = entry
      cleanupFunc($(entry))
    console.log "JQHelper.GC: Cleaned up %d jQuery objects.", JQHelper.Trash.length
    JQHelper.Trash.length = 0

  @_SelectAndPush: (selector, df) ->
    e = $(selector)
    JQHelper.Trash.push [e, df]
    return e


  ###
  Use ONLY for basic jquery stuff.
  ###
  @GetElement: (selector) ->
    return @_SelectAndPush selector, @_DestroyElement
  @_DestroyElement: (e) ->
    if e.exists()
      e.attr("id", null)
      e.remove()

  ###
  # jQuery UI Spinners
  ###
  @GetSpinner: (selector) ->
    return @_SelectAndPush selector, @_DestroySpinner
  @_DestroySpinner: (spinner) ->
    if spinner.exists() and spinner.data('uiSpinner')
      spinner.spinner("destroy")
      spinner.attr("id", null)

  ###
  # jQuery UI - Dialogs
  ###
  @GetDialog: (selector) ->
    return @_SelectAndPush selector, @_DestroyDialog
  @_DestroyDialog: (dlg) ->
    if dlg.exists() and dlg.data('uiDialog')
      dlg.dialog("destroy")
      dlg.attr("id", null)

  ###
  # jQuery UI - Progress bars
  ###
  @GetProgressBar: (selector) ->
    return @_SelectAndPush selector, @_DestroyProgressBar
  @_DestroyProgressBar: (prg) ->
    if prg.exists() and prg.hasClass('ui-progressbar')
      prg.progressbar("destroy")
      prg.attr("id", null)
      prg.remove()

JQConfirm = (config) ->
  if !config.title
    config.title='Confirm'
  config.yes_text = if 'yes_text' of config then config.yes_text else 'OK'
  config.no_text = if 'no_text' of config then config.no_text else 'Cancel'
  btns = {}
  btns[config.yes_text] = (ui, e) ->
    if config.yes
      config.yes(ui, e)
    $(this).dialog('close')
  btns[config.no_text] = (ui, e) ->
    if config.no
      config.no(ui, e)
    $(this).dialog('close')
  $('<div></div>')
    .attr('title', config.title)
    .append($('<p></p>').html(config.text))
    .dialog
      resizable: false
      modal: true
      height: "auto"
      close: (ui, e) ->
        $(this).remove()
      buttons: btns

JQAlert = (config) ->
  if !config.title
    config.title='Alert'
  config.ok_text = if 'ok_text' of config then config.ok_text else 'OK'
  btns = {}
  btns[config.ok_text] = (ui, e) ->
    if config.ok
      config.ok(ui, e)
    $(this).dialog('close')
  $('<div></div>')
    .attr('title', config.title)
    .append($('<p></p>').html(config.text))
    .dialog
      resizable: false
      modal: true
      height: "auto"
      close: (ui, e) ->
        $(this).remove()
      buttons: btns
