﻿window.UnitConvert =
  Distance:
    ###
    # cm <-> "
    ###
    Small: (v) ->
      if getVariables().UseSI
        return "#{Math.round(v)}cm"
      else
        if v == 0
          return '0"'
        allinches = v*0.393701
        inches = Math.round(allinches % 12)
        feet = Math.floor(allinches/12)
        o=[]
        if feet > 0
          o.push "#{feet}'"
        if inches > 0
          o.push "#{inches}\""
        return o.join(' ')

    CM2Inch: (v) ->
      return v*0.393701
    Inch2CM: (v) ->
      return v*2.54

  Mass:
    ###
    # kg <-> lb
    ###
    Medium: (v) ->
      if getVariables().UseSI
        return "#{Math.round(v)}kg"
      else
        v = v*2.20462
        return "#{Math.round(v)}lb"
