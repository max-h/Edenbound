﻿###
# This file contains interfaces with Nylon.
###

# Used to determine what Twine's current element is.  Can also be set.
window.twineCurrentEl = null

# Used to get the current parser. Can probably be set, but, like, why?
window.twineCurrentParser = null

###
# Parses TwineScript, then inserts the resulting DOM nodes into place.
# @param arg {String} Twinescript to parse.
# @param place {DOMElement} Parent DOMElement which will contain the output.
###
window.twinePrint = window.twprint = (arg, place=window.twineCurrentEl) ->
  parser = twineCurrentParser
  try
    # See comment within macros.display
    output = internalEval(arg)
    if output != null and (typeof output != 'number' or !isNaN(output))
      new Wikifier(place, '' + output)
  catch e
    place.append $("<span class=\"twineError\"><b>twinePrint():</b> bad expression: <code>#{escapeJS(arg)}</code></span>")
    if TWINE_CATCH_ERRORS
      console.error e
    else
      throw e
  return

###
# Prints a line break.
###
window.twineNewline = (place=window.twineCurrentEl) ->
  twinePrint '"<br>"', place

###
# Acts like <<display>>.
###
window.twineDisplayInline = (name, place=window.twineCurrentEl) ->
  if !window.tale.has(name)
    twinePrint "<span class=\"twineError\"><b>twineDisplayInline():</b> Unknown passage <code>#{name}</code></span>", place
  else
    passage = tale.get(name)
    new Wikifier(place, passage.processText())
  return

###
# Create an internal twine Button.
#
# @param label {string} Text of the button.
# @param passage {string} Passage to display.
# @param callback {function} Callback to run after display() is called.
# @param place {HTMLElement} Parent of the button in DOM.
###
window.twineButton = (label, passage, callback, place=window.twineCurrentEl) ->
  button = Wikifier.createInternalLink place, passage, (->
    window.state.display passage, null, null, callback
    return
  ), 'button'
  insertText button, label
  return button



window.updateStoryMenu = ->
  document.getElementById('storyMenu').setAttribute 'style', ''
  window.setPageElement 'storyMenu', 'StoryMenu', ''
  Nylon.PostSetPageElements.Fire()

window.getVariables = () ->
  return window.state.history[0].variables

window.postDisplay = {}
window.preDisplay = {}
window.preSetPageElements = {}
window.postSetPageElements = {}

window.preDisplay['*'] = (a,b,c) ->
  Nylon.PreRender.Fire(a,b,c)
  return
window.postDisplay['*'] = (a,b,c) ->
  Nylon.PostRender.Fire(a,b,c)
  return
window.postSetPageElements['*'] = ->
  Nylon.PostSetPageElements.Fire()
  return
window.preSetPageElements['*'] = ->
  Nylon.PreSetPageElements.Fire()
  return


window.getImageBlob = (passage) ->
  # Base64 passage transclusion
  imgPassages = window.tale.lookup('tags', 'Twine.image')
  j = 0
  while j < imgPassages.length
    if imgPassages[j].title == passage
      return imgPassages[j].text
    j++
  return null

window.getPassageByID = (passage) ->
  if !(passage of window.tale.passages)
    console.warn "Unable to find #{passage} in window.tale.passages. Here comes the errors..."
  return window.tale.passages[passage]

window.embedSVG = (passage, where=window.twineCurrentEl) ->
  div = document.createElement('div')
  div.innerHTML = getPassageByID(passage).text
  where.append div.firstChild


class Nylon
  @PreRender: new Event()
  @PostRender: new Event()
  @PostSetPageElements: new Event()
  @PreSetPageElements: new Event()

  @AddPreRenderHook: (cb) ->
    @PreRender.Attach cb
    return
  @AddPostRenderHook: (cb) ->
    @PostRender.Attach cb
    return
  @AddPostSetPageElementsHook: (cb) ->
    @PostSetPageElements.Attach cb
    return
  @AddPreSetPageElementsHook: (cb) ->
    @PreSetPageElements.Attach cb
    return

  @GetPassageByID: (passid) ->
    if !(passid of window.tale.passages)
      console.warn "Unable to find #{passid} in window.tale.passages. Here comes the errors..."
    return window.tale.passages[passid]

  @EmbedSVG: (passage, where=window.twineCurrentEl) ->
    div = document.createElement('div')
    div.classList.add 'svg'
    div.innerHTML = Nylon.GetPassageByID(passage).text
    where.append div.firstChild
    return

  @GetCurrentPassage: ->
    return window.state.history[0].passage

  @GetCurrentVariables: ->
    return window.state.history[0].variables

  @Get: (varID, defaultValue=0) ->
    vars = @GetCurrentVariables()
    if varID not of vars
      return defaultValue
    return vars[varID]

  @Set: (varID, value) ->
    @GetCurrentVariables()[varID]=value
    return
