﻿###
# Divide Floor
#
# Equivalent of Math.floor(x,y).
# More compact version, used to replicate python's // operator.
###
window.divF = (x,y)->
  return Math.floor(x/y)

###
From the Python manual:
[T]he result is the same as (a // b, a % b)
###
window.divmod = (a,b) ->
  return [divF(a,b), a % b]


window.Clamp = (subject, min, max) ->
  return Math.min(Math.max(subject, min), max)
