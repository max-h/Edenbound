﻿# http://www.scrollseek.com/training/densitiesofdifferentbodymatter.html
#kg/m3
window.DENSITY_OF_BLOOD = 1042.8
window.DENSITY_OF_BONE  = 1750.0
window.DENSITY_OF_FLESH = 985 # Taken from elsewhere.
window.DENSITY_OF_FAT   = 909.4


window.MINUTE = 60
window.HOUR = 60 * MINUTE
window.DAY = 24 * HOUR
window.WEEK = 7 * DAY
window.MONTH = 30 * DAY
window.YEAR = 365 * DAY
