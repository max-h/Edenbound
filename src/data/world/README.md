# World Specification

A Planet in Edenbound is a collection of data files that specify Regions, which have
a collection of Zones, which are further subdivided into Positions.

At buildtime, all these data points will be compiled into a single compressed JSON object and
stuffed into a special `__WORLD__` passage, later read by the Universe controller.

The intent is to produce a system as easy to work with as TiTS, with the options of BOUNDLESS,
but with far better editing, flexibility, and accessibility than either can provide.

## Planets

```yaml
Eden:
  # Planet class to instantiate.
  type: Planet

  # Atmospheric simulation
  atmosphere:
    # Pressure at sea level, in kPa (1 atm = 101.325)
    pressure: 101.325

    # The chance of the weather changing. Sampled every hour, 0-1.
    convective-ratio: 0.25

    # Chemicals present in the atmosphere. Ratios.
    reagents:
      oxygen: 0.2
      nitrogen: 0.8

  ###
  # Defaults for new regions.  Regions can override.
  ###

  # Incoming radiation info, as measured at sea level.
  radiation:
    # Sunburn damage per second to uncovered body parts in sunny areas. (Adjust as though there were the desired levels of ozone present.)
    ultraviolet:
      min: 0
      max: 0

    # Governs background gamma radiation exposure rates. Can result in mutations or rad damage.
    gamma:
      min: 0
      max: 0

    # Fucks with electronics, plus what gamma does.
    cosmic:
      min: 0
      max: 0

  # The climate of the planet.
  climate:
    # Governs max speed of winds during storms.
    winds: #kph
      min: 0
      max: 100

    # Governs chance of rainfall and how much each rainstorm produces.
    # 1 = earth humidity
    humidity: 1

    weather-types:
      - CLEAR
      - CLOUDY
      - RAIN
      - RAINSTORM
      - THUNDERSTORM
      - SNOWSTORM
      #- RADSTORM # Wind blowing around radioactive dust.
      #- DUSTSTORM # Mars-style dust storms. Add inhalation damage, block out the sun.
      #- SANDSTORM
      #- HURRICANE # Rare.
```

## Regions

```yaml
Hector Plateau:
  # NOTE: Newlines are not preserved.
  description: >
    A beautiful coastal mountain range in south Idonia.  Made of extinct and mildly
    active volcanoes, its peaks are conical and capped with snow year-round, which
    creep down in the Fall and Winter.  

    However, these peaks have only been lightly terraformed, so beware of dangerous
    native beasts.
  # Background picture, mostly.
  biome-type: MOUNTAINOUS

  planet: Eden

  atmosphere:
    pressure: 98.5 #kPa

  locations:
    # ZoneID => Marker
    Helena: [0,0] # Center of map

  encounters:
    # All seasons
    all:
      # Spawns a creature with the given SpeciesID.
      # Imports from earth
      - creature: wolf
      - creature: fox
      - creature: mountain lion
        chance: 0.05 # 5%
      - creature: mountain goat
      - creature: deer

      # constructs.  Will ignore you unless approached or otherwise threatened.
      - creature: rogue angel # android, dangerous as fuck
          chance: 0.01 # 1%
      - creature: rogue cherub # quadcopter with a taser
          chance: 0.05 # 5%
      - creature: rogue servitor # spiderbot with long, electrified claws.

      # Humanoids
      - creature: trader    # Trades things for coin
      - creature: traveller # exchange pleasantries, ask about their life etc, murderize them
      - creature: hunter    # Trader but with meat and skins
      - creature: bandit    # Combat

      # Native species
      - creature: ripper # angry komodo dragon/chameleon mix

      # Sends you to a passage.
      - passage: Find Mountain Cabin

    spring:
      - creature: mudworm # Big worm that hides in mud, strikes at legs to disable prey.
      - creature: screecher #
      # imports from earth
      - creature: goose
      - creature: raven

    summer:
      - creature: screecher
      - creature: raven
      - creature: goose

```

## Zones
```yaml
Helena:
  region:    Hector Plateau
  country:   Ericsland
  zone-type: CITY
  nav-type:  MENU
  description: >
    The capital of Ericsland.
  menu-items:
    - label: Bakery
      passage: Helena Bakery
      description: Smells good.

    - label: Pawn
      passage: Helena Pawn
      description: A place to sell your stuff.

    - label: Armory
      passage: Granger Armory
      description: World-reknowned armorers.

    - label: Clothier
      passage: Cottonmouth Clothiers
      descriptions: Get new threads.

    - label: Brothel
      hide-if: "($player.Mind.Memories['helena-brothel-password'] == undefined) || ($SFW == true)"
      passage: Helena Brothel
      descriptions: Check out the local tail.
