import os
import sys
import csv

EARLIEST=1880
NEWEST=2016

ALL_NAMES={}
SURNAMES = {}
OUT_DIR=os.path.join('src', 'tiddler', 'data')

class Name(object):
    def __init__(self, name:str, num:int=0):
        self.name=name
        self.num=num

for i in range(EARLIEST,NEWEST+1):
    path = os.path.join('data', 'ssa-gov-names', 'yob'+str(i)+'.txt')
    print(path)
    with open(path, 'r') as f:
        for line in f:
            name, sex, occurances = line.strip().split(',')
            occurances = int(occurances)
            if sex not in ALL_NAMES:
                ALL_NAMES[sex]={}
            if name not in ALL_NAMES[sex]:
                ALL_NAMES[sex][name]=Name(num=occurances, name=name)
            else:
                ALL_NAMES[sex][name].num += occurances
def writeNamesTo(filename, innames):
    print('w '+filename)
    names=[]
    for name in sorted(innames.values(), key=lambda x: x.num, reverse=True):
        names+=[name.name]
        if len(names) >= 500:
            break
    with open(filename, 'w') as f:
        for name in sorted(names):
            f.write(name+'\n')

writeNamesTo(os.path.join(OUT_DIR, 'MaleNames.txt'), ALL_NAMES['M'])
writeNamesTo(os.path.join(OUT_DIR, 'FemaleNames.txt'), ALL_NAMES['F'])

ALL_NAMES={}
SKIP=2
FLD_NAME=0
FLD_COUNT=2
with open(os.path.join('data', 'Names_2010Census_Top1000.csv'), 'r') as f:
    for _ in range(SKIP):
        next(f)
    reader = csv.reader(f)
    rown=0
    for row in reader:
        name = row[FLD_NAME]
        if name == '':
            break
        name = name[0].upper()+name.lower()[1:]
        if name.startswith('Mc'):
            name = 'Mc'+name[2].upper()+name.lower()[3:]
        if name.startswith('Mac') and name[3] not in 'aeiouyk':
            name = 'Mac'+name[3].upper()+name.lower()[4:]
        count = row[FLD_COUNT]
        if name not in ALL_NAMES:
            ALL_NAMES[name]=Name(name, count)
        else:
            ALL_NAMES[name].num += count
writeNamesTo(os.path.join(OUT_DIR, 'Surnames.txt'), ALL_NAMES)
