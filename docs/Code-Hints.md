We use a custom dependency resolution system for CoffeeScript.  Since it's relatively stupid,
we have some code "hints" to tell it when to (not) do things.

# General Structure

A code hint usually looks something like this:

```coffeescript
cls = BodyParts.CreateAndDeserialize data
bp = new cls() # @depdetect: ignore
```

As seen above, hints are given inside of comments.  Depending on the hint, the hint must be placed
**before** the code it references, or on the same line at the end (**inline**).

## Dependency Detector
The dependency detector hints affect how BUILD.py detects which files are
referenced by or reference other files.  This then affects how classes are loaded at runtime.

Failure to do this can result in weird bugs, like:
 * Class is undefined
 * Class does not have a constructor

<table>
<tr><th>Hint</th><th>Placement</th><th>Effects</th></tr>
<tr><td>`# @depdetect: ignore`</td><td>inline</td><td>Skips a line of code when trying to detect dependencies.  Helps with `new` statements that reference a variable.</td></tr>
<tr><td>`# @depdetect: enumdef Enum`<br>`# @enumdef: Enum`</td><td>before</td><td>Forces dependency detection to detect an enum definition in the current file.</td></tr>
<tr><td>`# @depdetect: enumref Enum`<br>`# @enumref: Enum`</td><td>before</td><td>Forces dependency detection to detect a reference to an enumerator in the current file.</td></tr>
<tr><td>`# @depdetect: classdef Class`<br>`# @classdef: Class`</td><td>before</td><td>Forces dependency detection to detect a class definition in the current file.</td></tr>
<tr><td>`# @depdetect: classref Class`<br>`# @classref: Enum`</td><td>before</td><td>Forces dependency detection to detect a reference to a class in the current file.</td></tr>
</table>

## Serializer Generator
The serializer generator tells the game how to serialize objects for saves.

<table>
<tr><th>Hint</th><th>Placement</th><th>Effects</th></tr>
<tr><td>`# @serialize: [no] [scalar|object=TYPE] [name=...] [new-instance=new...;] [required] [array] [pass-self] [...]`</td><td>Before, in constructor</td><td>**ONLY AFFECTS CLASS VARIABLES.**<br><ul><li>**no**: Skip serializing this variable whatsoever.</li><li>**scalar/object=...**: Whether the variable stores a scalar value (boolean/int/string/float) or an object. Affected by *array*.</li><li>**name=...:** Key to use in the generated object.</li><li>**new-instance=...;** How to create a new object during deserialization. **WARNING:** MUST include semicolon at the end of the statement!</li><li>**required:** Throws an error if not present during deserialization. Otherwise, uses default value. (See `default=`)</li><li>**array:** Marks variable as being a JS array (list, in Python). Each item in the list will be (de)serialized according to other rules.</li><li>**pass-self:** Passes `@` (`this` in JS) to `deserialize()` as well as `data`.</li></ul></td></tr>
<tr><td>`# @gen-all-serializers`</td><td>anywhere in a class</td><td>Calls `# @gen-cloner`, `# @gen-serializer`, and `# @gen-deserializer`.</td></tr>
<tr><td>`# @gen-cloner`</td><td>anywhere in a class</td><td>Creates a default cloner.</td></tr>
<tr><td>`# @gen-serializer`</td><td>anywhere in a class</td><td>Creates a default serializer.</td></tr>
<tr><td>`# @gen-deserializer`</td><td>anywhere in a class</td><td>Creates a default deserializer.</td></tr>
</table>
