The game uses a variety of different tags to control how the UI displays.

<table>
<tr><th>Tag</th><th>Description</th></tr>
<tr><th>nosidebar</th><td>Hides the sidebar.</td></tr>
<tr><th>hide-sidebar</th><td>Hides the sidebar.</td></tr>
</table>
